﻿using System;
using System.Web;
using System.Xml;
using System.IO;
//using XDocuments;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.Reflection;
using System.Net;
////using KubionLogNamespace;
using XHTMLMerge;
using System.Web.SessionState;
using System.Collections.Concurrent;
using System.Web.WebSockets;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace XDocServices
{
    ////    public  class WSClients
    ////    {
    ////        private  uint BufferSize = 1024 * 256;
    ////        private  ConcurrentDictionary<string, string> tickets = new ConcurrentDictionary<string, string>();
    ////        private  ConcurrentDictionary<string, AspNetWebSocketContext> clients = new ConcurrentDictionary<string, AspNetWebSocketContext>();
    ////        string sVersion = "";
    ////        AppCache m_AppCache = null;
    ////        CRuntime xrun = null;

    ////        public WSClients():this(null) {}
    ////        public WSClients(AppCache m_AppCache)
    ////        {
    ////            HttpContext m_context = HttpContext.Current;
    ////            if ((m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
    ////            if ((m_AppCache == null) && (m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
    ////            xrun = new CRuntime(m_AppCache, null);
    ////            xrun.myHttpContext = null;
    ////            xrun.sVersion = sVersion;
    ////        }
    ////        public async Task Connect(AspNetWebSocketContext context)
    ////        {
    ////            string sTicket = getTicket(HttpContext.Current.Request);
    ////            if (!tickets.ContainsKey(sTicket)) return;
    ////            string sUser = tickets[sTicket];
    ////            clients.TryAdd(sTicket, context);
    ////            RunWSTemplate("OnConnect", sTicket, sUser, "");
    //////            OnConnect(sTicket, sUser);
    ////            var buffer = new byte[BufferSize];
    ////            while (context.WebSocket.State == System.Net.WebSockets.WebSocketState.Open)
    ////            {
    ////                var cts = new CancellationTokenSource();
    ////                var segment = new ArraySegment<byte>(buffer);
    ////                try
    ////                {
    ////                    var result = await context.WebSocket.ReceiveAsync(segment, cts.Token);
    ////                    switch (result.MessageType)
    ////                    {
    ////                        case System.Net.WebSockets.WebSocketMessageType.Text:
    ////                            var message = System.Text.Encoding.UTF8.GetString(buffer, 0, result.Count);
    ////                            Message(sTicket, message);
    ////                            break;
    ////                        case System.Net.WebSockets.WebSocketMessageType.Close:
    ////                            Disconnect(sTicket,false);
    ////                            break;
    ////                        default:
    ////                            throw new NotSupportedException();
    ////                    }
    ////                }
    ////                catch (Exception ex)
    ////                {
    ////                    Console.WriteLine(ex.Message);
    ////                    Disconnect(sTicket,true);
    ////                }
    ////            }
    ////            //OnSocketClosed(guid);
    ////        }
    ////        //private async Task SendMessage<T>(string connectionId, T data)
    ////        public void addTicket(string sTicket, string sUser)
    ////        {
    ////            tickets.TryAdd(sTicket, sUser);
    ////        }
    ////        public void removeTicket(string sTicket, string sUser)
    ////        {
    ////            Disconnect(sTicket,true);
    ////        }
    ////        public void removeUser(string sUser)
    ////        {
    ////            string sTickets = "";
    ////            foreach (var de in tickets)
    ////                if (de.Value.ToString() == sUser)
    ////                    sTickets = sTickets + de.Key + ";";
    ////            string[] aTickets = sTickets.Split(';');
    ////            Disconnect(aTickets);
    ////        }
    ////        public void sendTicket(string sTicket, string sMessage)
    ////        {
    ////            string[] aTickets = { sTicket };
    ////            SendMessages(aTickets, sMessage);
    ////            //BroadCast(sMessage);
    ////        }
    ////        public void sendUser(string sUser, string sMessage)
    ////        {
    ////            string sTickets = "";
    ////            foreach (var de in tickets)
    ////                if (de.Value.ToString() == sUser)
    ////                    sTickets = sTickets + de.Key + ";";
    ////           string[] aTickets = sTickets.Split(';');
    ////           SendMessages(aTickets, sMessage);
    ////            //BroadCast(sMessage);
    ////        }

    ////        private async Task SendMessage(string sTicket, string message)
    ////        {
    ////            //var message = JsonConvert.SerializeObject(data);
    ////            var ws = clients[sTicket].WebSocket;
    ////            var buffer = System.Text.Encoding.UTF8.GetBytes(message);
    ////            var cts = new CancellationTokenSource();
    ////            if (ws.State == System.Net.WebSockets.WebSocketState.Open)
    ////            {
    ////                await ws.SendAsync(new ArraySegment<byte>(buffer), System.Net.WebSockets.WebSocketMessageType.Text, true, cts.Token);
    ////            }
    ////        }
    ////        private  async Task SendMessages(string[] aTickets, string message)
    ////        {
    ////            //var message = JsonConvert.SerializeObject(data);
    ////            //            string[] aTickets = sTickets.Split(';');
    ////            var buffer = System.Text.Encoding.UTF8.GetBytes(message);
    ////            foreach (string sTicket in aTickets)
    ////            {
    ////                if (clients.ContainsKey(sTicket))
    ////                {
    ////                    var ws = clients[sTicket].WebSocket;
    ////                    var cts = new CancellationTokenSource();
    ////                    if (ws.State == System.Net.WebSockets.WebSocketState.Open)
    ////                    {
    ////                        await ws.SendAsync(new ArraySegment<byte>(buffer), System.Net.WebSockets.WebSocketMessageType.Text, true, cts.Token);
    ////                    }
    ////                }
    ////            }
    ////        }
    ////        private  async Task BroadCast(string message)
    ////        {
    ////            var buffer = new ArraySegment<byte>(System.Text.Encoding.UTF8.GetBytes(message));
    ////            var tasks = new List<Task>();
    ////            var cts = new CancellationTokenSource();
    ////            foreach (var ws in clients.Values)
    ////            {
    ////                if (ws.WebSocket.State == System.Net.WebSockets.WebSocketState.Open)
    ////                {
    ////                    tasks.Add(ws.WebSocket.SendAsync(buffer, System.Net.WebSockets.WebSocketMessageType.Text, true, cts.Token));
    ////                }
    ////            }
    ////            await Task.WhenAll(tasks);
    ////        }

    ////        private void Disconnect(string[] aTickets)
    ////        {
    ////            foreach (string sTicket in aTickets)
    ////            {
    ////                Disconnect(sTicket, true);
    ////            }
    ////        }

    ////        private void Disconnect(string sTicket, bool bSilent)
    ////        {
    ////            AspNetWebSocketContext _context;
    ////            string sUser;
    ////            clients.TryRemove(sTicket, out _context);
    ////            tickets.TryRemove(sTicket, out sUser);
    ////            bool bLast = (!tickets.Values.Contains(sUser));
    ////            if(!bSilent)
    ////                RunWSTemplate("OnDisconnect", sTicket, sUser, bLast?"1":"0");
    ////                //OnDisconnect(sTicket, _user, bLast);
    ////            //var t = BroadCast("{}");
    ////        }
    ////        private  void Message(string sTicket, string sMessage)
    ////        {
    ////            string sUser;
    ////            tickets.TryGetValue(sTicket, out sUser);
    ////            RunWSTemplate("OnMessage", sTicket, sUser, sMessage);
    ////            //OnMessage(sTicket,_user, sMessage);
    ////            //var t = BroadCast("{}");
    ////        }
    ////        //private  void OnConnect(string sTicket, string sUser)
    ////        //{
    ////        //    Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
    ////        //    newHash.Add("Event", "OnConnect");
    ////        //    newHash.Add("Ticket", sTicket);
    ////        //    newHash.Add("User", sUser);
    ////        //    RunXDocTemplate("WS", newHash);
    ////        //}
    ////        //private  void OnDisconnect(string sTicket, string sUser, bool bLast)
    ////        //{
    ////        //    Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
    ////        //    newHash.Add("Event", "OnDisconnect");
    ////        //    newHash.Add("Ticket", sTicket);
    ////        //    newHash.Add("User", sUser);
    ////        //    newHash.Add("Last", bLast?"1":"0");
    ////        //    RunXDocTemplate("WS", newHash);
    ////        //}
    ////        //private  void OnMessage(string sTicket, string sUser, string sMessage)
    ////        //{
    ////        //    Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
    ////        //    newHash.Add("Event", "OnMessage");
    ////        //    newHash.Add("Ticket", sTicket);
    ////        //    newHash.Add("User", sUser);
    ////        //    newHash.Add("Message", sMessage);
    ////        //    RunXDocTemplate("WS", newHash);

    ////        //}

    ////        private void RunWSTemplate(string sEvent, string sTicket, string sUser, string sMessage)
    ////        {
    ////            Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
    ////            newHash.Add("Event", sEvent);
    ////            newHash.Add("Ticket", sTicket);
    ////            newHash.Add("User", sUser);
    ////            newHash.Add("Message", sMessage);
    ////            RunXDocTemplate("WS", newHash);

    ////        }

    ////        private string RunXDocTemplate(string strTemplateName, Hashtable hParameters)
    ////        {
    ////            StringBuilder sbResponse = new StringBuilder();
    ////            string sExit = "";
    ////            string sResponse = "";
    ////            try
    ////            {
    ////                string sResult = xrun.RunTemplate(strTemplateName, hParameters, sbResponse, ref sExit);
    ////                sResponse = sbResponse.ToString();
    ////                if (xrun.ExitValue != "") sExit = xrun.ExitValue;
    ////            }
    ////            catch (Exception ex)
    ////            {
    ////                sExit = ex.Message;
    ////            }
    //////            m_AppCache = xrun.AppCache;
    ////            if (sExit == "")
    ////                return sResponse;
    ////            else
    ////                return sExit;
    ////        }

    ////        private string myReadContent(HttpRequest req)
    ////        {
    ////            string postedData = "";
    ////            int iContentLength = req.ContentLength;
    ////            //if (iContentLength < 5000)
    ////            //{
    ////            StreamReader reader = new StreamReader(req.InputStream, req.ContentEncoding);
    ////            postedData = reader.ReadToEnd().Trim();
    ////            return postedData;
    ////            //}
    ////            //else
    ////            //    return "[[datasize exceeds limit]]";
    ////        }
    ////        private  string getTicket(HttpRequest req)
    ////        {
    ////            string sTicket = "";
    ////            NameValueCollection col = req.Params;
    ////            if (col["ticket"] != null)
    ////                sTicket = col["ticket"].ToString();
    ////            return sTicket;
    ////        }
    ////    }
    //public class User

    //{

    //    public string ConnectionId { get; set; }

    //    public string Name { get; set; }

    //    public string Image { get; set; }

    //}

    //public struct jsonMessage

    //{

    //    public string Type;

    //    public string Message;

    //    public User mUser;

    //}
    //public class WSHandler : IHttpHandler

    //{

    //    private uint BufferSize = 10240;

    //    //private ConcurrentDictionary<string, User> _users = new ConcurrentDictionary<string, User>();

    //    private ConcurrentDictionary<string, AspNetWebSocketContext> clients = new ConcurrentDictionary<string, AspNetWebSocketContext>();

    //    public void ProcessRequest(HttpContext context)

    //    {

    //        if (context.IsWebSocketRequest)

    //        {

    //            context.AcceptWebSocketRequest(Connect);

    //            //context.AcceptWebSocketRequest(WebSocketRequestHandler);

    //        }

    //        else

    //        {

    //            StreamReader reader = new StreamReader(context.Request.InputStream);

    //            string requestFromPost = reader.ReadToEnd();

    //            BroadCast("ServerEvent: " + requestFromPost);

    //            context.Response.StatusCode = 200;

    //            context.Response.Write("Message broadcasted: " + context.Request.ContentLength.ToString() + requestFromPost);

    //        }

    //    }

    //    public bool IsReusable

    //    {

    //        get { return true; }

    //    }

    //    private async Task Connect(AspNetWebSocketContext context)

    //    {

    //        var guid = Guid.NewGuid().ToString();

    //        clients.TryAdd(guid, context);

    //        var buffer = new byte[BufferSize];

    //        while (context.WebSocket.State == System.Net.WebSockets.WebSocketState.Open)

    //        {

    //            var cts = new CancellationTokenSource();

    //            var segment = new ArraySegment<byte>(buffer);

    //            try

    //            {

    //                var result = await context.WebSocket.ReceiveAsync(segment, cts.Token);

    //                switch (result.MessageType)

    //                {

    //                    case System.Net.WebSockets.WebSocketMessageType.Text:

    //                        var message = System.Text.Encoding.UTF8.GetString(buffer, 0, result.Count);

    //                        jsonMessage m = new jsonMessage();

    //                        m.Message = message;

    //                        //await SendMessage(guid, m.Message);

    //                        m.Type = "Echo";

    //                        var t = MessageReceived(guid, m);

    //                        break;

    //                    case System.Net.WebSockets.WebSocketMessageType.Close:

    //                        OnSocketClosed(guid);

    //                        break;

    //                    default:

    //                        throw new NotSupportedException();

    //                }

    //            }

    //            catch (Exception ex)

    //            {

    //                Console.WriteLine(ex.Message);

    //                OnSocketClosed(guid);

    //            }

    //        }

    //        //OnSocketClosed(guid);

    //    }

    //    //private async Task SendMessage<T>(string connectionId, T data)

    //    private async Task SendMessage(string connectionId, string message)

    //    {

    //        //var message = JsonConvert.SerializeObject(data);

    //        var ws = clients[connectionId].WebSocket;

    //        var buffer = System.Text.Encoding.UTF8.GetBytes(message);

    //        var cts = new CancellationTokenSource();

    //        if (ws.State == System.Net.WebSockets.WebSocketState.Open)

    //        {

    //            await ws.SendAsync(new ArraySegment<byte>(buffer), System.Net.WebSockets.WebSocketMessageType.Text, true, cts.Token);

    //        }

    //    }

    //    //private async Task BroadCast<T>(T data)

    //    private async Task BroadCast(string message)

    //    {

    //        //var message = JsonConvert.SerializeObject(data);

    //        var buffer = new ArraySegment<byte>(System.Text.Encoding.UTF8.GetBytes(message));

    //        var tasks = new List<Task>();

    //        var cts = new CancellationTokenSource();

    //        foreach (var ws in clients.Values)

    //        {

    //            if (ws.WebSocket.State == System.Net.WebSockets.WebSocketState.Open)

    //            {

    //                tasks.Add(ws.WebSocket.SendAsync(buffer, System.Net.WebSockets.WebSocketMessageType.Text, true, cts.Token));

    //            }

    //        }

    //        await Task.WhenAll(tasks);

    //    }

    //    private void OnSocketClosed(string connectionId)

    //    {

    //        AspNetWebSocketContext _;

    //        clients.TryRemove(connectionId, out _);

    //        //User user;

    //        //_users.TryRemove(connectionId, out user);

    //        //var t = BroadCast(new RemoveUserMessage { Users = new string[] { connectionId } });

    //        //var t = BroadCast("{ Users = [ { "+ connectionId.ToString() + " } ]");

    //        var t = BroadCast("{}");

    //    }

    //    //private async Task MessageReceived(string connectionId, string data)

    //    private async Task MessageReceived(string connectionId, jsonMessage baseMessage)

    //    {

    //        //var baseMessage = JsonConvert.DeserializeObject<Message>(data);

    //        switch (baseMessage.Type)

    //        {

    //            case "Echo":

    //                {

    //                    await SendMessage(connectionId, baseMessage.Message);

    //                    break;

    //                }

    //            case "Broadcast":

    //                {

    //                    await BroadCast("{ Users = [ { } ] }");

    //                    break;

    //                }

    //            case "User.Connect":

    //                {

    //                    //var message = JsonConvert.DeserializeObject<ConnectMessage>(data);

    //                    //var user = message.User;

    //                    //jsonMessage.mUser = baseMessage.mUser;

    //                    //user = baseMessage.User;

    //                    baseMessage.mUser.ConnectionId = connectionId;

    //                    //user.ConnectionId = connectionId;

    //                    var users = new List<User>();

    //                    //foreach (var u in _users.Values)

    //                    //{

    //                    // users.Add(u);

    //                    //}

    //                    //_users.TryAdd(connectionId, user);

    //                    //await BroadCast(new AddUserMessage { Users = new User[] { user } });

    //                    await BroadCast("{ Users = [ { } ] }");

    //                    if (users.Any())

    //                    {

    //                        //await SendMessage(connectionId, new AddUserMessage { Users = users.ToArray() });

    //                        await SendMessage(connectionId, "{ Users = [] }");

    //                    }

    //                }

    //                break;

    //            case "Chat.Say":

    //                {

    //                    //var message = JsonConvert.DeserializeObject<ChatMessage>(data);

    //                    //var t = BroadCast(message);

    //                    var t = BroadCast("{}");

    //                }

    //                break;

    //            default:

    //                throw new NotSupportedException(baseMessage.Type);

    //        }

    //    }

    //}

    //public class XDocWebSocket : IHttpHandler, IReadOnlySessionState

    //{
    //    WSClients wsClient = new WSClients();

    //    public void ProcessRequest(HttpContext context)
    //    {
    //        if (context.IsWebSocketRequest)
    //        {

    //            //string sTicket = getTicket(context.Request);

    //            context.AcceptWebSocketRequest(wsClient.Connect);
    //            //context.AcceptWebSocketRequest(WebSocketRequestHandler);
    //        }
    //        //else
    //        //{
    //        //    StreamReader reader = new StreamReader(context.Request.InputStream);
    //        //    string requestFromPost = reader.ReadToEnd();
    //        //    BroadCast("ServerEvent: " + requestFromPost);
    //        //    context.Response.StatusCode = 200;
    //        //    context.Response.Write("Message broadcasted: " + context.Request.ContentLength.ToString() + requestFromPost);
    //        //}
    //    }

    //    public bool IsReusable
    //    {
    //        get { return true; }
    //    }

    //}

    ////https://www.w3schools.com/html/tryit.asp?filename=tryhtml_intro
    ////    <!DOCTYPE HTML>
    ////<html>
    ////   <head>

    ////      <script type = "text/javascript" >
    ////         function WebSocketTest()
    ////    {
    ////        if ("WebSocket" in window)
    ////            {
    ////            alert("WebSocket is supported by your Browser!");

    ////            // Let us open a web socket
    ////            var ws = new WebSocket("ws://localhost/XDoc5S/?ticket=12345678901");

    ////            ws.onopen = function()
    ////               {
    ////                // Web Socket is connected, send data using send()
    ////                ws.send("Message to send");
    ////                alert("Message is sent...");
    ////            };

    ////            ws.onmessage = function(evt)
    ////               {
    ////                var received_msg = evt.data;
    ////                alert("Message is received...");
    ////            };

    ////            ws.onclose = function()
    ////               {
    ////                // websocket is closed.
    ////                alert("Connection is closed...");
    ////            };

    ////            window.onbeforeunload = function(event) {
    ////                socket.close();
    ////            };
    ////        }

    ////            else
    ////            {
    ////            // The browser doesn't support WebSocket
    ////            alert("WebSocket NOT supported by your Browser!");
    ////        }
    ////    }
    ////      </script>

    ////   </head>
    ////   <body>

    ////      <div id = "sse" >
    ////         < a href="javascript:WebSocketTest()">Run WebSocket</a>
    ////       </div>

    ////   </body>
    ////</html>



    ////var ws1 = new WebSocket("ws://localhost/XDoc5S/?ticket=12345678901");
    ////ws1.onopen = function() { console.log("Websocket connected"); };
    ////ws1.onmessage = function(evt) { console.log("Websocket message received: " + evt.data); };
    ////ws1.onclose = function() { console.log("Connection is closed..."); };
    ////window.onbeforeunload = function(event) { ws1.close(); };

    ////ws1.send("hello webservice");

}