﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

namespace XDocServices
{
    public class XDocServiceA : HttpTaskAsyncHandler, IReadOnlySessionState
    {
        //public override async Task ProcessRequestAsync(HttpContext context)
        //{

        //}
        public override bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public override Task ProcessRequestAsync(HttpContext p_ctx)
        {
            return doit(p_ctx);
        }

        static int _count = 0;
        async Task doit1(HttpContext p_ctx)
        {
            await Task.Delay(5000);
            p_ctx.Response.Write("doit " + (++_count).ToString());
        }
        async Task doit(HttpContext p_ctx)
        {

            XDocService4R xdoc4 = new XDocService4R();
            //            var result = await Task.Run(() => xdoc4.ProcessRequest(p_ctx));
            await Task.Run(() => xdoc4.ProcessRequest(p_ctx));
            //await Task.Delay(5000);
            //p_ctx.Response.Write("doit " + (++_count).ToString());
        }
    }
    //class WebHandler : IHttpHandler
    //    {

    //        public bool IsReusable { get { return true; } }

    //        private static bool Initialized = false;

    //        public void ProcessRequest(HttpContext context)
    //        {
    //            if (!Initialized)
    //            {
    //                Program.Init();

    //                Initialized = true;
    //            }

    //            HttpRequest request = context.Request;
    //            HttpResponse response = context.Response;
    //            string content = new System.IO.StreamReader(request.InputStream, request.ContentEncoding).ReadToEnd();

    //            string responseString = "";

    //            string[] segments = request.Path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

    //            if (segments.Length >= 1 && segments[0] == "admin")
    //            {
    //                responseString = InterfaceController.Handle(request, response, content, segments);
    //            }
    //            else
    //            {
    //                responseString = Program.Server.Handle(request, response, content);
    //            }

    //            if (responseString != null)
    //            {
    //                response.AppendHeader("Access-Control-Allow-Origin", "*");
    //                response.AppendHeader("Access-Control-Allow-Headers", "X-Requested-With");
    //                byte[] buffer = Encoding.UTF8.GetBytes(responseString);

    //                System.IO.Stream output = response.OutputStream;

    //                output.Write(buffer, 0, buffer.Length);
    //                output.Close();
    //            }
    //        }
    //    }

}
