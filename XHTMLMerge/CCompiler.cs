﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace XHTMLMerge
{
    class CCompiler
    {

        #region compile templates
//        Regex regexXDoc = new Regex(@"(#((TDEL)|(TSET)|(TSETI)|(TGET)|(SPARP)|(SPARA)|(SPAR)|(JSORT)|(JINS)|(JAPP)|(JUPD)|(JDEL)|(JSET)|(JPAR)|(JDATA)|(JLOOP)|(JLOOPC)|(JARRAY)|(EXIT)|(ENDJLOOP)|(JKEYS)|(ENDJKEYS)|(GETPARALL)|(GETPARSTRICT)|(GETPAR)|(GETCFGPAR)|(GETPARREPLACE)|(GETDEFPAR)|(GETITEMPAR)|(FINDITEMPAR)|(APPVAL)|(APPENDVAL)|(APPFLD)|(APPENDFLD)|(DEL)|(DELFROM)|(DELALL)|(DELITEM)|(SETPAR)|(SETPAREXT)|(SETFLD)|(SETFLDEXT)|(SETVAL)|(SETNULLPAR)|(SETNULLPAREXT)|(SETNULLFLD)|(SETNULLFLDEXT)|(SETNULLVAL)|(MSG)|(THROW)|(THROWPAR)|(FILE)|(COUNT)|(UDF)|(DEFPAR)|(DEFQRY)|(STARTBLOCK)|(ENDBLOCK)|(XDOCBLOCK)|(XDOCCOMMENT)|(STARTCOMMENT)|(ENDCOMMENT)|(XPATH)|(XPATHDATA)|(TXPATH)|(TXPATHDATA)|(JPATH)|(JPATHDATA)|(REP)|(REPDATA)|(QRY)|(QRYDATA)|(IIF)|(IF)|(ELSEIF)|(IFI)|(ELSEIFI)|(PAR)|(FLD)|(FLDBIN)|(ATTACH)|(ENDXPATH)|(ENDJPATH)|(ENDREP)|(ENDIF)|(ENDQRY)|(INCLUDEONCE)|(PARINCLUDE)|(INCLUDE)|(IMPORT)|(IMPORTSET)|(IMPORTFLD)|(EXPORT)|(DELETETEMPLATE)|(TRANSFERLIKE)|(TRANSFER)|(CFG)|(USEPARAMETERS)|(FUNC))[^#\x28]*(\x28[^\x29]*\x29)?(.\x28[^\x29]*\x29)?[^#\x28]*#)|[^#]+", RegexOptions.IgnoreCase);
        Regex regexXDoc = new Regex(@"(#((TDEL)|(TSET)|(TSETI)|(TGET)|(SPARP)|(SPARA)|(SPAR)|(JSORT)|(JINS)|(JAPP)|(JUPD)|(JDEL)|(JSET)|(JPAR)|(JDATA)|(JLOOP)|(JLOOPC)|(JARRAY)|(EXIT)|(ENDJLOOP)|(JKEYS)|(ENDJKEYS)|(GETPARALL)|(GETPARSTRICT)|(GETPAR)|(GETCFGPAR)|(GETPARREPLACE)|(GETDEFPAR)|(GETITEMPAR)|(FINDITEMPAR)|(APPVAL)|(APPENDVAL)|(APPFLD)|(APPENDFLD)|(DEL)|(DELFROM)|(DELALL)|(DELITEM)|(SETPAR)|(SETPAREXT)|(SETFLD)|(SETFLDEXT)|(SETVAL)|(SETNULLPAR)|(SETNULLPAREXT)|(SETNULLFLD)|(SETNULLFLDEXT)|(SETNULLVAL)|(MSG)|(THROW)|(THROWPAR)|(FILE)|(COUNT)|(UDF)|(DEFPAR)|(DEFQRY)|(STARTBLOCK)|(ENDBLOCK)|(XDOCBLOCK)|(XDOCCOMMENT)|(STARTCOMMENT)|(ENDCOMMENT)|(XPATH)|(XPATHDATA)|(TXPATH)|(TXPATHDATA)|(JPATH)|(JPATHDATA)|(REP)|(REPDATA)|(QRY)|(QRYDATA)|(IIF)|(IF)|(ELSEIF)|(IFI)|(ELSEIFI)|(PAR)|(FLD)|(FLDBIN)|(ATTACH)|(ENDXPATH)|(ENDJPATH)|(ENDREP)|(ENDIF)|(ENDQRY)|(INCLUDEONCE)|(PARINCLUDE)|(INCLUDE)|(IMPORT)|(IMPORTSET)|(IMPORTFLD)|(EXPORT)|(DELETETEMPLATE)|(TRANSFERLIKE)|(TRANSFER)|(CFG)|(USEPARAMETERS)|(FUNC))[^#\x28]*(\x28[^\x29]*\x29)?(?s:.)*?(\x28[^\x29]*\x29)*[^#\x28]*#)|[^#]+", RegexOptions.IgnoreCase);
        ////////Regex regexStartEnd = new Regex(@"(#start#(?:(?!#end#).|\n)*(?=#end#)#end#)", RegexOptions.IgnoreCase | RegexOptions.Multiline);

////#STARTCOMMENT#(.|\r|\n)*?#ENDCOMMENT#

////#STARTCOMMENT#(?:(?!#ENDCOMMENT#).|\r|\n)*#ENDCOMMENT#

        const string
            START_BLOCK = "STARTBLOCK",
            END_BLOCK = "ENDBLOCK",
            START_COMMENT = "STARTCOMMENT",
            END_COMMENT = "ENDCOMMENT";

        //string m_text = "";
        //bool m_skipCurrentMatch = false;
        ////bool m_commentBlock = false;
        ////bool m_ignoreBlock = false;
        //int m_partialTextOffset = 0;

        private bool BeginOfLine(string s)
        {
            int count = 0;
            int position = 0;
            while ((position = s.IndexOf('\n', position)) != -1)
            {
                count++;
                position++;
            }
            s.Substring(s.IndexOf('\n', count));
            return true;
        }

        private  int LinesCountIndexOf(string s)
        {
            int count = 0;
            int position = 0;
            while ((position = s.IndexOf('\n', position)) != -1)
            {
                count++;
                position++;         
            }
            return count;
        }
        private void CompileRender_Text(string text, int iLine, StringBuilder sJSON, StringBuilder sbWarning)
        {
            string m_text = text;
            m_text = m_text.Replace("\t", "");
            if (m_text.StartsWith("\r\n"))
                m_text = m_text.Substring(2);
            if (m_text != "")
            {
                sJSON.Append("{");
                sJSON.Append("\"T\":\"Text\"");
                sJSON.Append(",\"L\":"); sJSON.Append(iLine.ToString()); sJSON.Append("");
                sJSON.Append(",\"P1\":\""); sJSON.Append(Utils._JsonEscape(text)); sJSON.Append("\"");
                sJSON.Append("},");
            }
        }
        private void CompileRender_Cmd(string cmd,string text, int iLine, StringBuilder sJSON, StringBuilder sbWarning)
        {
            if (text != "")
            {
                sJSON.Append("{");
                sJSON.Append("\"T\":\""); sJSON.Append(Utils._JsonEscape(cmd)); sJSON.Append("\"");
                sJSON.Append(",\"L\":\""); sJSON.Append(iLine.ToString()); sJSON.Append("\"");
                sJSON.Append(",\"P1\":\""); sJSON.Append(Utils._JsonEscape(text)); sJSON.Append("\"");
                sJSON.Append("},");
            }
        }
        // SAMPLE REGEX REPLACE
        ////Hashtable m_blocks = new Hashtable();
        ////string source = "Dear [to name], [your name] has decided to share this [link]"; 
        ////Regex regex1 = new Regex(@"\[[^\]]+\]");
        ////string res = regex1.Replace(source,delegate(Match m) {
        ////    m_blocks.Add(m.Index.ToString(), m.Value);
        ////    return "#BLOCK." + m.Index.ToString() + "#";
        ////});
        //public string CompileWarning_3(string m_textContent, StringBuilder sbCompile)
        //{
        //    StringBuilder sbWarning = new StringBuilder();

        //    Regex regexBlocks = new Regex(@"(#STARTBLOCK#(?:(?!#ENDBLOCK#).|\n)*(?=#ENDBLOCK#)#ENDBLOCK#)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        //    Hashtable m_blocks = CollectionsUtil.CreateCaseInsensitiveHashtable(); //new Hashtable();
        //    m_textContent = regexBlocks.Replace(m_textContent, delegate(Match m)
        //    {
        //        m_blocks.Add(m.Index.ToString()+"#", m.Value);
        //        return "#XDOCBLOCK." + m.Index.ToString() + "#";
        //    });
        //    Regex regexComments = new Regex(@"(#STARTCOMMENT#(?:(?!#ENDCOMMENT#).|\n)*(?=#ENDCOMMENT#)#ENDCOMMENT#)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        //    Hashtable m_comments = CollectionsUtil.CreateCaseInsensitiveHashtable(); //new Hashtable();
        //    m_textContent = regexComments.Replace(m_textContent, delegate(Match m)
        //    {
        //        m_blocks.Add(m.Index.ToString()+"#", m.Value);
        //        return "#XDOCCOMMENT." + m.Index.ToString() + "#";
        //    });

        //    string hashIndex = "";
        //    int iLine=0;
        //    string[] parts = null;
        //    string sParams = "";
        //    string txtU = "";

        //    try
        //    {
        //        sbCompile.Append("{");
        //        sbCompile.Append("\"T\":\"ROOT\"");
        //        sbCompile.Append(",\"L\":\"-1\"");
        //        sbCompile.Append(",\"C\":[ ");
        //        MatchCollection mc = null;
        //        mc = regexXDoc.Matches(m_textContent);
        //        for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
        //        {
        //            Match m = mc[matchIndex];
        //            string txt = m.Value;
        //            if (txt != "\r\n")
        //            {
        //                //System.Diagnostics.Debug.WriteLine("cmd " + matchIndex.ToString ()+ ": " + m.Value);
        //                if (!txt.StartsWith("#"))
        //                    CompileRender_Text(txt,iLine, sbCompile, sbWarning);
        //                else
        //                {
        //                    txtU = txt.ToUpper();

        //                    if (txtU.StartsWith("#XDOCBLOCK."))
        //                    {
        //                        hashIndex = txt.Substring("#XDOCBLOCK.".Length - 0);
        //                        txt = (string)m_blocks[hashIndex];
        //                        CompileRender_Text(txt, iLine, sbCompile, sbWarning);
        //                    }
        //                    else if (txtU.StartsWith("#XDOCCOMMENT."))
        //                    {
        //                        hashIndex = txt.Substring("#XDOCCOMMENT.".Length - 0);
        //                        txt = (string)m_comments[hashIndex];
        //                        CompileRender_Text(txt, iLine, sbCompile, sbWarning);
        //                    }
        //                    else if (txtU.StartsWith("#JLOOP"))
        //                    {
        //                        parts = Utils.MySplit(txt);
        //                        sbCompile.Append("{");
        //                        sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
        //                        sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
        //                        if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
        //                        if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
        //                        if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
        //                        if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
        //                        if (parts.Length > 5) { sbCompile.Append(",\"P5\":\""); sbCompile.Append(Utils._JsonEscape(parts[5])); sbCompile.Append("\""); };
        //                        if (parts.Length > 6) { sbCompile.Append(",\"P6\":\""); sbCompile.Append(Utils._JsonEscape(parts[6])); sbCompile.Append("\""); };
        //                        if (parts.Length > 7) { sbCompile.Append(",\"P7\":\""); sbCompile.Append(Utils._JsonEscape(parts[7])); sbCompile.Append("\""); };
        //                        if (parts.Length > 8) { sbCompile.Append(",\"P8\":\""); sbCompile.Append(Utils._JsonEscape(parts[8])); sbCompile.Append("\""); };
        //                        if (parts.Length > 9) { sbCompile.Append(",\"P9\":\""); sbCompile.Append(Utils._JsonEscape(parts[9])); sbCompile.Append("\""); };
        //                        sbCompile.Append(",\"C\":[ ");
        //                    }
        //                    else if (txtU.StartsWith("#JKEYS"))
        //                    {
        //                        parts = Utils.MySplit(txt);
        //                        sbCompile.Append("{");
        //                        sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
        //                        sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
        //                        if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
        //                        if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
        //                        if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
        //                        if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
        //                        if (parts.Length > 5) { sbCompile.Append(",\"P5\":\""); sbCompile.Append(Utils._JsonEscape(parts[5])); sbCompile.Append("\""); };
        //                        if (parts.Length > 6) { sbCompile.Append(",\"P6\":\""); sbCompile.Append(Utils._JsonEscape(parts[6])); sbCompile.Append("\""); };
        //                        if (parts.Length > 7) { sbCompile.Append(",\"P7\":\""); sbCompile.Append(Utils._JsonEscape(parts[7])); sbCompile.Append("\""); };
        //                        if (parts.Length > 8) { sbCompile.Append(",\"P8\":\""); sbCompile.Append(Utils._JsonEscape(parts[8])); sbCompile.Append("\""); };
        //                        if (parts.Length > 9) { sbCompile.Append(",\"P9\":\""); sbCompile.Append(Utils._JsonEscape(parts[9])); sbCompile.Append("\""); };
        //                        sbCompile.Append(",\"C\":[ ");
        //                    }
        //                    else if (txt.StartsWith("#INCLUDE."))
        //                    {
        //                        sParams = "";
        //                        parts = Utils.MySplitParams(txt, out sParams);
        //                        sbCompile.Append("{");
        //                        sbCompile.Append("\"T\":\""); sbCompile.Append("INCLUDE"); sbCompile.Append("\"");
        //                        sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
        //                        if (sParams != "") { sbCompile.Append(",\"P\":[ "); sbCompile.Append(Utils._JsonEscape(sParams)); sbCompile.Append("]"); };
        //                        sbCompile.Append("},");
        //                    }
        //                    else if (txt.StartsWith("#PARINCLUDE."))
        //                    {
        //                        sParams = "";
        //                        parts = Utils.MySplitParams(txt, out sParams);
        //                        sbCompile.Append("{");
        //                        sbCompile.Append("\"T\":\""); sbCompile.Append("PARINCLUDE"); sbCompile.Append("\"");
        //                        sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
        //                        if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
        //                        if (sParams != "") { sbCompile.Append(",\"P\":[ "); sbCompile.Append(Utils._JsonEscape(sParams)); sbCompile.Append("]"); };
        //                        sbCompile.Append("},");
        //                    }
        //                    else if (txt.StartsWith("#IMPORT."))
        //                    {
        //                        sParams = "";
        //                        parts = Utils.MySplitParams(txt, out sParams);
        //                        sbCompile.Append("{");
        //                        sbCompile.Append("\"T\":\""); sbCompile.Append("IMPORT"); sbCompile.Append("\"");
        //                        sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
        //                        if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
        //                        if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
        //                        if (sParams != "") { sbCompile.Append(",\"P\":[ "); sbCompile.Append(Utils._JsonEscape(sParams)); sbCompile.Append("]"); };
        //                        sbCompile.Append("},");
        //                    }
        //                    else if (txt.StartsWith("#IMPORTSET."))
        //                    {
        //                        sParams = "";
        //                        parts = Utils.MySplitParams(txt, out sParams);
        //                        sbCompile.Append("{");
        //                        sbCompile.Append("\"T\":\""); sbCompile.Append("IMPORTSET"); sbCompile.Append("\"");
        //                        sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
        //                        if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
        //                        if (sParams != "") { sbCompile.Append(",\"P\":[ "); sbCompile.Append(Utils._JsonEscape(sParams)); sbCompile.Append("]"); };
        //                        sbCompile.Append("},");
        //                    }
        //                    else if (txt.StartsWith("#IF"))
        //                    {
        //                        sParams = "";
        //                        parts = Utils.MySplitParams(txt, out sParams);
        //                        sbCompile.Append("{");
        //                        sbCompile.Append("\"T\":\""); sbCompile.Append("IF"); sbCompile.Append("\"");
        //                        sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
        //                        sbCompile.Append(",\"C\":[ ");
        //                        sbCompile.Append("{");
        //                        sbCompile.Append("\"T\":\"SUBIF1\"");
        //                        sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
        //                        if (sParams != "") { sbCompile.Append(",\"P\":[ "); sbCompile.Append(Utils._JsonEscape(sParams)); sbCompile.Append("]"); };
        //                        sbCompile.Append(",\"C\":[ ");
        //                    }
        //                    else if (txt.StartsWith("#ELSEIF"))
        //                    {
        //                        sParams = "";
        //                        parts = Utils.MySplitParams(txt, out sParams);
        //                        sbCompile.Remove(sbCompile.Length - 1, 1);sbCompile.Append("]},");
        //                        sbCompile.Append("{");
        //                        sbCompile.Append("\"T\":\"SUBIF\"");
        //                        sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
        //                        if (sParams != "") { sbCompile.Append(",\"P\":[ "); sbCompile.Append(Utils._JsonEscape(sParams)); sbCompile.Append("]"); };
        //                        sbCompile.Append(",\"C\":[ ");
        //                    }
        //                    else if (txt.StartsWith("#IFI"))
        //                    {
        //                        parts = Utils.MySplit(txt);
        //                        sbCompile.Append("{");
        //                        sbCompile.Append("\"T\":\""); sbCompile.Append("IFI"); sbCompile.Append("\"");
        //                        sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
        //                        sbCompile.Append(",\"C\":[ ");
        //                        sbCompile.Append("{");
        //                        sbCompile.Append("\"T\":\"SUBIFI1\"");
        //                        sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
        //                        if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
        //                        sbCompile.Append(",\"C\":[ ");
        //                    }
        //                    else if (txt.StartsWith("#ELSEIFI"))
        //                    {
        //                        parts = Utils.MySplit(txt);
        //                        sbCompile.Remove(sbCompile.Length - 1, 1);sbCompile.Append("]},");
        //                        sbCompile.Append("{");
        //                        sbCompile.Append("\"T\":\"SUBIFI\"");
        //                        sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
        //                        if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
        //                        sbCompile.Append(",\"C\":[ ");
        //                    }
        //                    else if (txt.StartsWith("#ENDJLOOP"))
        //                    {
        //                        sbCompile.Remove(sbCompile.Length - 1, 1);sbCompile.Append("]},");
        //                    }
        //                    else if (txt.StartsWith("#ENDJKEYS"))
        //                    {
        //                        sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]},");
        //                    }
        //                    else if (txt.StartsWith("#ENDIF"))
        //                    {
        //                        sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]},");
        //                        sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]},");
        //                    }
        //                    else
        //                    {
        //                        parts = Utils.MySplit(txt);
        //                        sbCompile.Append("{");
        //                        sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
        //                        sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
        //                        if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
        //                        if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
        //                        if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
        //                        if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
        //                        if (parts.Length > 5) { sbCompile.Append(",\"P5\":\""); sbCompile.Append(Utils._JsonEscape(parts[5])); sbCompile.Append("\""); };
        //                        if (parts.Length > 6) { sbCompile.Append(",\"P6\":\""); sbCompile.Append(Utils._JsonEscape(parts[6])); sbCompile.Append("\""); };
        //                        if (parts.Length > 7) { sbCompile.Append(",\"P7\":\""); sbCompile.Append(Utils._JsonEscape(parts[7])); sbCompile.Append("\""); };
        //                        if (parts.Length > 8) { sbCompile.Append(",\"P8\":\""); sbCompile.Append(Utils._JsonEscape(parts[8])); sbCompile.Append("\""); };
        //                        if (parts.Length > 9) { sbCompile.Append(",\"P9\":\""); sbCompile.Append(Utils._JsonEscape(parts[9])); sbCompile.Append("\""); };
        //                        sbCompile.Append("},");
        //                    }
        //                }
        //                iLine += LinesCountIndexOf(txt);
        //            }
        //            else
        //            {
        //                iLine++;
        //            }
        ////public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        ////{
        ////    sJSON.Append("{");
        ////    sJSON.Append("\"Type\":\"JPAR\"");
        ////    sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
        ////    sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_ParamName)); sJSON.Append("\"");
        ////    sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_Source)); sJSON.Append("\"");
        ////    sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_JPath)); sJSON.Append("\"");
        ////    sJSON.Append("}");
        ////}

        ////public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        ////{
        ////    sJSON.Append("{");
        ////    sJSON.Append("\"Type\":\"JLOOP\"");
        ////    sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
        ////    sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_ParamName)); sJSON.Append("\"");
        ////    sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_Source)); sJSON.Append("\"");
        ////    sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_JPath)); sJSON.Append("\"");
        ////    sJSON.Append(",\"Value2\":\""); sJSON.Append(Utils._JsonEscape(m_PageSize)); sJSON.Append("\"");
        ////    sJSON.Append(",\"Value3\":\""); sJSON.Append(Utils._JsonEscape(m_PageNr)); sJSON.Append("\"");
        ////    sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(m_ErrorParamName)); sJSON.Append("\"");
        ////    sJSON.Append(",\"ID\":\""); sJSON.Append(Utils._JsonEscape(m_Condition )); sJSON.Append("\"");
        ////    //sJSON.Append(",\"ParSource\":\""); sJSON.Append(Utils._JsonEscape(m_isParSource)); sJSON.Append("\"");
        //            //sJSON.Append(",\"Commands\":[ ");
        //            //bool isFirst = true;
        //            //foreach (CCmd cmd in m_vChildCmds)
        //            //{
        //            //    if (isFirst) isFirst = false; else sJSON.Append(",");
        //            //    cmd.GetJson(sJSON, sbWarning);
        //            //}
        //            //sJSON.Append("]");
        ////    sJSON.Append("}");
        ////}

        ////    if (m_Conditions == "CmdCollection")
        ////    {
        ////        sJSON.Append("{");
        ////        sJSON.Append("\"Type\":\"IF\"");
        ////        sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
        ////        sJSON.Append(",\"Params\":[ ");
        ////        bool isFirst = true;
        ////        foreach (CCmd condCmd in this.m_conditionCmds)
        ////        {
        ////            if (isFirst) isFirst = false; else sJSON.Append(",");
        ////            condCmd.GetJson(sJSON, sbWarning);
        ////        }
        ////        sJSON.Append("]");
        ////        base.GetJsonChilds(sJSON, sbWarning);
        ////        if (m_ElseIfCmd != null)
        ////        {
        ////            sJSON.Append(",\"ElseIf\":");
        ////            m_ElseIfCmd.GetJson(sJSON, sbWarning);
        ////        }
        ////        sJSON.Append("}");
        ////    }
        ////    else
        ////    {
        ////        sJSON.Append("{");
        ////        sJSON.Append("\"Type\":\"IF\"");
        ////        sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
        ////        sJSON.Append(",\"Name\":\""); sJSON.Append("IFI"); sJSON.Append("\"");
        ////        sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_Conditions.ToString()); sJSON.Append("\"");
        ////        base.GetJsonChilds(sJSON, sbWarning);
        ////        if (m_ElseIfCmd != null)
        ////        {
        ////            sJSON.Append(",\"ElseIf\":");
        ////            m_ElseIfCmd.GetJson(sJSON, sbWarning);
        ////        }
        ////        sJSON.Append("}");
        ////    }
        //            //m_text = m_text.Replace("\t", "");
        //            ////                m_text = m_text.Replace("\n", "");
        //            //sJSON.Append("{");
        //            //sJSON.Append("\"Type\":\"Text\"");
        //            //sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
        //            //sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_text)); sJSON.Append("\"");
        //            //sJSON.Append("}");

        


        //            //this call can modify the m_skipCurrentMatch member variable
        //     ///       ParseNewCommand(sbCompile,m.Value,m.Index + m_partialTextOffset, true, false)
        //            //CCmd cmd = GetNewCmd(m.Value, m.Index + m_partialTextOffset, true, false);
        //            ////if the m_skipCurrentMatch has been modified, skip the current cycle.
        //            //if (m_skipCurrentMatch == true)
        //            //    if ((cmd.Type != CommandType.TEXTCommand) || ((cmd as CTextCmd).Text != ""))
        //            //    {
        //            //        ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
        //            //        continue;
        //            //    }
        //            //if (cmd != null)
        //            //{
        //            //    if (cmd.IsBlockComand)
        //            //    {
        //            //        if (cmd.Type == CommandType.ElseIFCommand)
        //            //        {
        //            //            CCmd cmdIf = (CCmd)m_stack.Pop();
        //            //            if (!(cmdIf is CIFCmd))
        //            //                throw new Exception("ELSEIF command at line " + GetLine(m.Index + m_partialTextOffset).ToString() + " does not have a correspondig IF command associated.");
        //            //            else
        //            //            {
        //            //                if (cmdIf.Type == CommandType.IFCommand)
        //            //                    ((CCmd)m_stack.Peek()).ChildCommands.Add(cmdIf);
        //            //            }
        //            //            ((CIFCmd)cmdIf).ElseIfCommand = (CIFCmd)cmd;

        //            //        }
        //            //        if ((cmd.Type != CommandType.TEXTCommand) || ((cmd as CTextCmd).Text != ""))
        //            //            m_stack.Push(cmd);
        //            //    }
        //            //    else
        //            //    {
        //            //        cmd.EndIndex = m.Index + m_partialTextOffset + m.Value.Length;
        //            //        if ((cmd.Type != CommandType.TEXTCommand) || ((cmd as CTextCmd).Text != ""))
        //            //            ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
        //            //        //else
        //            //        //    ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
        //            //    }
        //            //}
        //            //else // this means we're dealing with a closing #*# tag
        //            //{
        //            //    bool bCloseOK = VerifyClosingTag(m);
        //            //    if (bCloseOK)
        //            //    {
        //            //        CCmd cmdOK = (CCmd)m_stack.Pop();
        //            //        if (m_stack.Count == 0)
        //            //            throw new Exception("Empty stack!");
        //            //        cmdOK.EndIndex = m.Index + m_partialTextOffset + m.Value.Length;
        //            //        if (cmdOK.Type != CommandType.ElseIFCommand)
        //            //            ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmdOK);
        //            //    }
        //            //    else
        //            //        throw new Exception("The closing tag " + m.Value + " at line " + GetLine(m.Index + m_partialTextOffset).ToString() + " is not valid. Check the command it was intended for!");
        //            //}
        //        }

        //        //root.GetJson(sbCompile, sbWarning);
        //        sbCompile.Remove(sbCompile.Length - 1, 1);sbCompile.Append("]}");
        //        return sbWarning.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        sbCompile.Clear();
        //        sbCompile.Append("error");
        //        return "error:" + ex.Message;
        //    }
        //}

        public string CompileWarning(string m_textContent, StringBuilder sbCompile)
        {
            StringBuilder sbWarning = new StringBuilder();

            try
            {
                sbCompile.Append("{");
                sbCompile.Append("\"T\":\"ROOT\"");
                sbCompile.Append(",\"L\":-1");
                sbCompile.Append(",\"C\":[ ");

                Compile4(m_textContent, sbCompile, sbWarning);
                sbCompile.Remove(sbCompile.Length - 1, 1);sbCompile.Append("]}");

                JToken ob = JToken.Parse(sbCompile.ToString());
                return sbWarning.ToString();
            }
            catch (Exception ex)
            {
                sbCompile.Clear();
                sbCompile.Append("error");
                return "error:" + ex.Message;
            }
        }
        //public string UnCompileCmd(JCmd4 command,int depth, StringBuilder sbUnCompile)
        //{
        //    string sType = command.T.ToUpper();
        //    switch (sType)
        //    {
        //        case "TEXT":
        //            sbUnCompile.Append(command.P1);
        //            break;
        //        case "IF":
        //            break;
        //        case "JLOOP":
        //            break;
        //        case "JKEYS":
        //            break;
        //        case "SPAR":
        //            //sbUnCompile.Append("\r\n#SPAR");
        //            //if (command.Opt1 == "1") sbUnCompile.Append("P");
        //            //if (command.Opt2 == "1") sbUnCompile.Append("A");
        //            //sbUnCompile.Append(".");
        //            //sbUnCompile.Append(command.ParamName);
        //            //sbUnCompile.Append(".");
        //            //sbUnCompile.Append(command.Value);
        //            //sbUnCompile.Append("#\r\n");
        //            break;
        //        default:
        //            sbUnCompile.Append("\r\n#");
        //            sbUnCompile.Append(command.T);
        //            sbUnCompile.Append(".");
        //            sbUnCompile.Append(command.P1);
        //            sbUnCompile.Append(".");
        //            sbUnCompile.Append(command.P2);
        //            sbUnCompile.Append("#\r\n");
        //            break;
        //    }
        //    return "";
        //}
        public string UnCompile4(string m_textContent, StringBuilder sbUnCompile)
        {
            JCmd4 root = null; 
            root = Newtonsoft.Json.JsonConvert.DeserializeObject<JCmd4>(m_textContent);
//            root.Sintax4(sbUnCompile, -1, true);
            root.Sintax4(sbUnCompile, -1, false);
            return "";
        }
        public string Compile4(string m_textContent, StringBuilder sbCompile, StringBuilder sbWarning)
        {
            Regex regexBlocks = new Regex(@"(#STARTBLOCK#(?:(?!#ENDBLOCK#).|\n)*(?=#ENDBLOCK#)#ENDBLOCK#)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Hashtable m_blocks = CollectionsUtil.CreateCaseInsensitiveHashtable(); //new Hashtable();
            m_textContent = regexBlocks.Replace(m_textContent, delegate(Match m)
            {
                m_blocks.Add(m.Index.ToString() + "#", m.Value);
                return "#XDOCBLOCK." + m.Index.ToString() + "#";
//                return "";
            });
            Regex regexComments = new Regex(@"(#STARTCOMMENT#(?:(?!#ENDCOMMENT#).|\n)*(?=#ENDCOMMENT#)#ENDCOMMENT#)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Hashtable m_comments = CollectionsUtil.CreateCaseInsensitiveHashtable();// new Hashtable();
            m_textContent = regexComments.Replace(m_textContent, delegate(Match m)
            {
                m_comments.Add(m.Index.ToString() + "#", m.Value);
                return "#XDOCCOMMENT." + m.Index.ToString() + "#";
 //               return "";
            });

            string hashIndex = "";
            int iLine = 0;
            int iLastLinesCommand = 0;
            string[] parts = null;
            string sParams = "";
            string txtU = "";

            try
            {
                MatchCollection mc = null;
                mc = regexXDoc.Matches(m_textContent);
                for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
                {
                    Match m = mc[matchIndex];
                    sParams = "";
                    string txt = m.Value;
                    if (txt != "\r\n")
                    {
                        //System.Diagnostics.Debug.WriteLine("cmd " + matchIndex.ToString ()+ ": " + m.Value);
                        if (!txt.StartsWith("#"))
                            CompileRender_Text(txt, iLine, sbCompile, sbWarning);
                        else
                        {
                            txtU = txt.ToUpper();

                            if (txtU.StartsWith("#XDOCBLOCK."))
                            {
                                hashIndex = txt.Substring("#XDOCBLOCK.".Length - 0);
                                txt = (string)m_blocks[hashIndex];
                                txt = txt.Substring("#startblock#".Length, txt.Length - "#startblock#".Length - "#endblock#".Length);
                                CompileRender_Cmd("XDOCBLOCK", txt, iLine, sbCompile, sbWarning);
                            }
                            else if (txtU.StartsWith("#XDOCCOMMENT."))
                            {
                                hashIndex = txt.Substring("#XDOCCOMMENT.".Length - 0);
                                txt = (string)m_comments[hashIndex];
                                txt = txt.Substring("#startcomment#".Length, txt.Length - "#startcomment#".Length - "#endcomment#".Length);
                                CompileRender_Cmd("XDOCCOMMENT", txt, iLine, sbCompile, sbWarning);
                            }
                            else if (txtU.StartsWith("#JLOOPC"))
                            {
                                //#JLOOPC.<condition>.<par_name>.<json>.[<jpath>].[<pagesize>].[<err_par>]#
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                string P7 = "", P8 = "", P9 = "";
                                SetEvalCondition(parts[1], ref P7, ref P8, ref P9);
                                if (parts.Length > 1) { sbCompile.Append(",\"P6\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                if (parts.Length > 5) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[5])); sbCompile.Append("\""); };
                                if (parts.Length > 6) { sbCompile.Append(",\"P5\":\""); sbCompile.Append(Utils._JsonEscape(parts[6])); sbCompile.Append("\""); };
                                if (P7 != "") { sbCompile.Append(",\"P7\":\""); sbCompile.Append(Utils._JsonEscape(P7)); sbCompile.Append("\""); };
                                if (P8 != "") { sbCompile.Append(",\"P8\":\""); sbCompile.Append(Utils._JsonEscape(P8)); sbCompile.Append("\""); };
                                if (P9 != "") { sbCompile.Append(",\"P9\":\""); sbCompile.Append(Utils._JsonEscape(P9)); sbCompile.Append("\""); };
                                sbCompile.Append(",\"C\":[ ");
                            }
                            else if (txtU.StartsWith("#JLOOP"))
                            {
                                //#JLOOP.<par_name>.<json>.[<jpath>].[<pagesize>].[<pagenr>].[<err_par>]# 
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                if (parts.Length > 5) { sbCompile.Append(",\"P5\":\""); sbCompile.Append(Utils._JsonEscape(parts[5])); sbCompile.Append("\""); };
                                if (parts.Length > 6) { sbCompile.Append(",\"P6\":\""); sbCompile.Append(Utils._JsonEscape(parts[6])); sbCompile.Append("\""); };
                                sbCompile.Append(",\"C\":[ ");
                            }
                            else if (txt.StartsWith("#ENDJLOOP"))
                            {
                                sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]},");
                            }
                            else if (txtU.StartsWith("#JKEYS"))
                            {
                                parts = Utils.MySplit(txt, 3);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\"");
                                sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\"");
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                sbCompile.Append(",\"C\":[ ");
                            }
                            //else if (txtU.StartsWith("#JKEYS"))
                            //{
                            //    parts = Utils.MySplit(txt);
                            //    sbCompile.Append("{");
                            //    sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                            //    sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                            //    if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                            //    if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                            //    if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                            //    if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                            //    if (parts.Length > 5) { sbCompile.Append(",\"P5\":\""); sbCompile.Append(Utils._JsonEscape(parts[5])); sbCompile.Append("\""); };
                            //    if (parts.Length > 6) { sbCompile.Append(",\"P6\":\""); sbCompile.Append(Utils._JsonEscape(parts[6])); sbCompile.Append("\""); };
                            //    if (parts.Length > 7) { sbCompile.Append(",\"P7\":\""); sbCompile.Append(Utils._JsonEscape(parts[7])); sbCompile.Append("\""); };
                            //    if (parts.Length > 8) { sbCompile.Append(",\"P8\":\""); sbCompile.Append(Utils._JsonEscape(parts[8])); sbCompile.Append("\""); };
                            //    if (parts.Length > 9) { sbCompile.Append(",\"P9\":\""); sbCompile.Append(Utils._JsonEscape(parts[9])); sbCompile.Append("\""); };
                            //    sbCompile.Append(",\"C\":[ ");
                            //}
                            else if (txtU.StartsWith("#ENDJKEYS"))
                            {
                                sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]},");
                            }
                            else if (txtU.StartsWith("#JARRAY"))
                            {
                                parts = Utils.MySplit(txt, 3);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\"");
                                sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\"");
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
//#JSORT.<par_name>.<json_arr_source>.<jsort>#
//#JINS.<par_name>.<json_source>.[<jpath>].<attr_name>.<json_obj>#
//#JAPP.<par_name>.<json_source>.[<jpath>].<json_obj/array>.[<index>]#
//#JUPD.<par_name>.<json_source>.[<jpath>].[<json_obj>]#
//#JDEL.<par_name>.<json_source>.[<jpath>]#
                            else if (txtU.StartsWith("#JSORT")) //#JSORT.<par_name>.<json_arr_source>.<jsort>#
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("JSET"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\"");
                                sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\"");
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                sbCompile.Append(",\"P6\":\"SORT\"");
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#JINS")) //#JINS.<par_name>.<json_source>.[<jpath>].<attr_name>.<json_obj>#
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("JSET"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\"");
                                sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\"");
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 5) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[5])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P5\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                sbCompile.Append(",\"P6\":\"INS\"");
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#JAPP")) //#JAPP.<par_name>.<json_source>.[<jpath>].<json_obj/array>.[<index>]#
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("JSET"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\"");
                                sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\"");
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                if (parts.Length > 5) { sbCompile.Append(",\"P5\":\""); sbCompile.Append(Utils._JsonEscape(parts[5])); sbCompile.Append("\""); };
                                sbCompile.Append(",\"P6\":\"APP\"");
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#JUPD")) //#JUPD.<par_name>.<json_source>.[<jpath>].[<json_obj>]#
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("JSET"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\"");
                                sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\"");
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                sbCompile.Append(",\"P6\":\"UPD\"");
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#JDEL")) //#JDEL.<par_name>.<json_source>.[<jpath>]#
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("JSET"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\"");
                                sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\"");
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                sbCompile.Append(",\"P6\":\"DEL\"");
                                sbCompile.Append("},");
                            }

                            //else if (txtU.StartsWith("#JSORT")) //JSORT.<par_name>.<json_arr_source>.<jsort>
                            //{
                            //    parts = Utils.MySplit(txt, 3);
                            //    sbCompile.Append("{");
                            //    sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                            //    sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                            //    sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\"");
                            //    sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\"");
                            //    if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                            //    sbCompile.Append("},");
                            //}
                            else if (txtU.StartsWith("#INCLUDEONCE."))
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("INCLUDEONCE"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#INCLUDEI."))
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("INCLUDEI"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#INCLUDE"))
                            {
                                parts = Utils.MySplitParams(txt, out sParams);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("INCLUDE"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (sParams != "") { sbCompile.Append(",\"P\":[ "); Compile4(sParams, sbCompile, sbWarning); sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]"); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#PARINCLUDEI."))
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("PARINCLUDEI"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#PARINCLUDE."))
                            {
                                parts = Utils.MySplitParams(txt, out sParams);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("PARINCLUDE"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (sParams != "") { sbCompile.Append(",\"P\":[ "); Compile4(sParams, sbCompile, sbWarning); sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]"); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#IMPORTI."))
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("IMPORTI"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#IMPORT."))
                            {
                                parts = Utils.MySplitParams(txt, out sParams);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("IMPORT"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (sParams != "") { sbCompile.Append(",\"P\":[ "); Compile4(sParams, sbCompile, sbWarning); sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]"); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#IMPORTSETI."))
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("IMPORTSETI"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#IMPORTSET."))
                            {
                                parts = Utils.MySplitParams(txt, out sParams);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("IMPORTSET"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (sParams != "") { sbCompile.Append(",\"P\":[ "); Compile4(sParams, sbCompile, sbWarning); sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]"); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#IFI"))
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("IFI"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                sbCompile.Append(",\"C\":[ ");
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\"SUBIFI1\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                sbCompile.Append(",\"C\":[ ");
                            }
                            else if (txtU.StartsWith("#IF("))
                            {
                                parts = Utils.MySplitParams(txt, out sParams);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("IF"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                sbCompile.Append(",\"C\":[ ");
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\"SUBIF1\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (sParams != "") { sbCompile.Append(",\"P\":[ "); Compile4(sParams, sbCompile, sbWarning); sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]"); };
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                sbCompile.Append(",\"C\":[ ");
                            }
                            else if (txtU.StartsWith("#IF."))
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("IF"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                sbCompile.Append(",\"C\":[ ");
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\"SUBIF1\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (sParams != "") { sbCompile.Append(",\"P\":[ "); Compile4(sParams, sbCompile, sbWarning); sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]"); };
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                sbCompile.Append(",\"C\":[ ");
                            }
                            else if (txtU.StartsWith("#ELSEIFI"))
                            {
//                                parts = Utils.MySplit(txt);
                                parts = Utils.MySplitParams(txt, out sParams);
                                sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]},");
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\"SUBIFI\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
//                                if (sParams != "") { sbCompile.Append(",\"P\":[ "); Compile4(sParams, sbCompile, sbWarning); sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]"); };
                                sbCompile.Append(",\"C\":[ ");
                            }
                            else if (txtU.StartsWith("#ELSEIF"))
                            {
                                parts = Utils.MySplitParams(txt, out sParams);
                                sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]},");
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\"SUBIF\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (sParams != "") { sbCompile.Append(",\"P\":[ "); Compile4(sParams, sbCompile, sbWarning); sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]"); };
                                sbCompile.Append(",\"C\":[ ");
                            }
                            else if (txtU.StartsWith("#ENDIF"))
                            {
                                sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]},");
                                sbCompile.Remove(sbCompile.Length - 1, 1); sbCompile.Append("]},");
                            }
                            else if (txtU.StartsWith("#IIF"))
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                string P7="",P8="",P9="";
                                SetEvalCondition(parts[1], ref P7, ref P8, ref P9);
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                if (P7!="") { sbCompile.Append(",\"P7\":\""); sbCompile.Append(Utils._JsonEscape(P7)); sbCompile.Append("\""); };
                                if (P8!="") { sbCompile.Append(",\"P8\":\""); sbCompile.Append(Utils._JsonEscape(P8)); sbCompile.Append("\""); };
                                if (P9!="") { sbCompile.Append(",\"P9\":\""); sbCompile.Append(Utils._JsonEscape(P9)); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#DEFPAR")) //last group has .
                            {//JPAR.<par_name>.<json_source>.[<jpath>]
                                parts = Utils.MySplit(txt, 2);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#SPAR")) //last group has .
                            {//JPAR.<par_name>.<json_source>.[<jpath>]
                                parts = Utils.MySplit(txt, 2);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#JPAR")) //last group has .
                            {  //JPAR.<par_name>.<json_source>.[<jpath>]
                                parts = Utils.MySplit(txt, 3);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#APPVAL")) //last group has .
                            {  
                                //APPVAL.<value>.<sv_name>.<sv_context>.<sv_contextID>
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("SVSET"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                sbCompile.Append(",\"P8\":\"1\"");
                                //sbCompile.Append(",\"P9\":\"\"");
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#APPENDVAL")) //last group has .
                            {  
                                //APPENDVAL.<value>.<sv_name>.<sv_context>.<sv_contextID>
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("SVSET"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                sbCompile.Append(",\"P8\":\"1\"");
                                sbCompile.Append(",\"P9\":\"|\"");
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#SETNULLVAL")) //last group has .
                            {
                                //APPVAL.<value>.<sv_name>.<sv_context>.<sv_contextID>
                                //APPENDVAL.<value>.<sv_name>.<sv_context>.<sv_contextID>
                                //SETNULLVAL.<value>.<sv_name>.<sv_context>.<sv_contextID>
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("SVSET"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                sbCompile.Append(",\"P7\":\"1\"");
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#SETVAL")) //last group has .
                            {  //SETVAL.<value>.<sv_name>.<sv_context>.<sv_contextID>
                                parts = Utils.MySplit(txt, 4);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("SVSET"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#GETDEFPAR"))
                            {
                                //GETPAR.<par_name>.<sv_name>.[<context>].[<ID>]
                                //GETPARALL.<par_name>.<sv_name>.[<context>].[<ID>]
                                //GETPARSTRICT.<par_name>.<sv_name>.[<context>].[<ID>]
                                //GETPARREPLACE.<par_name>.<what>.<with>.<sv_name>.[<context>].[<ID>]
                                //GETDEFPAR.<par_name>.<default>.<sv_name>.[<context>].[<ID>]
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("SVGET"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P7\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                if (parts.Length > 5) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[5])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#GETPARREPLACE"))
                            {
                                //GETPAR.<par_name>.<sv_name>.[<context>].[<ID>]
                                //GETPARALL.<par_name>.<sv_name>.[<context>].[<ID>]
                                //GETPARSTRICT.<par_name>.<sv_name>.[<context>].[<ID>]
                                //GETPARREPLACE.<par_name>.<what>.<with>.<sv_name>.[<context>].[<ID>]
                                //GETDEFPAR.<par_name>.<default>.<sv_name>.[<context>].[<ID>]
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("SVGET"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                if (parts.Length > 5) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[5])); sbCompile.Append("\""); };
                                if (parts.Length > 6) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[6])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P8\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P9\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#GETPAR")) //last group has .
                            {
                                //GETPAR.<par_name>.<sv_name>.[<context>].[<ID>]
                                //GETPARALL.<par_name>.<sv_name>.[<context>].[<ID>]
                                //GETPARSTRICT.<par_name>.<sv_name>.[<context>].[<ID>]
                                //GETPARREPLACE.<par_name>.<what>.<with>.<sv_name>.[<context>].[<ID>]
                                //GETDEFPAR.<par_name>.<default>.<sv_name>.[<context>].[<ID>]
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("SVGET"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                if (txtU.StartsWith("#GETPARALL")) sbCompile.Append(",\"P5\":\"1\"");
                                if (txtU.StartsWith("#GETPARSTRICT")) sbCompile.Append(",\"P6\":\"1\"");
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#DEL"))
                            {
                                //DEL.<sv_name>.[<context>].[<ID>]
                                //DELALL.<sv_name>.[<context>].[<ID>]
                                //DELFROM.<sv_name>.[<context>].[<ID>]
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("SVDEL"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (txtU.StartsWith("#DELALL")) sbCompile.Append(",\"P5\":\"1\"");
                                if (txtU.StartsWith("#DELFROM")) sbCompile.Append(",\"P4\":\"1\"");
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#EXIT"))
                            {
                                //EXIT.loop|template|request.[msg]
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("EXIT"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#THROW"))
                            {
                                //EXIT.loop|template|request.[msg]
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("THROW"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#FILE"))
                            {
                                //FILE.intype.invalue.outtype.outval
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("FILE"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#DELETETEMPLATE"))
                            {
                                //FILE.intype.invalue.outtype.outval
                                parts = Utils.MySplit(txt, 1);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append("TDEL"); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#IMPORT"))
                            {
                                //IMPORT.content
                                parts = Utils.MySplit(txt, 1);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#EXPORT"))
                            {
                                //EXPORT.template_name
                                parts = Utils.MySplit(txt, 1);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#TRANSFER"))
                            {
                                //TRANSFER.template_pattern.???
                                parts = Utils.MySplit(txt, 1);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                            else if (txtU.StartsWith("#TGET") || txtU.StartsWith("#TDEL") || txtU.StartsWith("#TSET") || txtU.StartsWith("#TSETI"))
                            {
                                //TGET.template_pattern
                                //TSET.content
                                //TDEL.template_pattern
                                parts = Utils.MySplit(txt, 4);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }

                            else
                            {
                                parts = Utils.MySplit(txt);
                                sbCompile.Append("{");
                                sbCompile.Append("\"T\":\""); sbCompile.Append(parts[0].Substring(1).ToUpper()); sbCompile.Append("\"");
                                sbCompile.Append(",\"L\":"); sbCompile.Append(iLine.ToString()); sbCompile.Append("");
                                if (parts.Length > 1) { sbCompile.Append(",\"P1\":\""); sbCompile.Append(Utils._JsonEscape(parts[1])); sbCompile.Append("\""); };
                                if (parts.Length > 2) { sbCompile.Append(",\"P2\":\""); sbCompile.Append(Utils._JsonEscape(parts[2])); sbCompile.Append("\""); };
                                if (parts.Length > 3) { sbCompile.Append(",\"P3\":\""); sbCompile.Append(Utils._JsonEscape(parts[3])); sbCompile.Append("\""); };
                                if (parts.Length > 4) { sbCompile.Append(",\"P4\":\""); sbCompile.Append(Utils._JsonEscape(parts[4])); sbCompile.Append("\""); };
                                if (parts.Length > 5) { sbCompile.Append(",\"P5\":\""); sbCompile.Append(Utils._JsonEscape(parts[5])); sbCompile.Append("\""); };
                                if (parts.Length > 6) { sbCompile.Append(",\"P6\":\""); sbCompile.Append(Utils._JsonEscape(parts[6])); sbCompile.Append("\""); };
                                if (parts.Length > 7) { sbCompile.Append(",\"P7\":\""); sbCompile.Append(Utils._JsonEscape(parts[7])); sbCompile.Append("\""); };
                                if (parts.Length > 8) { sbCompile.Append(",\"P8\":\""); sbCompile.Append(Utils._JsonEscape(parts[8])); sbCompile.Append("\""); };
                                if (parts.Length > 9) { sbCompile.Append(",\"P9\":\""); sbCompile.Append(Utils._JsonEscape(parts[9])); sbCompile.Append("\""); };
                                sbCompile.Append("},");
                            }
                        }
                        iLastLinesCommand = LinesCountIndexOf(txt);
                        iLine += iLastLinesCommand;
                    }
                    else
                    {
                        iLine++;
                    }
                }
                return sbWarning.ToString();
            }
            catch (Exception ex)
            {
                sbCompile.Clear();
                sbCompile.Append("error");
                return "error:" + ex.Message;
            }
        }
        private void SetEvalCondition(string condition, ref string m_P7, ref string m_P8, ref string m_P9)
        {
            m_P8 = "";
            m_P9 = "";
            if (condition == "")
                m_P7 = "true";
            else if (condition.Contains("===="))
            {
                int i = condition.IndexOf("====");
                m_P8 = condition.Substring(0, i).Trim();
                m_P9 = condition.Substring(i + 4).Trim();
                m_P7 = "====";
            }
            else if (condition.Contains("!==="))
            {
                int i = condition.IndexOf("!===");
                m_P8 = condition.Substring(0, i).Trim();
                m_P9 = condition.Substring(i + 4).Trim();
                m_P7 = "!===";
            }
            else if (condition.Contains("==="))
            {
                int i = condition.IndexOf("===");
                m_P8 = condition.Substring(0, i).Trim();
                m_P9 = condition.Substring(i + 3).Trim();
                m_P7 = "===";
            }
            else if (condition.Contains("!=="))
            {
                int i = condition.IndexOf("!==");
                m_P8 = condition.Substring(0, i).Trim();
                m_P9 = condition.Substring(i + 3).Trim();
                m_P7 = "!==";
            }
            else if (condition.Contains("=="))
            {
                int i = condition.IndexOf("==");
                m_P8 = condition.Substring(0, i).Trim();
                m_P9 = condition.Substring(i + 2).Trim();
                m_P7 = "==";
            }
            else if (condition.Contains("!="))
            {
                int i = condition.IndexOf("!=");
                m_P8 = condition.Substring(0, i).Trim();
                m_P9 = condition.Substring(i + 2).Trim();
                m_P7 = "!=";
            }
            else if (condition.Contains(">>>"))
            {
                int i = condition.IndexOf(">>>");
                m_P8 = condition.Substring(0, i).Trim();
                m_P9 = condition.Substring(i + 3).Trim();
                m_P7 = ">>>";
            }
            else if (condition.Contains(">>"))
            {
                int i = condition.IndexOf(">>");
                m_P8 = condition.Substring(0, i).Trim();
                m_P9 = condition.Substring(i + 2).Trim();
                m_P7 = ">>";
            }
            else if (condition.Contains("<<<"))
            {
                int i = condition.IndexOf("<<<");
                m_P8 = condition.Substring(0, i).Trim();
                m_P9 = condition.Substring(i + 3).Trim();
                m_P7 = "<<<";
            }
            else if (condition.Contains("<<"))
            {
                int i = condition.IndexOf("<<");
                m_P8 = condition.Substring(0, i).Trim();
                m_P9 = condition.Substring(i + 2).Trim();
                m_P7 = "<<";
            }
            else
            {
                m_P7 = "SQL";
                m_P8 = condition;
            }

        }

        //public string CompileWarning_orig(string m_textContent, StringBuilder sbCompile)
        //{

        //    try
        //    {
        //        CCmd root = new CCmd();
        //        root.Type = CommandType.GenericCommand;
        //        m_stack.Clear();
        //        m_stack.Push(root);
        //        m_text = m_textContent;

        //        mc = regexXDoc.Matches(m_textContent);
        //        for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
        //        {
        //            m_skipCurrentMatch = false;
        //            Match m = mc[matchIndex];
        //            //this call can modify the m_skipCurrentMatch member variable
        //            CCmd cmd = GetNewCmd(m.Value, m.Index + m_partialTextOffset, true, false);
        //            //if the m_skipCurrentMatch has been modified, skip the current cycle.
        //            if (m_skipCurrentMatch == true)
        //                if ((cmd.Type != CommandType.TEXTCommand) || ((cmd as CTextCmd).Text != ""))
        //                {
        //                    ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
        //                    continue;
        //                }
        //            if (cmd != null)
        //            {
        //                if (cmd.IsBlockComand)
        //                {
        //                    if (cmd.Type == CommandType.ElseIFCommand)
        //                    {
        //                        CCmd cmdIf = (CCmd)m_stack.Pop();
        //                        if (!(cmdIf is CIFCmd))
        //                            throw new Exception("ELSEIF command at line " + GetLine(m.Index + m_partialTextOffset).ToString() + " does not have a correspondig IF command associated.");
        //                        else
        //                        {
        //                            if (cmdIf.Type == CommandType.IFCommand)
        //                                ((CCmd)m_stack.Peek()).ChildCommands.Add(cmdIf);
        //                        }
        //                        ((CIFCmd)cmdIf).ElseIfCommand = (CIFCmd)cmd;

        //                    }
        //                    if ((cmd.Type != CommandType.TEXTCommand) || ((cmd as CTextCmd).Text != ""))
        //                        m_stack.Push(cmd);
        //                }
        //                else
        //                {
        //                    cmd.EndIndex = m.Index + m_partialTextOffset + m.Value.Length;
        //                    if ((cmd.Type != CommandType.TEXTCommand) || ((cmd as CTextCmd).Text != ""))
        //                        ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
        //                    //else
        //                    //    ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
        //                }
        //            }
        //            else // this means we're dealing with a closing #*# tag
        //            {
        //                bool bCloseOK = VerifyClosingTag(m);
        //                if (bCloseOK)
        //                {
        //                    CCmd cmdOK = (CCmd)m_stack.Pop();
        //                    if (m_stack.Count == 0)
        //                        throw new Exception("Empty stack!");
        //                    cmdOK.EndIndex = m.Index + m_partialTextOffset + m.Value.Length;
        //                    if (cmdOK.Type != CommandType.ElseIFCommand)
        //                        ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmdOK);
        //                }
        //                else
        //                    throw new Exception("The closing tag " + m.Value + " at line " + GetLine(m.Index + m_partialTextOffset).ToString() + " is not valid. Check the command it was intended for!");
        //            }
        //        }

        //        if (m_stack.Count >= 1)
        //            foreach (CCmd stackCommand in m_stack)
        //                ValidateCommandTree(stackCommand, 0, 0);
        //        StringBuilder sbWarning = new StringBuilder();
        //        root.GetJson(sbCompile, sbWarning);
        //        return sbWarning.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        sbCompile.Clear();
        //        sbCompile.Append("error");
        //        return "error:" + ex.Message;
        //    }
        //}
        //public int GetLine(int index)
        //{
        //    if (m_text == null || m_text == "")
        //        return -1;

        //    int io = 0;
        //    int ret = 0;
        //    while (io != -1 && io < index)
        //    {
        //        ret++;
        //        //io = m_text.ToString().IndexOf(Environment.NewLine, io + 1);
        //        io = m_text.IndexOf(Environment.NewLine, io + 1);
        //    }

        //    if (ret > 0)
        //        ret--;

        //    return ret;
        //}
        //bool VerifyClosingTag(Match m)
        //{
        //    CCmd comm = (CCmd)m_stack.Peek();
        //    string tag = m.Value.Replace("#", "");
        //    if (tag.ToUpper() == "ENDIF" && !(comm is CIFCmd))
        //        return false;
        //    if (tag.ToUpper().StartsWith("ENDQRY."))
        //    {
        //        if (!(comm is CQRYCmd))
        //            return false;
        //        string queryName = tag.Substring(tag.IndexOf(".") + 1);
        //        if (((CQRYCmd)comm).QueryName.ToUpper() != queryName.ToUpper())
        //            return false;
        //    }

        //    if (tag.ToUpper().StartsWith("ENDREP."))
        //    {
        //        if (!(comm is CREPCmd))
        //            return false;
        //        string queryName = tag.Substring(tag.IndexOf(".") + 1);
        //        if (((CREPCmd)comm).QueryName.ToUpper() != queryName.ToUpper())
        //            return false;
        //    }

        //    if (tag.ToUpper().StartsWith("ENDXPATH."))
        //    {
        //        if (!(comm is CXPATHCmd))
        //            return false;
        //        string queryName = tag.Substring(tag.IndexOf(".") + 1);
        //        if (((CXPATHCmd)comm).QueryName.ToUpper() != queryName.ToUpper())
        //            return false;
        //    }

        //    if (tag.ToUpper().StartsWith("ENDJPATH."))
        //    {
        //        if (!(comm is CXPATHCmd))
        //            return false;
        //        string queryName = tag.Substring(tag.IndexOf(".") + 1);
        //        if (((CXPATHCmd)comm).QueryName.ToUpper() != queryName.ToUpper())
        //            return false;
        //    }
        //    if (tag.ToUpper().StartsWith("ENDJLOOP."))
        //    {
        //        if (!(comm is CJLOOPCmd))
        //            return false;
        //        string queryName = tag.Substring(tag.IndexOf(".") + 1);
        //        if (((CJLOOPCmd)comm).ParameterName.ToUpper() != queryName.ToUpper())
        //            return false;
        //    }
        //    if (tag.ToUpper().StartsWith("ENDJKEYS."))
        //    {
        //        if (!(comm is CJKEYSCmd))
        //            return false;
        //        string queryName = tag.Substring(tag.IndexOf(".") + 1);
        //        if (((CJKEYSCmd)comm).ParameterName.ToUpper() != queryName.ToUpper())
        //            return false;
        //    }
        //    return true;
        //}

        ////private int ValidateCommandTree(CCmd root, int i, int lev)
        ////{
        ////    if (root == null) return i;
        ////    if (root.EndIndex == -1 && root.Type != CommandType.GenericCommand && root.IsBlockComand)
        ////        if (!(root is CIFCmd && (root as CIFCmd).ElseIfCommand != null))
        ////            throw new Exception("The command " + root.Type.ToString() + " at line " + GetLine(root.StartIndex).ToString() + " does not have a valid closing tag!");
        ////    root.cmdID = i++;
        ////    //System.Diagnostics.Debug.WriteLine(m_templateID.ToString () +  " (" + lev.ToString() + ") " + root.Type + " " + root.ID.ToString());
        ////    if (root.Type == CommandType.IFCommand)
        ////        i = ValidateCommandTree((root as CIFCmd).ElseIfCommand, i, lev);
        ////    foreach (CCmd command in root.ChildCommands)
        ////        i = ValidateCommandTree(command, i++, lev + 1);
        ////    return i;
        ////}
        ////CCmd GetNewCmd(string txt, int initialIndex, bool acceptClosingBlockCommands, bool forceText)
        ////{// \((([^\)]*\\\))*[^\)]*|[^\)]*)\)
        ////    bool removedSeparator = false;
        ////    if (txt.StartsWith("#") && txt.EndsWith("#"))
        ////    {
        ////        txt = txt.Substring(1, txt.Length - 2 * 1);
        ////        removedSeparator = true;
        ////    }

        ////    CCmd cmd = null;

        ////    //try
        ////    //{
        ////    //    if (txt.ToUpper() == START_BLOCK)
        ////    //    {
        ////    //        m_ignoreBlock = true;
        ////    //        m_commentBlock = false;
        ////    //    }
        ////    //    else if (txt.ToUpper() == END_BLOCK)
        ////    //    {
        ////    //        m_ignoreBlock = false;
        ////    //        m_commentBlock = false;
        ////    //    }
        ////    //    if (txt.ToUpper() == START_COMMENT)
        ////    //    {
        ////    //        m_ignoreBlock = true;
        ////    //        m_commentBlock = true;
        ////    //    }
        ////    //    else if (txt.ToUpper() == END_COMMENT)
        ////    //    {
        ////    //        m_ignoreBlock = false;
        ////    //        m_commentBlock = false;
        ////    //    }

        ////    //    if ((!m_ignoreBlock))
        ////    //    {
        ////    //        if (txt.ToUpper().StartsWith("ADDPAR.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetADDPARCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("GETPARALL.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetGETPARALLCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("GETPARSTRICT.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetGETPARStrictCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("GETPAR.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetGETPARCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("GETCFGPAR.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetGETCFGPARCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("GETPARREPLACE.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetGETPARREPLACECmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("GETDEFPAR.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetGETDEFPARCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("GETITEMPAR.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetGETITEMPARCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("FINDITEMPAR.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetFINDITEMPARCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("APPVAL.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetAPPENDVALCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("APPENDVAL.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetAPPENDVALCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("APPFLD.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetAPPENDFLDCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("APPENDFLD.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetAPPENDFLDCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("DEL.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetDELCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("DELFROM.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetDELFROMCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("DELALL.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetDELALLCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("DELITEM.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetDELITEMCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("THROW.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetThrowCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("FILE.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetFILECmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("SETPAR.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetSETPARCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("SETPAREXT.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetSETPAREXTCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("SETFLD.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetSETFLDCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("SETFLDEXT.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetSETFLDEXTCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("SETVAL.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetSETVALCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("SETNULLPAR.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetSETNULLPARCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("SETNULLPAREXT.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetSETNULLPAREXTCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("SETNULLFLD.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetSETNULLFLDCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("SETNULLFLDEXT.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetSETNULLFLDEXTCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("SETNULLVAL.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetSETNULLVALCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("JPATHDATA.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetXPATHCmd(txt, initialIndex, false, true, true);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("XPATHDATA.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetXPATHCmd(txt, initialIndex, false, false, true);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("TXPATHDATA.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetXPATHCmd(txt, initialIndex, true, false, true);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("JPATH.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetXPATHCmd(txt, initialIndex, false, true, false);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("XPATH.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetXPATHCmd(txt, initialIndex, false, false, false);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("TXPATH.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetXPATHCmd(txt, initialIndex, true, false, false);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("SPARP."))
        ////    //        {
        ////    //            cmd = GetDefParCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("SPARA."))
        ////    //        {
        ////    //            cmd = GetDefParCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("SPAR."))
        ////    //        {
        ////    //            cmd = GetDefParCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("JPARP.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetJPARCmd(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("JPAR.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetJPARCmd(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("JDATA.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetJDATACmd(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("JKEYS.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetJKEYSCmd(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("JLOOP") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetJLOOPCmd(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("EXIT."))
        ////    //        {
        ////    //            cmd = GetEXITCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("IIF."))
        ////    //        {
        ////    //            cmd = GetIIFCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("REPDATA.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetREPCmd(txt, initialIndex, true);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("REP.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetREPCmd(txt, initialIndex, false);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("COUNT.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetCOUNTCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("QRYDATA.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetQRYCmd(txt, initialIndex, true);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("QRY.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetQRYCmd(txt, initialIndex, false);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("PAR.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetPARCmd(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("FLD.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetFLDCmd(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("FLDBIN.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetFLDBINCmd(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("ATTACH.") && (removedSeparator))
        ////    //        {
        ////    //            cmd = GetAttachCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("INCLUDEONCE"))
        ////    //        {
        ////    //            cmd = GetIncludeOnceCommand(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("PARINCLUDE."))
        ////    //        {
        ////    //            cmd = GetParIncludeCommand(txt, initialIndex);
        ////    //        }
        ////    //        else if ((txt.ToUpper().StartsWith("INCLUDE(") || txt.ToUpper().StartsWith("UDF(")))
        ////    //        {
        ////    //            cmd = GetIncludeCommand(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("IMPORTFLD."))
        ////    //        {
        ////    //            cmd = GetImportFldCommand(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("IMPORTSET."))
        ////    //        {
        ////    //            cmd = GetImportSetCommand(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("IMPORT."))
        ////    //        {
        ////    //            cmd = GetImportCommand(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("EXPORT."))
        ////    //        {
        ////    //            cmd = GetExportCommand(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("DELETETEMPLATE."))
        ////    //        {
        ////    //            cmd = GetDeleteTemplateCommand(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("TRANSFERLIKE."))
        ////    //        {
        ////    //            cmd = GetTransferLikeCommand(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("TRANSFER."))
        ////    //        {
        ////    //            cmd = GetTransferCommand(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("CFG."))
        ////    //        {
        ////    //            cmd = GetCfgCommand(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("USEPARAMETERS."))
        ////    //        {
        ////    //            cmd = GetUseParameters(txt);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("FUNC."))
        ////    //        {
        ////    //            cmd = GetFUNCCommand(txt);
        ////    //        }
        ////    //        else if ((txt.ToUpper().StartsWith("IF") || txt.ToUpper().StartsWith("ELSEIF")) && (removedSeparator))
        ////    //        {
        ////    //            // because of a limitation of generalFieldREx which does not report the starting "#" 
        ////    //            //when the IF condition contains brackets (other than the start and end brackets)
        ////    //            int ifIndex = removedSeparator ? initialIndex : initialIndex - 1;

        ////    //            cmd = GetIFCommand(txt, ifIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("DEFPAR."))
        ////    //        {
        ////    //            cmd = GetDefParCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("DEFQRY."))
        ////    //        {
        ////    //            cmd = GetDefQryCmd(txt, initialIndex);
        ////    //        }
        ////    //        else if (txt.ToUpper().StartsWith("MSG."))
        ////    //        {
        ////    //            cmd = GetMsgCmd(txt, initialIndex);
        ////    //        }
        ////    //        // else if (cmd == null && txt.IndexOf("#") == -1 && !txt.ToUpper().StartsWith("ENDXPATH") && !txt.ToUpper().StartsWith("ENDREP")
        ////    //        //&& !txt.ToUpper().StartsWith("ENDQRY") && !txt.ToUpper().StartsWith("ENDIF"))
        ////    //        else if (cmd == null && !txt.ToUpper().StartsWith("ENDJLOOP") && !txt.ToUpper().StartsWith("ENDJKEYS") && !txt.ToUpper().StartsWith("ENDJPATH") && !txt.ToUpper().StartsWith("ENDXPATH") && !txt.ToUpper().StartsWith("ENDREP")
        ////    //            && !txt.ToUpper().StartsWith("ENDQRY") && !txt.ToUpper().StartsWith("ENDIF"))
        ////    //        {
        ////    //            cmd = new CTextCmd();
        ////    //            if (forceText)
        ////    //            {
        ////    //                if (txt.ToUpper() == END_BLOCK)
        ////    //                    txt = "#" + END_BLOCK + "#";
        ////    //                (cmd as CTextCmd).ForceText(txt);
        ////    //            }
        ////    //            else
        ////    //                (cmd as CTextCmd).Text = txt;
        ////    //            //if ((cmd as CTextCmd).Text == "")
        ////    //            //    cmd = null;
        ////    //        }
        ////    //    }
        ////    //    else
        ////    //    {
        ////    //        cmd = new CTextCmd();
        ////    //        string blockContent = "";

        ////    //        int endBlockLen = 2 * 1 + (m_commentBlock ? END_COMMENT.Length : END_BLOCK.Length);

        ////    //        int i1 = m_text.ToUpper().IndexOf("#" + (m_commentBlock ? END_COMMENT : END_BLOCK) + "#", initialIndex + 1);
        ////    //        if (i1 != -1 && mc != null)
        ////    //        {
        ////    //            blockContent = m_text.ToString().Substring(initialIndex, i1 - initialIndex + endBlockLen);
        ////    //            string m_partialText = m_text.Substring(i1 + endBlockLen);
        ////    //            m_partialTextOffset = i1 + endBlockLen;
        ////    //            mc = regexXDoc.Matches(m_partialText);
        ////    //            //matchIndex = -1;
        ////    //            m_skipCurrentMatch = true;
        ////    //            (cmd as CTextCmd).ForceText(blockContent);
        ////    //            m_ignoreBlock = false;
        ////    //        }
        ////    //    }



        ////    //    if (cmd != null)
        ////    //    {
        ////    //        cmd.StartIndex = initialIndex;
        ////    //        cmd.Line = GetLine(initialIndex);
        ////    //        //                    cmd.Parser = this;
        ////    //    }
        ////    //    else if (!acceptClosingBlockCommands)
        ////    //        throw new Exception(" ");

        ////    //}
        ////    //catch (Exception ex)
        ////    //{
        ////    //    string message = string.Format("Could not create new command from text {0} at line {1}", txt, GetLine(initialIndex)) + Environment.NewLine + ex.Message;
        ////    //    throw new Exception(message);
        ////    //}
        ////    return cmd;
        ////}

        #endregion

    }
    class CCompiler34
    {

//        #region compile templates
//        Stack m_stack = null;
//        MatchCollection mc = null;
//        Regex regexXDoc = new Regex(@"(#((SPARP)|(SPARA)|(SPAR)|(JPAR)|(JDATA)|(JLOOP)|(JLOOPC)|(EXIT)|(ENDJLOOP)|(JKEYS)|(ENDJKEYS)|(ADDPAR)|(GETPARALL)|(GETPARSTRICT)|(GETPAR)|(GETCFGPAR)|(GETPARREPLACE)|(GETDEFPAR)|(GETITEMPAR)|(FINDITEMPAR)|(APPVAL)|(APPENDVAL)|(APPFLD)|(APPENDFLD)|(DEL)|(DELFROM)|(DELALL)|(DELITEM)|(SETPAR)|(SETPAREXT)|(SETFLD)|(SETFLDEXT)|(SETVAL)|(SETNULLPAR)|(SETNULLPAREXT)|(SETNULLFLD)|(SETNULLFLDEXT)|(SETNULLVAL)|(MSG)|(THROW)|(THROWPAR)|(FILE)|(COUNT)|(UDF)|(DEFPAR)|(DEFQRY)|(STARTBLOCK)|(ENDBLOCK)|(STARTCOMMENT)|(ENDCOMMENT)|(XPATH)|(XPATHDATA)|(TXPATH)|(TXPATHDATA)|(JPATH)|(JPATHDATA)|(REP)|(REPDATA)|(QRY)|(QRYDATA)|(IIF)|(IF)|(ELSEIF)|(IFI)|(ELSEIFI)|(PAR)|(FLD)|(FLDBIN)|(ATTACH)|(ENDXPATH)|(ENDJPATH)|(ENDREP)|(ENDIF)|(ENDQRY)|(INCLUDEONCE)|(PARINCLUDE)|(INCLUDE)|(IMPORT)|(IMPORTSET)|(IMPORTFLD)|(EXPORT)|(DELETETEMPLATE)|(TRANSFERLIKE)|(TRANSFER)|(CFG)|(USEPARAMETERS)|(FUNC))[^#\x28]*(\x28[^\x29]*\x29)?(.\x28[^\x29]*\x29)?#)|[^#]+", RegexOptions.IgnoreCase);
//        const string
//            START_BLOCK = "STARTBLOCK",
//            END_BLOCK = "ENDBLOCK",
//            START_COMMENT = "STARTCOMMENT",
//            END_COMMENT = "ENDCOMMENT";

//        string m_text = "";
//        bool m_skipCurrentMatch = false;
//        bool m_commentBlock = false;
//        bool m_ignoreBlock = false;
//        int m_partialTextOffset = 0;

//        public string CompileWarning(string m_textContent, StringBuilder sbCompile)
//        {

//            try
//            {
//                CCmd root = new CCmd();
//                root.Type = CommandType.GenericCommand;
//                m_stack.Clear();
//                m_stack.Push(root);
//                m_text = m_textContent;

//                mc = regexXDoc.Matches(m_textContent);
//                for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
//                {
//                    m_skipCurrentMatch = false;
//                    Match m = mc[matchIndex];
//                    //this call can modify the m_skipCurrentMatch member variable
//                    CCmd cmd = GetNewCmd(m.Value, m.Index + m_partialTextOffset, true, false);
//                    //if the m_skipCurrentMatch has been modified, skip the current cycle.
//                    if (m_skipCurrentMatch == true)
//                        if ((cmd.Type != CommandType.TEXTCommand) || ((cmd as CTextCmd).Text != ""))
//                        {
//                            ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
//                            continue;
//                        }
//                    if (cmd != null)
//                    {
//                        if (cmd.IsBlockComand)
//                        {
//                            if (cmd.Type == CommandType.ElseIFCommand)
//                            {
//                                CCmd cmdIf = (CCmd)m_stack.Pop();
//                                if (!(cmdIf is CIFCmd))
//                                    throw new Exception("ELSEIF command at line " + GetLine(m.Index + m_partialTextOffset).ToString() + " does not have a correspondig IF command associated.");
//                                else
//                                {
//                                    if (cmdIf.Type == CommandType.IFCommand)
//                                        ((CCmd)m_stack.Peek()).ChildCommands.Add(cmdIf);
//                                }
//                                ((CIFCmd)cmdIf).ElseIfCommand = (CIFCmd)cmd;

//                            }
//                            if ((cmd.Type != CommandType.TEXTCommand) || ((cmd as CTextCmd).Text != ""))
//                                m_stack.Push(cmd);
//                        }
//                        else
//                        {
//                            cmd.EndIndex = m.Index + m_partialTextOffset + m.Value.Length;
//                            if ((cmd.Type != CommandType.TEXTCommand) || ((cmd as CTextCmd).Text != ""))
//                                ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
//                            //else
//                            //    ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
//                        }
//                    }
//                    else // this means we're dealing with a closing #*# tag
//                    {
//                        bool bCloseOK = VerifyClosingTag(m);
//                        if (bCloseOK)
//                        {
//                            CCmd cmdOK = (CCmd)m_stack.Pop();
//                            if (m_stack.Count == 0)
//                                throw new Exception("Empty stack!");
//                            cmdOK.EndIndex = m.Index + m_partialTextOffset + m.Value.Length;
//                            if (cmdOK.Type != CommandType.ElseIFCommand)
//                                ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmdOK);
//                        }
//                        else
//                            throw new Exception("The closing tag " + m.Value + " at line " + GetLine(m.Index + m_partialTextOffset).ToString() + " is not valid. Check the command it was intended for!");
//                    }
//                }

//                if (m_stack.Count >= 1)
//                    foreach (CCmd stackCommand in m_stack)
//                        ValidateCommandTree(stackCommand, 0, 0);
//                StringBuilder sbWarning = new StringBuilder();
//                root.GetJson(sbCompile, sbWarning);
//                return sbWarning.ToString();
//            }
//            catch (Exception ex)
//            {
//                sbCompile.Clear();
//                sbCompile.Append("error");
//                return "error:" + ex.Message;
//            }
//        }
//        public int GetLine(int index)
//        {
//            if (m_text == null || m_text == "")
//                return -1;

//            int io = 0;
//            int ret = 0;
//            while (io != -1 && io < index)
//            {
//                ret++;
//                //io = m_text.ToString().IndexOf(Environment.NewLine, io + 1);
//                io = m_text.IndexOf(Environment.NewLine, io + 1);
//            }

//            if (ret > 0)
//                ret--;

//            return ret;
//        }
//        bool VerifyClosingTag(Match m)
//        {
//            CCmd comm = (CCmd)m_stack.Peek();
//            string tag = m.Value.Replace("#", "");
//            if (tag.ToUpper() == "ENDIF" && !(comm is CIFCmd))
//                return false;
//            if (tag.ToUpper().StartsWith("ENDQRY."))
//            {
//                if (!(comm is CQRYCmd))
//                    return false;
//                string queryName = tag.Substring(tag.IndexOf(".") + 1);
//                if (((CQRYCmd)comm).QueryName.ToUpper() != queryName.ToUpper())
//                    return false;
//            }

//            if (tag.ToUpper().StartsWith("ENDREP."))
//            {
//                if (!(comm is CREPCmd))
//                    return false;
//                string queryName = tag.Substring(tag.IndexOf(".") + 1);
//                if (((CREPCmd)comm).QueryName.ToUpper() != queryName.ToUpper())
//                    return false;
//            }

//            if (tag.ToUpper().StartsWith("ENDXPATH."))
//            {
//                if (!(comm is CXPATHCmd))
//                    return false;
//                string queryName = tag.Substring(tag.IndexOf(".") + 1);
//                if (((CXPATHCmd)comm).QueryName.ToUpper() != queryName.ToUpper())
//                    return false;
//            }

//            if (tag.ToUpper().StartsWith("ENDJPATH."))
//            {
//                if (!(comm is CXPATHCmd))
//                    return false;
//                string queryName = tag.Substring(tag.IndexOf(".") + 1);
//                if (((CXPATHCmd)comm).QueryName.ToUpper() != queryName.ToUpper())
//                    return false;
//            }
//            if (tag.ToUpper().StartsWith("ENDJLOOP."))
//            {
//                if (!(comm is CJLOOPCmd))
//                    return false;
//                string queryName = tag.Substring(tag.IndexOf(".") + 1);
//                if (((CJLOOPCmd)comm).ParameterName.ToUpper() != queryName.ToUpper())
//                    return false;
//            }
//            if (tag.ToUpper().StartsWith("ENDJKEYS."))
//            {
//                if (!(comm is CJKEYSCmd))
//                    return false;
//                string queryName = tag.Substring(tag.IndexOf(".") + 1);
//                if (((CJKEYSCmd)comm).ParameterName.ToUpper() != queryName.ToUpper())
//                    return false;
//            }
//            return true;
//        }

//        private int ValidateCommandTree(CCmd root, int i, int lev)
//        {
//            if (root == null) return i;
//            if (root.EndIndex == -1 && root.Type != CommandType.GenericCommand && root.IsBlockComand)
//                if (!(root is CIFCmd && (root as CIFCmd).ElseIfCommand != null))
//                    throw new Exception("The command " + root.Type.ToString() + " at line " + GetLine(root.StartIndex).ToString() + " does not have a valid closing tag!");
//            root.cmdID = i++;
//            //System.Diagnostics.Debug.WriteLine(m_templateID.ToString () +  " (" + lev.ToString() + ") " + root.Type + " " + root.ID.ToString());
//            if (root.Type == CommandType.IFCommand)
//                i = ValidateCommandTree((root as CIFCmd).ElseIfCommand, i, lev);
//            foreach (CCmd command in root.ChildCommands)
//                i = ValidateCommandTree(command, i++, lev + 1);
//            return i;
//        }
//        CCmd GetNewCmd(string txt, int initialIndex, bool acceptClosingBlockCommands, bool forceText)
//        {// \((([^\)]*\\\))*[^\)]*|[^\)]*)\)
//            bool removedSeparator = false;
//            if (txt.StartsWith("#") && txt.EndsWith("#"))
//            {
//                txt = txt.Substring(1, txt.Length - 2 * 1);
//                removedSeparator = true;
//            }

//            CCmd cmd = null;

//            try
//            {
//                if (txt.ToUpper() == START_BLOCK)
//                {
//                    m_ignoreBlock = true;
//                    m_commentBlock = false;
//                }
//                else if (txt.ToUpper() == END_BLOCK)
//                {
//                    m_ignoreBlock = false;
//                    m_commentBlock = false;
//                }
//                if (txt.ToUpper() == START_COMMENT)
//                {
//                    m_ignoreBlock = true;
//                    m_commentBlock = true;
//                }
//                else if (txt.ToUpper() == END_COMMENT)
//                {
//                    m_ignoreBlock = false;
//                    m_commentBlock = false;
//                }

//                if ((!m_ignoreBlock))
//                {
//                    if (txt.ToUpper().StartsWith("ADDPAR.") && (removedSeparator))
//                    {
//                        cmd = GetADDPARCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("GETPARALL.") && (removedSeparator))
//                    {
//                        cmd = GetGETPARALLCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("GETPARSTRICT.") && (removedSeparator))
//                    {
//                        cmd = GetGETPARStrictCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("GETPAR.") && (removedSeparator))
//                    {
//                        cmd = GetGETPARCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("GETCFGPAR.") && (removedSeparator))
//                    {
//                        cmd = GetGETCFGPARCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("GETPARREPLACE.") && (removedSeparator))
//                    {
//                        cmd = GetGETPARREPLACECmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("GETDEFPAR.") && (removedSeparator))
//                    {
//                        cmd = GetGETDEFPARCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("GETITEMPAR.") && (removedSeparator))
//                    {
//                        cmd = GetGETITEMPARCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("FINDITEMPAR.") && (removedSeparator))
//                    {
//                        cmd = GetFINDITEMPARCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("APPVAL.") && (removedSeparator))
//                    {
//                        cmd = GetAPPENDVALCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("APPENDVAL.") && (removedSeparator))
//                    {
//                        cmd = GetAPPENDVALCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("APPFLD.") && (removedSeparator))
//                    {
//                        cmd = GetAPPENDFLDCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("APPENDFLD.") && (removedSeparator))
//                    {
//                        cmd = GetAPPENDFLDCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("DEL.") && (removedSeparator))
//                    {
//                        cmd = GetDELCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("DELFROM.") && (removedSeparator))
//                    {
//                        cmd = GetDELFROMCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("DELALL.") && (removedSeparator))
//                    {
//                        cmd = GetDELALLCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("DELITEM.") && (removedSeparator))
//                    {
//                        cmd = GetDELITEMCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("THROW.") && (removedSeparator))
//                    {
//                        cmd = GetThrowCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("FILE.") && (removedSeparator))
//                    {
//                        cmd = GetFILECmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("SETPAR.") && (removedSeparator))
//                    {
//                        cmd = GetSETPARCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("SETPAREXT.") && (removedSeparator))
//                    {
//                        cmd = GetSETPAREXTCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("SETFLD.") && (removedSeparator))
//                    {
//                        cmd = GetSETFLDCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("SETFLDEXT.") && (removedSeparator))
//                    {
//                        cmd = GetSETFLDEXTCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("SETVAL.") && (removedSeparator))
//                    {
//                        cmd = GetSETVALCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("SETNULLPAR.") && (removedSeparator))
//                    {
//                        cmd = GetSETNULLPARCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("SETNULLPAREXT.") && (removedSeparator))
//                    {
//                        cmd = GetSETNULLPAREXTCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("SETNULLFLD.") && (removedSeparator))
//                    {
//                        cmd = GetSETNULLFLDCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("SETNULLFLDEXT.") && (removedSeparator))
//                    {
//                        cmd = GetSETNULLFLDEXTCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("SETNULLVAL.") && (removedSeparator))
//                    {
//                        cmd = GetSETNULLVALCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("JPATHDATA.") && (removedSeparator))
//                    {
//                        cmd = GetXPATHCmd(txt, initialIndex, false, true, true);
//                    }
//                    else if (txt.ToUpper().StartsWith("XPATHDATA.") && (removedSeparator))
//                    {
//                        cmd = GetXPATHCmd(txt, initialIndex, false, false, true);
//                    }
//                    else if (txt.ToUpper().StartsWith("TXPATHDATA.") && (removedSeparator))
//                    {
//                        cmd = GetXPATHCmd(txt, initialIndex, true, false, true);
//                    }
//                    else if (txt.ToUpper().StartsWith("JPATH.") && (removedSeparator))
//                    {
//                        cmd = GetXPATHCmd(txt, initialIndex, false, true, false);
//                    }
//                    else if (txt.ToUpper().StartsWith("XPATH.") && (removedSeparator))
//                    {
//                        cmd = GetXPATHCmd(txt, initialIndex, false, false, false);
//                    }
//                    else if (txt.ToUpper().StartsWith("TXPATH.") && (removedSeparator))
//                    {
//                        cmd = GetXPATHCmd(txt, initialIndex, true, false, false);
//                    }
//                    else if (txt.ToUpper().StartsWith("SPARP."))
//                    {
//                        cmd = GetDefParCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("SPARA."))
//                    {
//                        cmd = GetDefParCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("SPAR."))
//                    {
//                        cmd = GetDefParCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("JPARP.") && (removedSeparator))
//                    {
//                        cmd = GetJPARCmd(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("JPAR.") && (removedSeparator))
//                    {
//                        cmd = GetJPARCmd(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("JDATA.") && (removedSeparator))
//                    {
//                        cmd = GetJDATACmd(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("JKEYS.") && (removedSeparator))
//                    {
//                        cmd = GetJKEYSCmd(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("JLOOP") && (removedSeparator))
//                    {
//                        cmd = GetJLOOPCmd(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("EXIT."))
//                    {
//                        cmd = GetEXITCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("IIF."))
//                    {
//                        cmd = GetIIFCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("REPDATA.") && (removedSeparator))
//                    {
//                        cmd = GetREPCmd(txt, initialIndex, true);
//                    }
//                    else if (txt.ToUpper().StartsWith("REP.") && (removedSeparator))
//                    {
//                        cmd = GetREPCmd(txt, initialIndex, false);
//                    }
//                    else if (txt.ToUpper().StartsWith("COUNT.") && (removedSeparator))
//                    {
//                        cmd = GetCOUNTCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("QRYDATA.") && (removedSeparator))
//                    {
//                        cmd = GetQRYCmd(txt, initialIndex, true);
//                    }
//                    else if (txt.ToUpper().StartsWith("QRY.") && (removedSeparator))
//                    {
//                        cmd = GetQRYCmd(txt, initialIndex, false);
//                    }
//                    else if (txt.ToUpper().StartsWith("PAR.") && (removedSeparator))
//                    {
//                        cmd = GetPARCmd(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("FLD.") && (removedSeparator))
//                    {
//                        cmd = GetFLDCmd(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("FLDBIN.") && (removedSeparator))
//                    {
//                        cmd = GetFLDBINCmd(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("ATTACH.") && (removedSeparator))
//                    {
//                        cmd = GetAttachCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("INCLUDEONCE"))
//                    {
//                        cmd = GetIncludeOnceCommand(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("PARINCLUDE."))
//                    {
//                        cmd = GetParIncludeCommand(txt, initialIndex);
//                    }
//                    else if ((txt.ToUpper().StartsWith("INCLUDE(") || txt.ToUpper().StartsWith("UDF(")))
//                    {
//                        cmd = GetIncludeCommand(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("IMPORTFLD."))
//                    {
//                        cmd = GetImportFldCommand(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("IMPORTSET."))
//                    {
//                        cmd = GetImportSetCommand(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("IMPORT."))
//                    {
//                        cmd = GetImportCommand(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("EXPORT."))
//                    {
//                        cmd = GetExportCommand(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("DELETETEMPLATE."))
//                    {
//                        cmd = GetDeleteTemplateCommand(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("TRANSFERLIKE."))
//                    {
//                        cmd = GetTransferLikeCommand(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("TRANSFER."))
//                    {
//                        cmd = GetTransferCommand(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("CFG."))
//                    {
//                        cmd = GetCfgCommand(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("USEPARAMETERS."))
//                    {
//                        cmd = GetUseParameters(txt);
//                    }
//                    else if (txt.ToUpper().StartsWith("FUNC."))
//                    {
//                        cmd = GetFUNCCommand(txt);
//                    }
//                    else if ((txt.ToUpper().StartsWith("IF") || txt.ToUpper().StartsWith("ELSEIF")) && (removedSeparator))
//                    {
//                        // because of a limitation of generalFieldREx which does not report the starting "#" 
//                        //when the IF condition contains brackets (other than the start and end brackets)
//                        int ifIndex = removedSeparator ? initialIndex : initialIndex - 1;

//                        cmd = GetIFCommand(txt, ifIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("DEFPAR."))
//                    {
//                        cmd = GetDefParCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("DEFQRY."))
//                    {
//                        cmd = GetDefQryCmd(txt, initialIndex);
//                    }
//                    else if (txt.ToUpper().StartsWith("MSG."))
//                    {
//                        cmd = GetMsgCmd(txt, initialIndex);
//                    }
//                    // else if (cmd == null && txt.IndexOf("#") == -1 && !txt.ToUpper().StartsWith("ENDXPATH") && !txt.ToUpper().StartsWith("ENDREP")
//                    //&& !txt.ToUpper().StartsWith("ENDQRY") && !txt.ToUpper().StartsWith("ENDIF"))
//                    else if (cmd == null && !txt.ToUpper().StartsWith("ENDJLOOP") && !txt.ToUpper().StartsWith("ENDJKEYS") && !txt.ToUpper().StartsWith("ENDJPATH") && !txt.ToUpper().StartsWith("ENDXPATH") && !txt.ToUpper().StartsWith("ENDREP")
//                        && !txt.ToUpper().StartsWith("ENDQRY") && !txt.ToUpper().StartsWith("ENDIF"))
//                    {
//                        cmd = new CTextCmd();
//                        if (forceText)
//                        {
//                            if (txt.ToUpper() == END_BLOCK)
//                                txt = "#" + END_BLOCK + "#";
//                            (cmd as CTextCmd).ForceText(txt);
//                        }
//                        else
//                            (cmd as CTextCmd).Text = txt;
//                        //if ((cmd as CTextCmd).Text == "")
//                        //    cmd = null;
//                    }
//                }
//                else
//                {
//                    cmd = new CTextCmd();
//                    string blockContent = "";

//                    int endBlockLen = 2 * 1 + (m_commentBlock ? END_COMMENT.Length : END_BLOCK.Length);

//                    int i1 = m_text.ToUpper().IndexOf("#" + (m_commentBlock ? END_COMMENT : END_BLOCK) + "#", initialIndex + 1);
//                    if (i1 != -1 && mc != null)
//                    {
//                        blockContent = m_text.ToString().Substring(initialIndex, i1 - initialIndex + endBlockLen);
//                        string m_partialText = m_text.Substring(i1 + endBlockLen);
//                        m_partialTextOffset = i1 + endBlockLen;
//                        mc = regexXDoc.Matches(m_partialText);
//                        //matchIndex = -1;
//                        m_skipCurrentMatch = true;
//                        (cmd as CTextCmd).ForceText(blockContent);
//                        m_ignoreBlock = false;
//                    }
//                }



//                if (cmd != null)
//                {
//                    cmd.StartIndex = initialIndex;
//                    cmd.Line = GetLine(initialIndex);
////                    cmd.Parser = this;
//                }
//                else if (!acceptClosingBlockCommands)
//                    throw new Exception(" ");

//            }
//            catch (Exception ex)
//            {
//                string message = string.Format("Could not create new command from text {0} at line {1}", txt, GetLine(initialIndex)) + Environment.NewLine + ex.Message;
//                throw new Exception(message);
//            }
//            return cmd;
//        }

//        #endregion

    }
}
