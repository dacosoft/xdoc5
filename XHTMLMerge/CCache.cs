﻿#region using
using System;
using System.Data;
using System.Collections;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Web;
using System.Text;
using System.Configuration;
//using KubionLogNamespace;
//using XDataSourceModule;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Xml;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading;
using KubionDataNamespace;
#endregion


namespace XHTMLMerge
{

    //class RijndaelExample
    //    {
    //        public static void Main()
    //        {
    //            try
    //            {

    //                string original = "Here is some data to encrypt!";

    //                // Create a new instance of the RijndaelManaged 
    //                // class.  This generates a new key and initialization  
    //                // vector (IV). 
    //                using (RijndaelManaged myRijndael = new RijndaelManaged())
    //                {

    //                    myRijndael.GenerateKey();
    //                    myRijndael.GenerateIV();
    //                    // Encrypt the string to an array of bytes. 
    //                    byte[] encrypted = EncryptStringToBytes(original, myRijndael.Key, myRijndael.IV);

    //                    // Decrypt the bytes to a string. 
    //                    string roundtrip = DecryptStringFromBytes(encrypted, myRijndael.Key, myRijndael.IV);

    //                    //Display the original data and the decrypted data.
    //                    Console.WriteLine("Original:   {0}", original);
    //                    Console.WriteLine("Round Trip: {0}", roundtrip);
    //                }

    //            }
    //            catch (Exception e)
    //            {
    //                Console.WriteLine("Error: {0}", e.Message);
    //            }
    //        }

    //        static byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
    //        {
    //            // Check arguments. 
    //            if (plainText == null || plainText.Length <= 0)
    //                throw new ArgumentNullException("plainText");
    //            if (Key == null || Key.Length <= 0)
    //                throw new ArgumentNullException("Key");
    //            if (IV == null || IV.Length <= 0)
    //                throw new ArgumentNullException("IV");
    //            byte[] encrypted;
    //            // Create an RijndaelManaged object 
    //            // with the specified key and IV. 
    //            using (RijndaelManaged rijAlg = new RijndaelManaged())
    //            {
    //                rijAlg.Key = Key;
    //                rijAlg.IV = IV;

    //                // Create a decryptor to perform the stream transform.
    //                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

    //                // Create the streams used for encryption. 
    //                using (MemoryStream msEncrypt = new MemoryStream())
    //                {
    //                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
    //                    {
    //                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
    //                        {

    //                            //Write all data to the stream.
    //                            swEncrypt.Write(plainText);
    //                        }
    //                        encrypted = msEncrypt.ToArray();
    //                    }
    //                }
    //            }


    //            // Return the encrypted bytes from the memory stream. 
    //            return encrypted;

    //        }
    //        static string DecryptStringFromBytes(byte[] cipherText, byte[] Key, byte[] IV)
    //        {
    //            // Check arguments. 
    //            if (cipherText == null || cipherText.Length <= 0)
    //                throw new ArgumentNullException("cipherText");
    //            if (Key == null || Key.Length <= 0)
    //                throw new ArgumentNullException("Key");
    //            if (IV == null || IV.Length <= 0)
    //                throw new ArgumentNullException("IV");

    //            // Declare the string used to hold 
    //            // the decrypted text. 
    //            string plaintext = null;

    //            // Create an RijndaelManaged object 
    //            // with the specified key and IV. 
    //            using (RijndaelManaged rijAlg = new RijndaelManaged())
    //            {
    //                rijAlg.Key = Key;
    //                rijAlg.IV = IV;

    //                // Create a decrytor to perform the stream transform.
    //                ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

    //                // Create the streams used for decryption. 
    //                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
    //                {
    //                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
    //                    {
    //                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
    //                        {

    //                            // Read the decrypted bytes from the decrypting stream 
    //                            // and place them in a string.
    //                            plaintext = srDecrypt.ReadToEnd();
    //                        }
    //                    }
    //                }

    //            }

    //            return plaintext;

    //        }
    //    }


    /***SimplerAES***
    public class SimplerAES
    {
        private static byte[] key = { 123, 217, 19, 11, 24, 26, 85, 45, 114, 184, 27, 162, 37, 112, 222, 209, 241, 24, 175, 144, 173, 53, 196, 29, 24, 26, 17, 218, 131, 236, 53, 209 };
        private static byte[] vector = { 146, 64, 191, 111, 23, 3, 113, 119, 231, 121, 221, 112, 79, 32, 114, 156 };
        private ICryptoTransform encryptor, decryptor;
        private UTF8Encoding encoder;

        public SimplerAES()
        {
            RijndaelManaged rm = new RijndaelManaged();
            encryptor = rm.CreateEncryptor(key, vector);
            decryptor = rm.CreateDecryptor(key, vector);
            encoder = new UTF8Encoding();
        }

        public string Encrypt(string unencrypted)
        {
            return Convert.ToBase64String(Encrypt(encoder.GetBytes(unencrypted)));
        }

        public string Decrypt(string encrypted)
        {
            return encoder.GetString(Decrypt(Convert.FromBase64String(encrypted)));
        }

        public byte[] Encrypt(byte[] buffer)
        {
            return Transform(buffer, encryptor);
        }

        public byte[] Decrypt(byte[] buffer)
        {
            return Transform(buffer, decryptor);
        }

        protected byte[] Transform(byte[] buffer, ICryptoTransform transform)
        {
            MemoryStream stream = new MemoryStream();
            using (CryptoStream cs = new CryptoStream(stream, transform, CryptoStreamMode.Write))
            {
                cs.Write(buffer, 0, buffer.Length);
            }
            return stream.ToArray();
        }
    }
     ***/

    [Serializable]
    public class SVCache 
	{
        #region Protected Members
        protected SVCache m_SVCache0 = null;
        // protected XProviderData m_dataProvider = null;
        protected Hashtable m_ContextSessionVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Hashtable m_ContextSessionVariablesString = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected string sSessionID = "";
        protected Hashtable m_TemplateOnce = CollectionsUtil.CreateCaseInsensitiveHashtable();
        private bool bReplaceEmptySV = false;
        #endregion
        public Hashtable aCookies = CollectionsUtil.CreateCaseInsensitiveHashtable();

        public SVCache()
        {
            sSessionID = System.Guid.NewGuid().ToString("N");
            bReplaceEmptySV = ((string)ConfigurationManager.AppSettings["ReplaceEmptySV"] == "1");
        }
        private string DelHash(Hashtable hash, string skey)
        {
            if (skey == "") { hash.Clear(); return hash.Count.ToString(); }
            else if (hash.ContainsKey(skey)) { hash.Remove(skey); return skey; }
            else return "";
        }
        private string GetHashKeys(Hashtable hash)
        {
            string sResult = "[";
            string sSep = "";
            foreach (string key in hash.Keys)
            {
                sResult += sSep + "\"" + key.Replace("\\", "/") + "\"";
                sSep = ",";
            }
            sResult += "]";
            return sResult;
        }
        private string Hashtable2Json(Hashtable hash)
        {
            StringBuilder sJSON = new StringBuilder();
            sJSON.Append("{");
            bool bFirst = true;
            SortedList vecKeys = new SortedList();
            foreach (string key in hash.Keys)
                vecKeys.Add(key, key);

            foreach (string key in vecKeys.Values)
            {
                if (bFirst) bFirst = false;
                else sJSON.Append(",");
                if (hash[key] is Hashtable)
                {
                    sJSON.Append("\""); sJSON.Append(Utils._JsonEscape(key)); sJSON.Append("\":"); sJSON.Append(Hashtable2Json((Hashtable)hash[key])); sJSON.Append("");
                }
                else
                {
                    sJSON.Append("\""); sJSON.Append(Utils._JsonEscape(key)); sJSON.Append("\":\""); sJSON.Append(Utils._JsonEscape(hash[key].ToString())); sJSON.Append("\"");
                }

            }
            sJSON.Append("}");
            JToken ob = JToken.Parse(sJSON.ToString());
            string sResponse = ob.ToString(Newtonsoft.Json.Formatting.Indented);
            return sResponse;
        }
        public string SVCacheCommand(string sWhat)
        {
            string key = "";
            if (sWhat.IndexOf("/") > 0)
            {
                key = sWhat.Substring(sWhat.IndexOf("/") + 1);
                sWhat = sWhat.Substring(0, sWhat.IndexOf("/"));
                key = key.Replace("/", "\\");

            }
            sWhat = sWhat.ToUpper();
            if (sWhat == "TEMPLATEONCE") return GetHashKeys(m_TemplateOnce);
            if (sWhat == "TEMPLATEONCEDEL") return DelHash(m_TemplateOnce, key);
            if (sWhat == "TEMPLATEONCECLEAR") return DelHash(m_TemplateOnce, "");
            if (sWhat == "GET") return Hashtable2Json(m_ContextSessionVariables);
            if (sWhat == "DEL") return DelHash(m_ContextSessionVariables, key);
            if (sWhat == "CLEAR") ClearCache("");
            if (sWhat == "CLEAR0") ClearCache("0");
            return "";
        }

        public string GetIncludeOnce()
        {
            string sVal = "";
            //Creates vector object
            SortedList vecKeys1 = new SortedList();

            foreach (string key in m_TemplateOnce.Keys)
            {
                vecKeys1.Add(key, key);
            }

            foreach (string key in vecKeys1.Values)
            {
                sVal += key + "|";
            }

            return sVal;
        }
        public bool SetIncludeOnce(string sTemplateName)
        {
            if (m_TemplateOnce.ContainsKey(sTemplateName)) return false;
            m_TemplateOnce[sTemplateName] = 1;
            return true;
        }
        public void ClearIncludeOnce()
        {
            m_TemplateOnce.Clear();
        }

        static public string ReplaceString(string str, string oldValue, string newValue, StringComparison comparison)
        {
            StringBuilder sb = new StringBuilder();

            int previousIndex = 0;
            int index = str.IndexOf(oldValue, comparison);
            while (index != -1)
            {
                sb.Append(str.Substring(previousIndex, index - previousIndex));
                sb.Append(newValue);
                index += oldValue.Length;

                previousIndex = index;
                index = str.IndexOf(oldValue, index, comparison);
            }
            sb.Append(str.Substring(previousIndex));

            return sb.ToString();
        }

        #region SVCache methods

        public string SessionID
        {
            get { return sSessionID; }
            set { sSessionID = value; }
        }
        public void ClearCache(string sContextName)
        {
            if (sContextName.StartsWith("0"))
            {
                LoadSVCache0();
                m_SVCache0.ClearCache(sContextName.Substring(1));
                return;
            }
            m_ContextSessionVariables.Clear();
            m_ContextSessionVariablesString.Clear();
        }
        public void FillCookies(HttpCookieCollection col)
        {
            DelSV("", "Response_", false);
            DelSV("", "Response_Headers", false);
            DelSV("", "cookies_", false);
            foreach (string skey in col)
            {
                HttpCookie key = col[skey];
                SetSV(key.Name, "cookies_", key.Value);
            }
        }
        public void WriteCookies(HttpCookieCollection col)
        {
            string sContextName = "cookies_";
            Hashtable m_SessionVariables = (Hashtable)m_ContextSessionVariables[sContextName];
            foreach (string key in m_SessionVariables.Keys)
            {
                if (key.EndsWith("__value"))
                {
                    try
                    {
                        string name = key.Substring(0, key.Length - "__value".Length);
                        HttpCookie cookie = new HttpCookie("");
                        cookie.Name = name;
                        cookie.Value = m_SessionVariables[key].ToString();
                        if (m_SessionVariables.ContainsKey(name + "__domain"))
                            cookie.Domain = m_SessionVariables[name + "__domain"].ToString();
                        if (m_SessionVariables.ContainsKey(name + "__path"))
                            cookie.Path = m_SessionVariables[name + "__path"].ToString();
                        if (m_SessionVariables.ContainsKey(name + "__expires"))
                            cookie.Expires = Convert.ToDateTime(m_SessionVariables[name + "__expires"].ToString());
                        if (m_SessionVariables.ContainsKey(name + "__httponly"))
                            cookie.HttpOnly = (m_SessionVariables[name + "__httponly"].ToString() == "1");
                        if (m_SessionVariables.ContainsKey(name + "__secure"))
                            cookie.Secure = (m_SessionVariables[name + "__secure"].ToString() == "1");
                        if (m_SessionVariables.ContainsKey(name + "__shareable"))
                            cookie.Shareable = (m_SessionVariables[name + "__shareable"].ToString() == "1");
                        col.Add(cookie);
                    }
                    catch (Exception )
                    { }
                }
            }
        }

        public string GetSV(string sVarName, string sContextName, string sDefault)
        {
            if (sContextName.StartsWith("0"))
            {
                LoadSVCache0();
                return m_SVCache0.GetSV(sVarName, sContextName.Substring(1), sDefault);
            }
            CreateSVContext(sContextName);
            Hashtable m_SessionVariables =(Hashtable ) m_ContextSessionVariables[sContextName];
            string sVal = sDefault;

            if (sVarName == "*")
            {
                sVal = "";

                //Creates vector object
                SortedList  vecKeys = new SortedList ();

                foreach (string key in m_SessionVariables.Keys)
                {
                    vecKeys.Add(key, key);
                }

                foreach (string key in  vecKeys.Values )
                {
                    String value = m_SessionVariables[key].ToString ();
                    sVal += key.Replace("=", "\\=").Replace(";", "\\;") + "=" + value.Replace("=", "\\=").Replace(";", "\\;") + ";";
                }

                //foreach (DictionaryEntry entry in m_SessionVariables)
                //{
                //    sVal += entry.Key.ToString().Replace("=", "\\=").Replace(";", "\\;") + "=" + entry.Value.ToString().Replace("=", "\\=").Replace(";", "\\;") + ";";
                //}
            }
            else if (sVarName == "INCLUDEONCE")
            {
                sVal = GetIncludeOnce();
            }
            else if (sVarName == "ALLCONTEXTS")
            {
                sVal = "";
                //Creates vector object
                SortedList vecKeys1 = new SortedList();

                foreach (string key in m_ContextSessionVariables.Keys)
                {
                    vecKeys1.Add(key, key);
                }

                foreach (string key in vecKeys1.Values)
                {
                    m_SessionVariables = (Hashtable)m_ContextSessionVariables[key];
                    if (m_SessionVariables.Count > 0)
                        sVal += key + "|";
                }


                //foreach (DictionaryEntry entry in m_ContextSessionVariables)
                //{
                //    m_SessionVariables = (Hashtable)m_ContextSessionVariables[entry.Key];
                //    if (m_SessionVariables.Count > 0)
                //        sVal += entry.Key + "|";
                //}

            }
            else
            {
                sVal = sDefault;
                if (m_SessionVariables.ContainsKey(sVarName))
                    sVal = m_SessionVariables[sVarName].ToString();
                else
                    if (!bReplaceEmptySV)
                        if (sVal == "") sVal = sDefault;
                //sVal = ReplaceSettings(sVal, sContextName);
                // do not replace setting here but in the CParser
            }
            return sVal;
        }
        public void SetSV(string sVarName, string sContextName, string sVal)
        {
            if (sContextName.StartsWith("0"))
            {
                LoadSVCache0();
                m_SVCache0.SetSV(sVarName, sContextName.Substring(1), sVal);
                return;
            }
            CreateSVContext(sContextName);
            Hashtable m_SessionVariables = (Hashtable) m_ContextSessionVariables[sContextName];
            string s_SessionVariables = (string)m_ContextSessionVariablesString[sContextName];
            if (sVarName == "*")
            {
                DelSV("", sContextName, true);
                //split sVal
                //sVal = sVal.Replace("\\;", "<tilda>");
                //string[] aRows = sVal.Split(';');
                string[] aRows =Utils.MySplit(  sVal,';');

                for (int i = 0; i < aRows.Length; i++)
                {
                    //aRows[i] = aRows[i].Replace("<tilda>", ";");
                    //aRows[i] = aRows[i].Replace("\\=", "<tilda>");
                    //aRows[i] = aRows[i] + "=";
                    //string[] aCels = aRows[i].Split('=');
                    //string sVar1 = aCels[0].Replace("<tilda>", "=");
                    //string sVal1 = aCels[1].Replace("<tilda>", "=");
                    aRows[i] = aRows[i] + "=";
                    string[] aCels = Utils.MySplit(aRows[i],'=');
                    if (aCels[0] != "")
                        SetSV(aCels[0], sContextName, aCels[1]);
                }
            }
            else if (sVal == "-delete")
            {
                DelSV(sVarName, sContextName, false);
            }

            else
            {

                if (m_SessionVariables.Contains(sVarName))
                {
                    s_SessionVariables = ReplaceString(s_SessionVariables, "|" + sVarName + "|", "|", StringComparison.CurrentCultureIgnoreCase);
                    //s_SessionVariables = s_SessionVariables.Replace("|" + sVarName + "|", "|");
                }
                m_SessionVariables[sVarName] = sVal;
                s_SessionVariables += sVarName + "|";
                m_ContextSessionVariables[sContextName] = m_SessionVariables;
                m_ContextSessionVariablesString[sContextName] = s_SessionVariables;
            }
        }
        public void DelSV(string sVarName, string sContextName, bool delfrom)
        {
            if (sContextName.StartsWith("0"))
            {
                LoadSVCache0();
                m_SVCache0.DelSV(sVarName, sContextName.Substring(1), delfrom);
                return;
            }
            if (sContextName == "*")
            {
                //m_ContextSessionVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
                //m_ContextSessionVariablesString = CollectionsUtil.CreateCaseInsensitiveHashtable();
                foreach (DictionaryEntry entry in m_ContextSessionVariables)
                {
                    //m_ContextSessionVariables[entry.Key] = CollectionsUtil.CreateCaseInsensitiveHashtable();
                    m_ContextSessionVariablesString[entry.Key] = "|";
                }
                foreach (DictionaryEntry entry in m_ContextSessionVariablesString)
                {
                    m_ContextSessionVariables[entry.Key] = CollectionsUtil.CreateCaseInsensitiveHashtable();
                    //m_ContextSessionVariablesString[entry.Key] = "|";
                }
            }
            else
            {
                CreateSVContext(sContextName);
                if (sVarName == "")
                {
                    m_ContextSessionVariables[sContextName] = CollectionsUtil.CreateCaseInsensitiveHashtable();
                    m_ContextSessionVariablesString[sContextName] = "|";
                }
                else
                    if (sVarName == "*")
                    {
                        m_ContextSessionVariables[sContextName] = CollectionsUtil.CreateCaseInsensitiveHashtable();
                        m_ContextSessionVariablesString[sContextName] = "|";
                    }
                    else
                    {
                    Hashtable m_SessionVariables = (Hashtable)m_ContextSessionVariables[sContextName];
                    string s_SessionVariables = (string)m_ContextSessionVariablesString[sContextName];
                    if (m_SessionVariables.Contains(sVarName))
                    {
                        if (delfrom)
                        {
                            string[] a_SessionVariables = s_SessionVariables.Substring(s_SessionVariables.IndexOf("|" + sVarName + "|",StringComparison.CurrentCultureIgnoreCase  )).Split('|');
                            foreach (string isVarName in a_SessionVariables)
                            {
                                if (isVarName != "")
                                {
                                    m_SessionVariables.Remove(isVarName);
                                    s_SessionVariables = ReplaceString(s_SessionVariables, "|" + isVarName + "|", "|", StringComparison.CurrentCultureIgnoreCase);
                                    //s_SessionVariables = s_SessionVariables.Replace("|" + isVarName + "|", "|");
                                }
                            }
                        }
                        else
                        {
                            m_SessionVariables.Remove(sVarName);
                            s_SessionVariables = ReplaceString(s_SessionVariables, "|" + sVarName + "|", "|", StringComparison.CurrentCultureIgnoreCase);
                            //s_SessionVariables = s_SessionVariables.Replace("|" + sVarName + "|", "|");
                        }
                        m_ContextSessionVariables[sContextName] = m_SessionVariables;
                        m_ContextSessionVariablesString[sContextName] = s_SessionVariables;
                    }
                }
            }
        }
        private void CreateSVContext(string sContextName)
        {
            if (!m_ContextSessionVariables.Contains(sContextName))
            {
                Hashtable m_SessionVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
                string s_SessionVariables = "|";
                m_ContextSessionVariables[sContextName] = m_SessionVariables;
                m_ContextSessionVariablesString[sContextName] = s_SessionVariables;
            }
        }
        public void LoadSVCache0()
        {
            HttpContext m_context = HttpContext.Current;
            if ((m_context != null) && (m_context.Application != null)) m_SVCache0 = (SVCache)m_context.Application["SVCache0"];
            if (m_SVCache0 == null) m_SVCache0 = new SVCache();
        }
        public void SaveSVCache0()
        {
            if (m_SVCache0 != null)
            {
                HttpContext m_context = HttpContext.Current;
                if ((m_context != null) && (m_context.Application != null)) m_context.Application["SVCache0"] = m_SVCache0;
            }
        }
        #endregion

    }
    [Serializable]
    public class AppCache
    {
        #region Protected Members
        protected Hashtable m_ConfigVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Hashtable m_TemplateID2Name = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Hashtable m_TemplateTree = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Hashtable m_TemplateTree4 = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Hashtable m_TemplateText = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected ClientData m_clientData = null;
        protected string m_XFilesPath = "";
        protected string m_XDocPath = "XDoc";
        protected string m_XBinPath = "";
        protected string m_XLibPath = "";
        protected string m_sConnections = "";
        protected bool bBak = false;


        #endregion

        public ClientData ClientData
        {
            get { return m_clientData; }
            set { m_clientData = value; }
        }
        public string sConnections
        {
            get
            {
                if (m_sConnections == "")
                {
                    string sConnectionsFile1 = m_XFilesPath + "S_Connections.json";
                    string sConnectionsFile2 = m_XDocPath + "S_Connections.json";
                    if (File.Exists(sConnectionsFile1))
                        m_sConnections = File.ReadAllText(sConnectionsFile1);
                    else if (File.Exists(sConnectionsFile2))
                        m_sConnections = File.ReadAllText(sConnectionsFile2);
                }
                return m_sConnections;
            }
            set
            {
                string sConnectionsFile1 = m_XFilesPath + "S_Connections.json";
                if (File.Exists(sConnectionsFile1))
                    File.Delete(sConnectionsFile1);
                WriteAllText(sConnectionsFile1, "{\"sConnections\":" + value + "}");
            }
        }
        //public string XBinPath
        //{
        //    get { return m_XBinPath; }
        //    set { m_XBinPath = value; }
        //}
        //public string XLibPath
        //{
        //    get { return m_XLibPath; }
        //    set { m_XLibPath = value; }
        //}
        public string XFilesPath
        {
            get { return m_XFilesPath; }
//            set { m_JsonPath = value; }
        }

        CCompiler compiler = null;
        CPersister persister = null;

        public AppCache()
        {
            InitPath();
            if (System.Configuration.ConfigurationManager.AppSettings["bak"] != null) bBak = (string)System.Configuration.ConfigurationManager.AppSettings["bak"] == "1";
            //compiler = new CCompiler();
            //persister = new CPersister(m_XDocPath, m_XBinPath);
        }
        private string GetConfigPathCombine(string sName, string sDefault, string m_XFilesPath)
        {//GetFullPath, GetPath, GetFullFilePath, GetConfigPath
            string sPath = ConfigurationManager.AppSettings[sName];
            if (sPath == null) sPath = sDefault;
            if (sPath == null) return "";
            string sFullPath = null;

            if (sPath.StartsWith("~"))
                    sFullPath = GetConfigPath(sName,sDefault);
            else
                //if (Directory.Exists(Path.Combine(m_XFilesPath, sPath)))
                if (Directory.Exists(m_XFilesPath))
                    sFullPath = Path.Combine(m_XFilesPath, sPath);
                else
                    sFullPath = GetConfigPath(sName,sDefault);
            
            if (sFullPath!="") if (!sFullPath.EndsWith("\\")) sFullPath += "\\";
            return sFullPath;
        }
        private string GetConfigPath(string sName, string sDefault)
        {//GetFullPath, GetPath, GetFullFilePath, GetConfigPath
            string sPath = ConfigurationManager.AppSettings[sName];
            if (sPath == null) sPath = sDefault;
            if (sPath == null) return "";
            if (HttpContext.Current != null)
            {
                if (sPath.StartsWith("~"))
                    sPath = HttpContext.Current.Server.MapPath("~") + sPath.Substring(1);
                else
                    if (!Path.IsPathRooted(sPath))
                        if ((HttpContext.Current != null) && (HttpContext.Current.Server != null)) sPath = HttpContext.Current.Server.MapPath(Path.Combine("~", sPath));
            }
            else
            {
                if (sPath.StartsWith("~"))
                    sPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + sPath.Substring(1);
                else
                    if (!Path.IsPathRooted(sPath))
                        sPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, sPath);
            }
           
            //if (!Directory.Exists(sPath))
            //    Directory.CreateDirectory(sPath);
            if (!sPath.EndsWith("\\")) sPath += "\\";
            return sPath;
        }
        private void InitPath()
        {
            m_XFilesPath = GetConfigPath( "XFiles", "");
            m_XDocPath = GetConfigPathCombine("XDoc", "XDoc", m_XFilesPath);
            m_XBinPath = GetConfigPathCombine("XBin", null, m_XFilesPath);
            m_XLibPath = GetConfigPathCombine("XLib", null, m_XFilesPath);
        }


        #region TemplatesCache methods
        private static void WriteAllText(string filePath, string content)
        {
            System.IO.FileInfo file = new System.IO.FileInfo(filePath);
            file.Directory.Create(); // If the directory already exists, this method does nothing.
            System.IO.File.WriteAllText(file.FullName, content);
        }

        private static string L_String(string strString)
        {
            if (strString == null) return "";
            else return strString.Replace("'", "''");
        }

        string sSchedules = "";
        public string GetSchedules()
        {
            if (sSchedules == "")
            {
                string sSchedulesFile = m_XDocPath + "S_Schedules.json";
                string sSchedulesFile1 = m_XFilesPath + "S_Schedules.json";
                if (File.Exists(sSchedulesFile)) sSchedules = File.ReadAllText(sSchedulesFile);
                if (File.Exists(sSchedulesFile1)) sSchedules = File.ReadAllText(sSchedulesFile1);
            }
/*
             if (sSchedules != "")
            {
                JToken jt = (JObject.Parse(sSchedules)).SelectToken("Schedules");
                if (jt is JArray)
                {
                    JArray ja = (JArray)jt;
                    for (int i = 0; i < ja.Count; i++)
                    {
                        JToken j = ja[i];
                        string sID = "";
                        string sTemplate = "";
                        string sInterval = "";
                        string sEnabled = "1";
                        int ID = 0, iInterval = 0;
                        foreach (JProperty jp in j.Children<JProperty>())
                        {
                            if (jp.Name.ToLower() == "id") sID = jp.Value.ToString();
                            if (jp.Name.ToLower() == "template") sTemplate = jp.Value.ToString();
                            if (jp.Name.ToLower() == "interval") sInterval = jp.Value.ToString();
                            if (jp.Name.ToLower() == "enabled") sEnabled = jp.Value.ToString();
                        }
                        if ((sEnabled == "1") && int.TryParse(sID, out ID) && int.TryParse(sInterval, out iInterval) && (iInterval > 0) && (sTemplate != ""))
                        {
                            System.Diagnostics.Debug.WriteLine("sID: " + sID + " sTemplate: " + sTemplate + " sInterval: " + sInterval + " sEnabled: " + sEnabled + " ");
                        
                        }
                    }
                }
            }

*/
                            return sSchedules;
        }
        private int CompileDirectory(string targetDirectory, StringBuilder sb, bool only)
        {
            int i = 0;
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
                if (fileName.EndsWith(".xdoc"))
                    i += CompileFile(fileName, sb);
            if (!only)
            {
                string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
                foreach (string subdirectory in subdirectoryEntries)
                    i += CompileDirectory(subdirectory, sb, false);
            }
            return i;
        }

        // Insert logic for processing found files here.
        private int CompileFile(string targetFile, StringBuilder sb)
        {
            if (sb.Capacity > 5050000) return 0;
            string templateName = targetFile.Substring(m_XDocPath.Length);
            templateName = templateName.Substring(0, templateName.Length - 5);
            templateName = templateName.Replace("/", "\\");
            sb.Append(",{\"Name\":\""); sb.Append(Utils._JsonEscape(templateName)); sb.Append("\"");
            sb.Append(",\"ElseIf\":");
            //CompileTemplate(templateName, sb, false);
            CompileTemplate(templateName, sb, true);
            sb.Append("}");
            Console.WriteLine("Processed file '{0}'.", targetFile);
            return 1;
        }

        public string CompiledLibrary(string strTemplatePath)
        {
            int iResult = 0;
            if ((m_XLibPath != "") && (m_XDocPath != ""))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("{\"Name\":\""); sb.Append(Utils._JsonEscape(strTemplatePath)); sb.Append("\"");
                sb.Append(",\"Commands\":[{}");
                if (strTemplatePath.EndsWith("__only"))
                    iResult = CompileDirectory(m_XDocPath + strTemplatePath.Substring(0, strTemplatePath.Length - "__only".Length), sb, true);
                else
                    iResult = CompileDirectory(m_XDocPath + strTemplatePath, sb,false);
                sb.Append("]}");
                if (sb.Capacity <= 5050000)
                {
                    string sCompiled = sb.ToString();
                    if (File.Exists(m_XLibPath + strTemplatePath + ".xlib"))
                        File.Delete(m_XLibPath + strTemplatePath + ".xlib");
                    string sCompiledEnc = new SimplerAES().Encrypt(sCompiled);
                    /*
                                    System.Diagnostics.Debug.WriteLine("WRITE LIB " + strTemplatePath + ": " + iResult.ToString() + "templates");
                                    System.Diagnostics.Debug.WriteLine("---");
                                    //System.Diagnostics.Debug.WriteLine("" + sCompiled);
                                    System.Diagnostics.Debug.WriteLine("======");
                    */
                    WriteAllText(m_XLibPath + strTemplatePath + ".xlib", sCompiledEnc);
                }
                else iResult = -1 * iResult;
            }
            return iResult.ToString() + " templates";
        }

        private bool RemoveCompiledTemplates(string strTemplateName)
        {
            bool bResult = false;
            if (m_XBinPath != "")
                if (File.Exists(m_XBinPath + strTemplateName + ".xbin"))
                {
                    File.Delete(m_XBinPath + strTemplateName + ".xbin");
                    bResult = true;
                }
            if (m_XDocPath != "")
                if (File.Exists(m_XDocPath + strTemplateName + ".xdoc"))
                {
                    if (File.Exists(m_XDocPath + strTemplateName + ".bak.xdoc"))
                        File.Delete(m_XDocPath + strTemplateName + ".bak.xdoc");
                    if (bBak)
                        File.Move(m_XDocPath + strTemplateName + ".xdoc", m_XDocPath + strTemplateName + ".bak.xdoc");
                    else
                        File.Delete(m_XDocPath + strTemplateName + ".xdoc");
                    bResult = true;
                }
            return bResult;
        }
        private string StoreCompiledTemplate(string strTemplateName, string sCompiled, string sWarning)
        {
            string sResult = strTemplateName;
            if (m_XBinPath != "")
            {
                if (File.Exists(m_XBinPath + strTemplateName + ".xbin"))
                    File.Delete(m_XBinPath + strTemplateName + ".xbin");
                string sCompiledEnc = new SimplerAES().Encrypt(sCompiled);
                System.Diagnostics.Debug.WriteLine("WRITE    " + strTemplateName);
                System.Diagnostics.Debug.WriteLine("---");
                System.Diagnostics.Debug.WriteLine("" + sCompiled);
                System.Diagnostics.Debug.WriteLine("======");

                if (compiler != null)
                {
                    if (File.Exists(m_XBinPath + strTemplateName + ".xbin3"))
                        File.Delete(m_XBinPath + strTemplateName + ".xbin3");
                    JToken ob = JToken.Parse(sCompiled);
                    sCompiled = ob.ToString(Newtonsoft.Json.Formatting.Indented);
                    WriteAllText(m_XBinPath + strTemplateName + ".xbin3", sCompiled);
                }
                //                File.WriteAllText(m_XBinPath + strTemplateName + ".xbin", sCompiledEnc);
                WriteAllText(m_XBinPath + strTemplateName + ".xbin", sCompiledEnc);
                if  (m_XDocPath != "")
                    if (File.Exists(m_XDocPath + strTemplateName + "_warning.txt"))
                        File.Delete(m_XDocPath + strTemplateName + "_warning.txt");
                if ((sWarning != "") && (m_XDocPath != ""))
                    WriteAllText(m_XDocPath + strTemplateName + "_warning.txt", sWarning);
                //try
                //{
                //    StoreCompiledTemplateDB(strTemplateName, sCompiled, sWarning);
                //}
                //catch (Exception ex)
                //{
                //}
            }
            else
                if (m_clientData!=null) return StoreCompiledTemplateDB(strTemplateName, sCompiled, sWarning);
            return sResult;
        }
        private string StoreTemplateContent(string strTemplateName, string sContent)
        {
            string sResult = strTemplateName;
            RemoveCacheTemplate(strTemplateName);
            if (sContent.IndexOf("#startcomment#memory#endcomment#", StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                JCmd oTree = null;
                StringBuilder sbCompile = new StringBuilder();
                CompileContent(strTemplateName, sContent, sbCompile);
                sContent = sbCompile.ToString();
                oTree = Newtonsoft.Json.JsonConvert.DeserializeObject<JCmd>(sContent);
                //if (!(sContent.Contains("\"text\":\"#startcomment#nocache#endcomment#\"")))
                //    if (oTree != null) m_TemplateTree[templateName] = oTree;
                if (sContent.IndexOf("#startcomment#nocache#endcomment#", StringComparison.CurrentCultureIgnoreCase) == -1)
                    if (oTree != null) m_TemplateTree[strTemplateName] = oTree;
            }
            else if (m_XDocPath != "")
            {
                //if (File.Exists(m_XDocPath + strTemplateName + ".xdoc"))
                //    File.Delete(m_XDocPath + strTemplateName + ".xdoc");
                if (File.Exists(m_XDocPath + strTemplateName + ".xdoc"))
                {
                    if (File.Exists(m_XDocPath + strTemplateName + ".bak.xdoc"))
                        File.Delete(m_XDocPath + strTemplateName + ".bak.xdoc");
                    if (bBak)
                        File.Move(m_XDocPath + strTemplateName + ".xdoc", m_XDocPath + strTemplateName + ".bak.xdoc");
                    else
                        File.Delete(m_XDocPath + strTemplateName + ".xdoc");
                }
                WriteAllText(m_XDocPath + strTemplateName + ".xdoc", sContent);
            }
            else
                return StoreTemplateContentDB(strTemplateName, sContent);
            return sResult;
        }

        private string LoadTemplateCompiled(string templateName, ref string templateWarning)
        {
            string sTemplateText = "error";
            if ((m_XDocPath != "") && (m_XBinPath != ""))
            {
                DateTime cdate = FileGetLastWriteTime(m_XBinPath + templateName + ".xbin");
                DateTime tdate = FileGetLastWriteTime(m_XDocPath + templateName + ".xdoc");
                if (cdate > tdate)
                    if (File.Exists(m_XBinPath + templateName + ".xbin"))
                    {
                        sTemplateText = File.ReadAllText(m_XBinPath + templateName + ".xbin");
                        sTemplateText = new SimplerAES().Decrypt(sTemplateText);
                    }
                if (File.Exists(m_XDocPath + templateName + "_warning.txt"))
                    templateWarning = File.ReadAllText(m_XDocPath + templateName + "_warning.txt");
            }
            else if (m_XBinPath != "")
            {
                if (File.Exists(m_XBinPath + templateName + ".xbin"))
                {
                    sTemplateText = File.ReadAllText(m_XBinPath + templateName + ".xbin");
                    sTemplateText = new SimplerAES().Decrypt(sTemplateText);
                }
            }
            else
                sTemplateText = LoadTemplateCompiledDB(templateName, ref templateWarning);
            //System.Diagnostics.Debug.WriteLine("READ    " + templateName);
            //System.Diagnostics.Debug.WriteLine("---");
            //System.Diagnostics.Debug.WriteLine("" + sTemplateText);
            //System.Diagnostics.Debug.WriteLine("======");  
            return sTemplateText;
        }
        public string ExtractTemplatesDB()
        {

            string sTemplateText = "error";
            string sTemplateName = "";
            //string sSQL = "SELECT templatename,templatecontent FROM S_Templates WHERE TemplateToRedirect not like 'compiled%'";
            string sSQL = "SELECT templatename,templatecontent FROM S_Templates ";
            DataTable dt = m_clientData.GetDataTable(sSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    sTemplateName = ClientData.GetStringResult(row["templatename"], "");
                    sTemplateText = ClientData.GetStringResult(row["templatecontent"], "");
                    StoreTemplateContent(sTemplateName, sTemplateText);
                }
            }
            return "Extracted " + dt.Rows.Count.ToString() + " templates!";
        }
        private string CompileDirtyTemplatesDB()
        {
            StringBuilder sbCompile = new StringBuilder();
            string sResponse = "";
            if (m_clientData == null) return "";
            string sTemplateText = "error";
            string sTemplateName = "";
            //string sSQL = "SELECT templatename,templatecontent FROM S_Templates WHERE TemplateToRedirect not like 'compiled%'";
            string sSQL = "SELECT templatename,templatecontent FROM S_Templates ";
            DataTable dt = m_clientData.GetDataTable(sSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    sTemplateName = ClientData.GetStringResult(row["templatename"], "");
                    sTemplateText = ClientData.GetStringResult(row["templatecontent"], "");
                    m_TemplateText[sTemplateName] = sTemplateText;
                    sbCompile.Clear();
                    sResponse += CompileTemplate(sTemplateName, sbCompile, true);
                }
            }
            return sResponse;
        }
        private string LoadTemplateContent(string templateName)
        {
            string sTemplateText = "error";
            if (m_XDocPath != "")
            {
                if (File.Exists(m_XDocPath + templateName + ".xdoc"))
                {
                    sTemplateText = File.ReadAllText(m_XDocPath + templateName + ".xdoc");
                }
                else
                {
                    sTemplateText = LoadTemplateContentDB(templateName);
//                    File.WriteAllText(m_XDocPath + templateName + ".xdoc", sTemplateText);
                    if (sTemplateText!="error")
                        WriteAllText(m_XDocPath + templateName + ".xdoc", sTemplateText);
                }
            }
            else
                sTemplateText = LoadTemplateContentDB(templateName);
            return sTemplateText;
        }
        private string LoadTemplateContent1(string templateName)
        {
            string sTemplateText = "error";
            if (m_XDocPath != "")
            {
                if (File.Exists(m_XDocPath + templateName + ".xdoc"))
                {
                    sTemplateText = File.ReadAllText(m_XDocPath + templateName + ".xdoc");
                }
                else
                {
                    sTemplateText = LoadTemplateContentDB(templateName);
//                    File.WriteAllText(m_XDocPath + templateName + ".xdoc", sTemplateText);
                    if (sTemplateText != "error")
                        WriteAllText(m_XDocPath + templateName + ".xdoc", sTemplateText);
                }
            }
            else
                sTemplateText = LoadTemplateContentDB(templateName);
            return sTemplateText;
        }
        private string LoadTemplateContentDB(string templateName)
        {
            string sTemplateText = "error";
            if (m_clientData != null)
            {
                string sSQL = "SELECT templatename,templatecontent,templatecontentexpanded FROM S_Templates WHERE templatename='" + L_String(templateName) + "'";
                DataTable dt = m_clientData.GetDataTable(sSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    sTemplateText = ClientData.GetStringResult(row["templatecontentexpanded"], "");
                    if ((sTemplateText == "{no_macros}") || (sTemplateText == ""))
                        sTemplateText = ClientData.GetStringResult(row["templatecontent"], "");
                }
            }
            return sTemplateText;
        }
        private string LoadTemplateCompiledDB(string templateName, ref string templateWarning)
        {
            string sTemplateText = "error";
            string sSQL = "SELECT templatename,compiled,warning FROM C_Templates WHERE templatename='" + L_String(templateName) + "' AND exists (SELECT templatename from s_templates where templatename='" + L_String(templateName) + "' and TemplateToRedirect like 'compiled%')";
            DataTable dt = m_clientData.GetDataTable(sSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                templateWarning = ClientData.GetStringResult(row["warning"], "");
                sTemplateText = ClientData.GetStringResult(row["compiled"], "");
            }
            return sTemplateText;
        }
        private string StoreCompiledTemplateDB(string strTemplateName, string sCompiled, string sWarning)
        {
            string sResult = "";
            string sSQL = "";
            try
            {
                sSQL = "DELETE FROM C_Templates WHERE templatename='" + L_String(strTemplateName) + "'; INSERT INTO C_Templates (templatename,compiled,warning) VALUES('" + L_String(strTemplateName) + "','" + L_String(sCompiled) + "','" + L_String(sWarning) + "');  UPDATE S_Templates set TemplateToRedirect = 'compiled " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "' WHERE templatename='" + L_String(strTemplateName) + "'";
                sResult = m_clientData.ExecuteNonQuery(sSQL);
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Invalid object name"))
                {
                    string sMessage1 = @"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[C_Templates]') AND type in (N'U')) 
                                    CREATE TABLE [C_Templates](
                                    	[TemplateName] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[Compiled] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[Warning] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                     CONSTRAINT [PK_C_Templates] PRIMARY KEY CLUSTERED ([TemplateName] ASC) WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]) ON [PRIMARY] ";
                    m_clientData.ExecuteNonQuery(sMessage1 + " " + sSQL);
                }
            }
            return sResult;
        }
        private string StoreTemplateContentDB(string strTemplateName, string sContent)
        {
            string sResult = "";
            string sSQL = "";
            try
            {
                sSQL = "DELETE FROM C_Templates WHERE templatename='" + L_String(strTemplateName) + "'; UPDATE S_Templates SET templatecontent = '" + L_String(sContent) + "'  WHERE templatename='" + L_String(strTemplateName) + "'; IF @@ROWCOUNT = 0 INSERT INTO S_Templates (templatename,TEMPLATECONTENT) VALUES('" + L_String(strTemplateName) + "','" + L_String(sContent) + "';";
                sResult = m_clientData.ExecuteNonQuery(sSQL);
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Invalid object name"))
                {
                    string sMessage1 = @"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[C_Templates]') AND type in (N'U')) 
                                    CREATE TABLE [C_Templates](
                                    	[TemplateName] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[Compiled] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[Warning] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                     CONSTRAINT [PK_C_Templates] PRIMARY KEY CLUSTERED ([TemplateName] ASC) WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]) ON [PRIMARY] ";
                    m_clientData.ExecuteNonQuery(sMessage1 + " " + sSQL);
                }
            }
            return sResult;
        }

            //private string LoadTemplateByID(int templateID,ref string templateName)
        //{
        //    string sTemplateText = "error";
        //    string sSQL = "SELECT templateid,templatename,content FROM C_Templates WHERE templateid='" + templateID.ToString () + "'";
        //    DataTable dt = m_dataProvider.GetDataTable(sSQL);
        //    if (dt != null && dt.Rows.Count > 0)
        //    {
        //        DataRow row = dt.Rows[0];
        //        templateName = ClientData.GetStringResult(row["templatename"], "error");
        //        sTemplateText = ClientData.GetStringResult(row["content"], "");
        //    }
        //    return sTemplateText;

        //}
        public string GetCfg(string sVarName, string sDefault)
        {
            string sVal, sVar;

            if(sVarName=="__TemplateTree")
                return AppCacheCommand("GET");
            else if (sVarName.StartsWith("TemplateTree"))
            {
                if (sVarName.Contains("__"))
                {
                    sVarName = sVarName.Substring(sVarName.IndexOf("__") + 2);
                    if (sVarName == "")
                        sVarName = AppCacheCommand("GET");
                    else if (sVarName == "*")
                    {
                        m_TemplateTree.Clear();
                        m_TemplateTree4.Clear();
                    }
                    else
                    {
                        m_TemplateTree.Remove(sVarName);
                        m_TemplateTree4.Remove(sVarName);
                    }
                    return sVarName;
                }
            }
            if (m_ConfigVariables.Count == 0)
            {
                if (m_XDocPath != "")
                {
                    string sConfigVariables = "";
                    string sConfigVariablesFile = m_XDocPath + "S_ConfigVariables.json";
                    string sConfigVariablesFile1 = m_XFilesPath + "S_ConfigVariables.json";
                    if (File.Exists(sConfigVariablesFile)) sConfigVariables = File.ReadAllText(sConfigVariablesFile);
                    if (File.Exists(sConfigVariablesFile1)) sConfigVariables = File.ReadAllText(sConfigVariablesFile1);
                    if (sConfigVariables != "")
                    {
                        JToken jt = (JObject.Parse(sConfigVariables)).SelectToken("sConfigVariables");
                        if (jt is JArray)
                        {
                            JArray ja = (JArray)jt;
                            for (int i = 0; i < ja.Count; i++)
                            {
                                JToken j = ja[i];
                                string sConfigClass = "";
                                string sConfigName = "";
                                string sValue = "";
                                foreach (JProperty jp in j.Children<JProperty>())
                                {
                                    if (jp.Name == "ConfigClass") sConfigClass = jp.Value.ToString();
                                    if (jp.Name == "ConfigName") sConfigName = jp.Value.ToString();
                                    if (jp.Name == "Value") sValue = jp.Value.ToString();

                                }
                                if (sConfigName != "")
                                {
                                    m_ConfigVariables[sConfigClass + "__" + sConfigName] = sValue;
                                }
                            }
                        }
                    }
                    else if (m_clientData != null)
                    {

                        string sSQL = "SELECT ConfigClass,ConfigName,Value FROM [S_ConfigVariables] ";
                        DataTable dt = m_clientData.GetDataTable(sSQL);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                sVal = ClientData.GetStringResult(row["Value"], "");
                                sVar = ClientData.GetStringResult(row["ConfigClass"], "") + "__" + row["ConfigName"];
                                m_ConfigVariables[sVar] = sVal;
                            }
                        }
                        try
                        {
                            sConfigVariables = Newtonsoft.Json.JsonConvert.SerializeObject(dt);
                            WriteAllText(sConfigVariablesFile1, "{\"sConfigVariables\":" + sConfigVariables + "}");
                        }
                        catch (Exception) { }
                    }
                }
            }

            sVal = sDefault;
            if (!sVarName.Contains("__")) sVarName = "__" + sVarName;
            if (m_ConfigVariables.ContainsKey(sVarName))
            {
                sVal = m_ConfigVariables[sVarName].ToString();
            }
            return sVal;
        }
        private DateTime FileGetLastWriteTime(string sFile)
        {
            DateTime wt = File.GetLastWriteTime(sFile);
            DateTime ct = File.GetCreationTime(sFile);
            if (ct > wt)
                return ct;
            return wt;
        }
        private JCmd LoadTemplateTree(string templateName)
        {
            string sContent = "error";
            JCmd oTree = null;
            JCmd oLib = null;
            DateTime Date_XDoc = DateTime.MinValue;
            DateTime Date_XBin = DateTime.MinValue;
            DateTime Date_XLib = DateTime.MinValue;

            if ((m_XDocPath != "") && (File.Exists(m_XDocPath + templateName + ".xdoc")))
                Date_XDoc = FileGetLastWriteTime(m_XDocPath + templateName + ".xdoc");

            if ((m_XBinPath != "") && (File.Exists(m_XBinPath + templateName + ".xbin")))
                Date_XBin = FileGetLastWriteTime(m_XBinPath + templateName + ".xbin");
            string[] aPaths = templateName.Split('\\');
            string templatePath = "", libPath = "";
            for (int i = 0; i < aPaths.Length ; i++)
            {
                if ((m_XLibPath != "") && (File.Exists(m_XLibPath + templatePath + ".xlib")))
                    if (Date_XLib < FileGetLastWriteTime(m_XLibPath + templatePath + ".xlib"))
                    {
                        Date_XLib = FileGetLastWriteTime(m_XLibPath + templatePath + ".xlib");
                        libPath = templatePath;
                    }
                if (i==aPaths.Length-1)
                    if ((m_XLibPath != "") && (File.Exists(m_XLibPath + templatePath + "__only.xlib")))
                        if (Date_XLib < FileGetLastWriteTime(m_XLibPath + templatePath + "__only.xlib"))
                        {
                            Date_XLib = FileGetLastWriteTime(m_XLibPath + templatePath + "__only.xlib");
                            libPath = templatePath+"__only";
                        }
                templatePath += "\\" + aPaths[i];
            }
            try
            {
                if ((Date_XDoc > Date_XBin) && (Date_XDoc > Date_XLib))
                {
                    System.Diagnostics.Debug.WriteLine("" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - Found template " + templateName + " in xdoc: " + m_XDocPath + templateName + ".xdoc");
                    sContent = File.ReadAllText(m_XDocPath + templateName + ".xdoc");
                    //compile and store if no cache
                    m_TemplateText.Remove(templateName);
                    StringBuilder sbCompile = new StringBuilder();
                    //CompileTemplate(templateName, sbCompile);
                    CompileContent(templateName, sContent, sbCompile);
                    sContent = sbCompile.ToString();
                    oTree = Newtonsoft.Json.JsonConvert.DeserializeObject<JCmd>(sContent);
                    //if (!(sContent.Contains("\"text\":\"#startcomment#nocache#endcomment#\"")))
                    //    if (oTree != null) m_TemplateTree[templateName] = oTree;
                    if (sContent.IndexOf("#startcomment#nocache#endcomment#", StringComparison.CurrentCultureIgnoreCase) == -1)
                        if (oTree != null) m_TemplateTree[templateName] = oTree;
                }
                else if ((Date_XBin > Date_XDoc) && (Date_XBin > Date_XLib))
                {
                    System.Diagnostics.Debug.WriteLine("" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - Found template " + templateName + " in xbin: " + m_XBinPath + templateName + ".xbin");
                    //store if no cache and return
                    m_TemplateText.Remove(templateName);
                    sContent = File.ReadAllText(m_XBinPath + templateName + ".xbin");
                    sContent = new SimplerAES().Decrypt(sContent);
                    oTree = Newtonsoft.Json.JsonConvert.DeserializeObject<JCmd>(sContent);
                    //if (!(sContent.Contains("\"text\":\"#startcomment#nocache#endcomment#\"")))
                    //    if (oTree != null) m_TemplateTree[templateName] = oTree;
                    if (sContent.IndexOf("#startcomment#nocache#endcomment#", StringComparison.CurrentCultureIgnoreCase) == -1)
                        if (oTree != null) m_TemplateTree[templateName] = oTree;
                }
                else if ((Date_XLib > Date_XDoc) && (Date_XLib > Date_XBin))
                {
                    System.Diagnostics.Debug.WriteLine("" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - Found template " + templateName + " in xlib: " + m_XLibPath + libPath + ".xlib");
                    //store lib and query template
                    sContent = File.ReadAllText(m_XLibPath + libPath + ".xlib");
                    sContent = new SimplerAES().Decrypt(sContent);
                    oLib = Newtonsoft.Json.JsonConvert.DeserializeObject<JCmd>(sContent);
                    foreach (JCmd command in oLib.Commands)
                    {
                        if (command.ElseIf != null) m_TemplateTree[command.Name] = command.ElseIf;
                    }
                    if (m_TemplateTree.ContainsKey(templateName))
                        oTree = (JCmd)m_TemplateTree[templateName];
                }
                else
                {
                    //from DB
                    sContent = LoadTemplateContentDB(templateName);
                    m_TemplateText.Remove(templateName);
                    StringBuilder sbCompile = new StringBuilder();
                    //CompileTemplate(templateName, sbCompile);
                    CompileContent(templateName, sContent, sbCompile);
                    sContent = sbCompile.ToString();
                    oTree = Newtonsoft.Json.JsonConvert.DeserializeObject<JCmd>(sContent);
                    //if (!(sContent.Contains("\"text\":\"#startcomment#nocache#endcomment#\"")))
                    //    if (oTree != null) m_TemplateTree[templateName] = oTree;
                    if (sContent.IndexOf("#startcomment#nocache#endcomment#", StringComparison.CurrentCultureIgnoreCase) == -1)
                        if (oTree != null) m_TemplateTree[templateName] = oTree;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "error:" + ex.ToString());
            }
            if(oTree==null)
                System.Diagnostics.Debug.WriteLine("" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - Template not found!!!" );

            return oTree;
        }
        private JCmd4 LoadTemplateTree4(string templateName, int iTry = 0)
        {
            string sContent = "error";
            JCmd4 oTree4 = null;
            JCmd4 oLib4 = null;
            DateTime Date_XDoc = DateTime.MinValue;
            DateTime Date_XBin = DateTime.MinValue;
            DateTime Date_XLib = DateTime.MinValue;

            if (iTry == 5) return null;
            if ((m_XDocPath != "") && (File.Exists(m_XDocPath + templateName + ".xdoc")))
                Date_XDoc = FileGetLastWriteTime(m_XDocPath + templateName + ".xdoc");

            if ((m_XBinPath != "") && (File.Exists(m_XBinPath + templateName + ".xbin4")))
                Date_XBin = FileGetLastWriteTime(m_XBinPath + templateName + ".xbin4");
            string[] aPaths = templateName.Split('\\');
            string templatePath = "", libPath = "";
            for (int i = 0; i < aPaths.Length; i++)
            {
                if ((m_XLibPath != "") && (File.Exists(m_XLibPath + templatePath + ".xlib4")))
                    if (Date_XLib < FileGetLastWriteTime(m_XLibPath + templatePath + ".xlib4"))
                    {
                        Date_XLib = FileGetLastWriteTime(m_XLibPath + templatePath + ".xlib4");
                        libPath = templatePath;
                    }
                if (i == aPaths.Length - 1)
                    if ((m_XLibPath != "") && (File.Exists(m_XLibPath + templatePath + "__only.xlib4")))
                        if (Date_XLib < FileGetLastWriteTime(m_XLibPath + templatePath + "__only.xlib4"))
                        {
                            Date_XLib = FileGetLastWriteTime(m_XLibPath + templatePath + "__only.xlib4");
                            libPath = templatePath + "__only";
                        }
                templatePath += "\\" + aPaths[i];
            }
            try
            {
                if ((Date_XDoc > Date_XBin) && (Date_XDoc > Date_XLib))
                {
                    System.Diagnostics.Debug.WriteLine("" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - Found template " + templateName + " in xdoc: " + m_XDocPath + templateName + ".xdoc");
                    sContent = File.ReadAllText(m_XDocPath + templateName + ".xdoc");
                    //compile and store if no cache
                    m_TemplateText.Remove(templateName);
                    StringBuilder sbCompile4 = new StringBuilder();
                    CCompiler compiler = new CCompiler();
                    if (compiler != null)
                    {
                        string sWarning4 = compiler.CompileWarning(sContent, sbCompile4);
                        CPersister persister = new CPersister(m_XDocPath, m_XBinPath);
                        persister.SaveTemplateCompiled(templateName, sbCompile4.ToString(), sWarning4);
                    }
                    try
                    {
                        if (sbCompile4.ToString() != "error")
                        {
                            oTree4 = Newtonsoft.Json.JsonConvert.DeserializeObject<JCmd4>(sbCompile4.ToString());
                            if (sContent.IndexOf("#startcomment#nocache#endcomment#", StringComparison.CurrentCultureIgnoreCase) == -1)
                                if (oTree4 != null) m_TemplateTree4[templateName] = oTree4;
                        }
                    }
                    catch(Exception )
                    {

                    }
                  }
                else if ((Date_XBin > Date_XDoc) && (Date_XBin > Date_XLib))
                {
                    System.Diagnostics.Debug.WriteLine("" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - Found template " + templateName + " in xbin: " + m_XBinPath + templateName + ".xbin4");
                    //store if no cache and return
                    m_TemplateText.Remove(templateName);
                    try
                    {
                        sContent = File.ReadAllText(m_XBinPath + templateName + ".xbin4");
                        sContent = new SimplerAES().Decrypt(sContent);
//                        oTree4 = Newtonsoft.Json.JsonConvert.DeserializeObject<JCmd4>(sContent);
                        // thread
                        //Thread thread = new Thread(new ThreadStart(() => graph = JsonConvert.DeserializeObject<Graph<Intersection>>(json, GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings)), 1000000);
                        //thread.Start();
                        //thread.Join();
                        // thread
                        Thread thread = new Thread(new ThreadStart(() => oTree4 = JsonConvert.DeserializeObject<JCmd4>(sContent)), 1000000);
                        thread.Start();
                        thread.Join();
                        if (oTree4 == null)
                        {
                            CPersister persister = new CPersister(m_XDocPath, m_XBinPath);
                            persister.SaveTemplateCompiled(templateName, null, "");
                        }
                    }
                    catch (Exception e1)
                    {
                        string sWarning4 = e1.Message;
                        CPersister persister = new CPersister(m_XDocPath, m_XBinPath);
                        persister.SaveTemplateCompiled(templateName, null, sWarning4);
                        return LoadTemplateTree4(templateName, iTry + 1);
                    }
                    //if (!(sContent.Contains("\"text\":\"#startcomment#nocache#endcomment#\"")))
                    //    if (oTree != null) m_TemplateTree[templateName] = oTree;
                    if (sContent.IndexOf("#startcomment#nocache#endcomment#", StringComparison.CurrentCultureIgnoreCase) == -1)
                        if (oTree4 != null) m_TemplateTree4[templateName] = oTree4;
                }
                else if ((Date_XLib > Date_XDoc) && (Date_XLib > Date_XBin))
                {
                    System.Diagnostics.Debug.WriteLine("" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - Found template " + templateName + " in xlib: " + m_XLibPath + libPath + ".xlib4");
                    //store lib and query template
                    sContent = File.ReadAllText(m_XLibPath + libPath + ".xlib4");
                    sContent = new SimplerAES().Decrypt(sContent);
                    oLib4 = Newtonsoft.Json.JsonConvert.DeserializeObject<JCmd4>(sContent);
                    foreach (JCmd4 command in oLib4.C)
                    {
                        if (command.EI != null) m_TemplateTree4[command.P1] = command.EI;
                    }
                    if (m_TemplateTree4.ContainsKey(templateName))
                        oTree4 = (JCmd4)m_TemplateTree4[templateName];
                }
                else
                {
                    //from DB
                    sContent = LoadTemplateContentDB(templateName);
                    m_TemplateText.Remove(templateName);
                    if (sContent != "error")
                    {
                        StringBuilder sbCompile4 = new StringBuilder();
                        CCompiler compiler = new CCompiler();
                        if (compiler != null)
                        {
                            string sWarning4 = compiler.CompileWarning(sContent, sbCompile4);
                            CPersister persister = new CPersister(m_XDocPath, m_XBinPath);
                            persister.SaveTemplateCompiled(templateName, sbCompile4.ToString(), sWarning4);
                        }
                        oTree4 = Newtonsoft.Json.JsonConvert.DeserializeObject<JCmd4>(sbCompile4.ToString());
                        if (sContent.IndexOf("#startcomment#nocache#endcomment#", StringComparison.CurrentCultureIgnoreCase) == -1)
                            if (oTree4 != null) m_TemplateTree4[templateName] = oTree4;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "error:" + ex.ToString());
            }
            if (oTree4 == null)
                System.Diagnostics.Debug.WriteLine("" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - Template not found!!!");

            return oTree4;
        }
        public JCmd RequestTemplateTree(string templateName, ref string sWarning)
        {
            templateName = templateName.Replace("/", "\\");
            if (m_TemplateTree.ContainsKey(templateName))
                return (JCmd)m_TemplateTree[templateName];

            return LoadTemplateTree(templateName);

            //{
            //    sTemplateCompiled = LoadTemplateCompiled(templateName, ref sWarning);
            //    if (sTemplateCompiled == "error")
            //    {
            //        m_TemplateText.Remove(templateName);
            //        StringBuilder sbCompile = new StringBuilder();
            //        CompileTemplate(templateName, sbCompile);
            //        sTemplateCompiled = sbCompile.ToString();
            //    }
            //    //o = Newtonsoft.Json.JsonConvert.DeserializeObject(sTemplateCompiled);
            //    o = Newtonsoft.Json.JsonConvert.DeserializeObject<JCmd>(sTemplateCompiled);

            //    if (!(sTemplateCompiled.Contains("\"text\":\"#startcomment#nocache#endcomment#\"")))
            //        if (o != null) m_TemplateTree[templateName] = o;
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("Template " + templateName + " invalid!");
            //}
            //return o;
        }
        public JCmd4 RequestTemplateTree4(string templateName, ref string sWarning)
        {
            templateName = templateName.Replace("/", "\\");
            if (m_TemplateTree4.ContainsKey(templateName))
                return (JCmd4)m_TemplateTree4[templateName];

            return LoadTemplateTree4(templateName);
        }
        //public JCmd RequestTemplateTree_orig(string templateName, ref string sWarning)
        //{
        //    templateName = templateName.Replace("/", "\\");
        //    string sTemplateCompiled = "notloaded";
        //    if (m_TemplateTree.ContainsKey(templateName))
        //        return (JCmd)m_TemplateTree[templateName];
        //    //            dynamic o = null;
        //    JCmd o = null;
        //    try
        //    {
        //        sTemplateCompiled = LoadTemplateCompiled(templateName, ref sWarning);
        //        if (sTemplateCompiled == "error")
        //        {
        //            m_TemplateText.Remove(templateName);
        //            StringBuilder sbCompile = new StringBuilder();
        //            CompileTemplate(templateName, sbCompile,true);
        //            sTemplateCompiled = sbCompile.ToString();
        //        }
        //        //o = Newtonsoft.Json.JsonConvert.DeserializeObject(sTemplateCompiled);
        //        o = Newtonsoft.Json.JsonConvert.DeserializeObject<JCmd>(sTemplateCompiled);

        //        //if (!(sTemplateCompiled.Contains("\"text\":\"#startcomment#nocache#endcomment#\"")))
        //        //    if (o != null) m_TemplateTree[templateName] = o;
        //        if (sTemplateCompiled.IndexOf("#startcomment#nocache#endcomment#", StringComparison.CurrentCultureIgnoreCase) == -1)
        //            if (o != null) m_TemplateTree[templateName] = o;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Template " + templateName + " invalid!");
        //    }
        //    return o;
        //}
        private bool ExtractTemplate(ref string sContent, ref string sTemplateName, ref string sTemplateContent )
        {
            int i1 = sContent.IndexOf("#STARTTEMPLATE.");
            if (i1 < 0) return false;
            int i2 = sContent.IndexOf("#", i1 + 1);
            if (i2 < 0) return false;
            sTemplateName = sContent.Substring(i1 + "#STARTTEMPLATE.".Length, i2 - i1 - "#STARTTEMPLATE.".Length);
            int i3 = sContent.IndexOf("#ENDTEMPLATE." + sTemplateName + "#", i2 + 1);
            if (i3 < 0) return false;
            sTemplateContent = sContent.Substring(i2 + 1, i3 - i2 - 1);
            sContent =  sContent.Substring(i3 + 1);
            return true;
        }
        public string SetTemplates(string sContent)
        {
            string sResult = "";
            string  sTemplateName="";
            string sTemplateContent="";
            while( ExtractTemplate(ref sContent, ref sTemplateName, ref sTemplateContent))
            {
                System.Diagnostics.Debug.WriteLine(sTemplateName);
            }

            return sResult;
        }
        public string GetTemplateNames(string relpath, bool recursive)
        {
            string path = Path.Combine(m_XDocPath, relpath);
            string s = "";
            try
            {
                if (File.Exists(path + ".xdoc"))
                {
                    s += "," + relpath;
                }
                else
                {
                    string[] files = Directory.GetFiles(path);
                    foreach (string f in files)
                    {
                        s += "," + f.Substring(m_XDocPath.Length, f.Length - m_XDocPath.Length - 5);
                        //                        s += "," + f;
                    }
                    if (recursive)
                    {
                        var subfolders = Directory.GetDirectories(path);
                        foreach (var sf in subfolders)
                        {
                            s += GetTemplateNames(sf, recursive);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            };
            s += "";
            //if (s == "") s = ",";
            return s;

        }
        public string SearchTemplateNames(string relpath,string patt, string recursive)
        {
            string path = Path.Combine(m_XDocPath, relpath);
            string s = "";
            try
            {
                string[] files = null;
                if (recursive=="1")
                    files = Directory.GetFiles(path,patt,SearchOption.AllDirectories);
                else
                    files = Directory.GetFiles(path,patt,SearchOption.TopDirectoryOnly);
                foreach (string f in files)
                    {
                        s += "," + f.Substring(m_XDocPath.Length, f.Length - m_XDocPath.Length - 5);
                        //                        s += "," + f;
                    }
            }
            catch (Exception e)
            {
                throw e;
            };
            //if (s=="") s = ",";
            return s;

        }

        public string RequestTemplateText(string templateName)
        {
            string sTemplateText = "error";
            if (m_TemplateText.ContainsKey(templateName))
            {
                sTemplateText = (string)m_TemplateText[templateName];
                //if (!(sTemplateText.Contains("#startcomment#nocache#endcomment#")))
                //    return sTemplateText;
                if (sTemplateText.IndexOf("#startcomment#nocache#endcomment#", StringComparison.CurrentCultureIgnoreCase) == -1)
                    return sTemplateText;
            }
            sTemplateText = LoadTemplateContent(templateName);
            if(sTemplateText != "error") m_TemplateText[templateName] = sTemplateText;
            return sTemplateText;
        }
        private bool RemoveCacheTemplate(string templateName)
        {
            bool bResult = m_TemplateText.ContainsKey(templateName);
            if (m_TemplateText.ContainsKey(templateName))
                m_TemplateText.Remove(templateName);
            if (m_TemplateTree.ContainsKey(templateName))
                m_TemplateTree.Remove(templateName);
            if (m_TemplateTree4.ContainsKey(templateName))
                m_TemplateTree4.Remove(templateName);
            return bResult;
        }
        public bool RemoveTemplate(string templateName)
        {
            bool bResult = RemoveCompiledTemplates(templateName);
            if (m_TemplateText.ContainsKey(templateName))
                m_TemplateText.Remove(templateName);
            if (m_TemplateTree.ContainsKey(templateName))
                m_TemplateTree.Remove(templateName);
            if (m_TemplateTree4.ContainsKey(templateName))
                m_TemplateTree4.Remove(templateName);
            return bResult;
        }
        public string RemoveTemplates(string templateName, string response)
        {
            if (templateName == "ImportSet")
            {
                string[] sRows;
                sRows = Utils.MySplit(response, '\n');
                int iRows = sRows.GetLength(0);
                for (int i = 0; i < iRows; i++)
                {
                    string sTemplateName = "";
                    if (sRows[i].Contains("Name:"))
                        sTemplateName = sRows[i].Substring(sRows[i].IndexOf("Name:") + 5);
                    RemoveTemplate(sTemplateName);
                }
            }
            else
                RemoveTemplate(templateName);
            return "";
        }
        public string ImportTemplates(string templateName, string sXML)
        {
            string sResult = "{\"templates\":[";
            bool isFirst = true;
            string sContent = "";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sXML);
            string sTemplates = JsonConvert.SerializeXmlNode(doc);
            //JObject o = JObject.Parse(sResult);

            if (templateName == "ImportSet")
            {
                //JArray ja = (JArray)(JObject.Parse(sTemplates)).SelectToken("TemplateSet.Template");
                JArray ja = SelectJSONArray(sTemplates, "TemplateSet.Template");
                foreach(JToken j in ja)
                {
                    templateName = j.SelectToken("Name").ToString();
                    sContent = j.SelectToken("Content.#cdata-section").ToString();
                    if (isFirst)  
                        isFirst = false; 
                    else
                        sResult += ",";
                    sResult += "\"" + StoreTemplateContent(templateName, sContent) + "\"";
                }
            }
            else
            {
                if(templateName == "fromXML")
                    templateName = JObject.Parse(sTemplates).SelectToken("Template.Name").ToString();
                sContent = JObject.Parse(sTemplates).SelectToken("Template.Content.#cdata-section").ToString();
                sResult += "\"" + StoreTemplateContent(templateName, sContent) + "\"";
            }
            sResult += "]}";
            return sResult;
        }
        public string ExportTemplates(string templateName)
        {
            //  v5.0 Used? export template text 
            string sResult = "<TemplatesXML>";
            return sResult;
        }
        private JArray SelectJSONArray(string l_Source, string l_JPath)
        {
            JToken jt;
            if (l_JPath == "")
                jt = JArray.Parse(l_Source);
            else
                jt = (JObject.Parse(l_Source)).SelectToken(l_JPath);
            if (jt is JArray)
                return (JArray)jt;
            else
                return new JArray(jt);
        }

        private string DelHash(Hashtable hash,string skey)
        {
            if (skey == "") { hash.Clear(); return hash.Count.ToString(); }
            else if (hash.ContainsKey(skey)) { hash.Remove(skey); return skey; }
            else return "";
        }
        private string GetHashKeys(Hashtable hash)
        {
            string sResult = "[";
            string sSep = "";
            foreach (string key in hash.Keys)
            {
                sResult += sSep + "\"" + key.Replace("\\","/") + "\"";
                sSep = ",";
            }
            sResult += "]";
            JToken ob = JToken.Parse(sResult);
            sResult = ob.ToString(Newtonsoft.Json.Formatting.Indented);
            return sResult;
        }

        public string AppCacheCommand(string sWhat)
        {
            string key = "";
            if (sWhat.IndexOf("/")>0)
            {
                key = sWhat.Substring(sWhat.IndexOf("/") + 1);
                sWhat = sWhat.Substring(0, sWhat.IndexOf("/"));
                key = key.Replace("/", "\\");

            }
            sWhat = sWhat.ToUpper();
            if (sWhat == "CFGGET") return GetHashKeys(m_ConfigVariables);
            if (sWhat == "CFGDEL") return DelHash(m_ConfigVariables, key);
            if (sWhat == "GET") return GetHashKeys(m_TemplateTree.Count == 0? m_TemplateTree4: m_TemplateTree);
            if (sWhat == "DEL") return DelHash(m_TemplateTree.Count == 0 ? m_TemplateTree4 : m_TemplateTree, key); 
            if (sWhat == "CLEAR") ClearTemplatesCache();
            return "";
        }

        public void ClearTemplatesCache()
        {
            m_ConfigVariables.Clear();
            m_TemplateText.Clear();
            m_TemplateTree.Clear();
            m_TemplateTree4.Clear();
            m_TemplateID2Name.Clear();
        }

        public string CompileTemplates()
        {
            return CompileDirtyTemplatesDB();
        }
        public string CompileTemplate(string strTemplateName)
        {
            StringBuilder sbCompile = new StringBuilder();
            return CompileTemplate(strTemplateName, sbCompile,true);
        }
        private string CompileTemplate(string strTemplateName, StringBuilder sbCompile,bool bStore)
        {
            string sResponse = "=== Template " + strTemplateName + " was compiled with ";
            try
            {
                m_TemplateText.Remove(strTemplateName);
                string strTemplateContent = RequestTemplateText(strTemplateName);
                if (strTemplateContent != "error")
                {
                    if (compiler != null)
                    {
                        // new compiler
                        //                    CCompiler compiler = new CCompiler();
                        //                    CPersister persister = new CPersister(m_XDocPath, m_XBinPath);
                        StringBuilder sbCompile4 = new StringBuilder();
                        string sWarning4 = compiler.CompileWarning(strTemplateContent, sbCompile4);
                        StringBuilder sbUnCompile4 = new StringBuilder();
                        string s4 = compiler.UnCompile4(sbCompile4.ToString(), sbUnCompile4);
                        persister.SaveTemplateUnCompiled(strTemplateName, sbUnCompile4.ToString());
                        persister.SaveTemplateCompiled(strTemplateName, sbCompile4.ToString(), sWarning4);
                    }


                    CParser parser = new CParser(null, -1, null, "", null);
                    string sWarning = parser.CompileWarning(strTemplateContent, sbCompile);
                    if (strTemplateContent.IndexOf("#startcomment#memory#endcomment#", StringComparison.CurrentCultureIgnoreCase) != -1) bStore = false;
                    if(bStore)
                        StoreCompiledTemplate(strTemplateName, sbCompile.ToString(), sWarning);

                    ////m_TemplateTree.Remove(strTemplateName);
                    if (sWarning != "")
                    {
                        sResponse += "error:\r\n" + sWarning + "\r\n";
                    }
                    else
                        sResponse += "success." + "\r\n";
                }
                else
                {
                    //sbCompile.Clear();
                    //                    sbCompile.Append ("{\"error\"}");
                    sbCompile.Append ("{}");
                    sResponse += "error." + "\r\n";
                }
            }
            catch (Exception ex)
            {
                sbCompile.Clear();
                sbCompile.Append("error");
                sResponse += "error." + "\r\n";
                sResponse += "\tERROR:\r\n" + ex.Message;
            }
            return sResponse;
        }
        private string CompileContent(string strTemplateName, string strTemplateContent, StringBuilder sbCompile)
        {
            string sResponse = "=== Template " + strTemplateName + " was compiled with ";
            try
            {
                m_TemplateText.Remove(strTemplateName);
                if (strTemplateContent != "error")
                {
                    if (compiler != null)
                    {
                        // new compiler
                        //                    CCompiler compiler = new CCompiler();
                        //                    CPersister persister = new CPersister(m_XDocPath, m_XBinPath);
                        StringBuilder sbCompile4 = new StringBuilder();
                        string sWarning4 = compiler.CompileWarning(strTemplateContent, sbCompile4);
                        StringBuilder sbUnCompile4 = new StringBuilder();
                        string s4 = compiler.UnCompile4(sbCompile4.ToString(), sbUnCompile4);
                        persister.SaveTemplateUnCompiled(strTemplateName, sbUnCompile4.ToString());
                        persister.SaveTemplateCompiled(strTemplateName, sbCompile4.ToString(), sWarning4);
                    }

                    CParser parser = new CParser(null, -1, null, "", null);
                    string sWarning = parser.CompileWarning(strTemplateContent, sbCompile);
                    if (strTemplateContent.IndexOf("#startcomment#memory#endcomment#", StringComparison.CurrentCultureIgnoreCase) == -1) 
                        StoreCompiledTemplate(strTemplateName, sbCompile.ToString(), sWarning);

                    m_TemplateTree.Remove(strTemplateName);
                    if (sWarning != "")
                    {
                        sResponse += "error:\r\n" + sWarning + "\r\n";
                    }
                    else
                        sResponse += "success." + "\r\n";
                }
                else
                {
                    sbCompile.Clear();
                    sbCompile.Append("error");
                    sResponse += "error." + "\r\n";
                }
            }
            catch (Exception ex)
            {
                sbCompile.Clear();
                sbCompile.Append("error");
                sResponse += "error." + "\r\n";
                sResponse += "\tERROR:\r\n" + ex.Message;
            }
            return sResponse;
        }
        public string UncompileTemplates()
        {
            string sResponse = "";
                foreach (DictionaryEntry entry in m_TemplateText)
                {
                    //m_ContextSessionVariables[entry.Key] = CollectionsUtil.CreateCaseInsensitiveHashtable();
                    sResponse+=UncompileTemplateContent((string)entry.Key, (string)entry.Value);
                }
                return sResponse;
        }

        //public string UncompileTemplate(string strTemplateName)
        //{
        //    string strTemplateContent = RequestTemplateText(strTemplateName);
        //    return UncompileTemplate(strTemplateName, strTemplateContent);
        //}
        public string UncompileTemplate(string strTemplateName)
        {
            string s = "";
            JCmd4 oTree4 = RequestTemplateTree4(strTemplateName, ref s);
            if (oTree4 != null)
                return UncompileTreeTemplate4(strTemplateName, oTree4);
            else
            {
                //JCmd oTree = RequestTemplateTree(strTemplateName, ref s);
                //if (oTree != null)
                //    return UncompileTreeTemplate(strTemplateName, oTree);
                //else
                //{

                    string strTemplateContent = RequestTemplateText(strTemplateName);
                    if (strTemplateContent != "error")
                        return UncompileTemplateContent(strTemplateName, strTemplateContent);
                //}
            }
            return "";
        }
        private string UncompileTreeTemplate(string strTemplateName, JCmd oTree)
        {
            string sResponse = "=== Template " + strTemplateName + " was uncompiled with ";
            try
            {
                StringBuilder sbUnCompile4 = new StringBuilder();
                string s4 = oTree.Sintax4(sbUnCompile4, -1, "false");
                CPersister persister = new CPersister(m_XDocPath, m_XBinPath);
                persister.SaveTemplateUnCompiled(strTemplateName, sbUnCompile4.ToString());
                if (s4!="false" )
                {
                    sResponse += "error:\r\n" + s4 + "\r\n";
                }
                else
                    sResponse += "success." + "\r\n";

            }
            catch (Exception ex)
            {
                sResponse += "error." + "\r\n";
                sResponse += "\tERROR:\r\n" + ex.Message;
            }
            return sResponse;
        }

        private string UncompileTreeTemplate4(string strTemplateName, JCmd4 oTree)
        {
            string sResponse = "=== Template " + strTemplateName + " was uncompiled with ";
            try
            {
                StringBuilder sbUnCompile4 = new StringBuilder();
                bool s4 = oTree.Sintax4(sbUnCompile4, -1, false);
                CPersister persister = new CPersister(m_XDocPath, m_XBinPath);
                persister.SaveTemplateUnCompiled(strTemplateName, sbUnCompile4.ToString());
                if (s4 != false)
                {
                    sResponse += "error:\r\n" + s4 + "\r\n";
                }
                else
                    sResponse += "success." + "\r\n";

            }
            catch (Exception ex)
            {
                sResponse += "error." + "\r\n";
                sResponse += "\tERROR:\r\n" + ex.Message;
            }
            return sResponse;
        }
        private string UncompileTemplateContent(string strTemplateName, string strTemplateContent)
        {
            string sResponse = "=== Template " + strTemplateName + " was uncompiled with ";
            try
            {
                if (strTemplateContent != "error")
                {
                    CCompiler compiler = new CCompiler();
                    CPersister persister = new CPersister(m_XDocPath, m_XBinPath);
                    if (compiler != null)
                    {
                        StringBuilder sbCompile4 = new StringBuilder();
                        string sWarning4 = compiler.CompileWarning(strTemplateContent, sbCompile4);
                        persister.SaveTemplateCompiled(strTemplateName, sbCompile4.ToString(), sWarning4);

                        StringBuilder sbUnCompile4 = new StringBuilder();
                        string s4 = compiler.UnCompile4(sbCompile4.ToString(), sbUnCompile4);
                        persister.SaveTemplateUnCompiled(strTemplateName, sbUnCompile4.ToString());
                        if (s4 != "")
                        {
                            sResponse += "error:\r\n" + s4 + "\r\n";
                        }
                        else
                            sResponse += "success." + "\r\n";

                    }
                }
                else
                {
                    sResponse += "error." + "\r\n";
                }
            }
            catch (Exception ex)
            {
                sResponse += "error." + "\r\n";
                sResponse += "\tERROR:\r\n" + ex.Message;
            }
            return sResponse;
        }
        #endregion


    }


}
