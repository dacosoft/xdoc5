﻿////using KubionLogNamespace;
using KubionDataNamespace;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Collections.Concurrent;
using System.Web.WebSockets;
using System.Threading;
using System.IO;
using System.Net.Http;


namespace XHTMLMerge
{

/*
    public class WSClient
    {
        private uint BufferSize = 1024 * 256;
        //private AspNetWebSocketContext ws_context;
        internal System.Net.WebSockets.WebSocket ws=null;
        private string sTicket = "";
        public Clients factory;
        public WSClient(string ticket,Clients clients)
        {
            factory = clients;
            sTicket = ticket;
        }

        internal async Task Connect(AspNetWebSocketContext context)
        {
            //string sResult = "";
            //string sTicket = getRequestTicket(HttpContext.Current.Request);

            var buffer = new byte[BufferSize];
            ws = context.WebSocket;
            while (ws.State == System.Net.WebSockets.WebSocketState.Open)
            {
                var cts = new CancellationTokenSource();
                var segment = new ArraySegment<byte>(buffer);
                try
                {
                    var result = await context.WebSocket.ReceiveAsync(segment, cts.Token);
                    switch (result.MessageType)
                    {
                        case System.Net.WebSockets.WebSocketMessageType.Text:
                            var message = System.Text.Encoding.UTF8.GetString(buffer, 0, result.Count);
                            factory.Messaged(sTicket, message);
                            break;
                        case System.Net.WebSockets.WebSocketMessageType.Close:
                            factory.Disconnected(sTicket, false);
                            break;
                        default:
                            throw new NotSupportedException();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    factory.Disconnected(sTicket, true);
                }
            }
            //OnSocketClosed(guid);
        }

        internal async Task SendMessage(string message)
        {
            //var message = JsonConvert.SerializeObject(data);
            var buffer = System.Text.Encoding.UTF8.GetBytes(message);
            var cts = new CancellationTokenSource();
            if (ws.State == System.Net.WebSockets.WebSocketState.Open)
            {
                await ws.SendAsync(new ArraySegment<byte>(buffer), System.Net.WebSockets.WebSocketMessageType.Text, true, cts.Token);
            }
        }

        internal async void SendDisconnect()
        {
            if (ws.State == System.Net.WebSockets.WebSocketState.Open)
            {
                var cts = new CancellationTokenSource();
                // .CloseAsync( System.Net.WebSockets.WebSocketCloseStatus.Empty,"",cts.Token);
                await ws.CloseOutputAsync(System.Net.WebSockets.WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
            }
        }
    }
    public class SSEClient
    {
        public HttpContext sse_context;
        private string sTicket = "";
        public Clients factory;
        public SSEClient(string ticket, Clients clients)
        {
            factory = clients;
            sTicket = ticket;
        }

        internal async Task Connect(HttpContext context)
        {
            sse_context = context;
            context.Response.ContentType  ="text/event-stream";
            context.Response.CacheControl = "no-cache";
            context.Response.AddHeader("Connection", "keep-alive");
            context.Response.AddHeader("Access-Control-Allow-Origin", "*");
            context.Response.Expires = -1;
//            SendMessage(sTicket + " i am in!");
            while(true)
            {
                System.Threading.Thread.Sleep(5000);
                //               SendMessage(sTicket + " i am in!");
                SendMessage1("alive","alive");
            };
       }
        internal async Task SendMessage1(string sevent,string message)
        {

            //if (!sse_context.Response.IsClientConnected) return;
            sse_context.Response.Write("event: " + sevent + "\n");
            sse_context.Response.Write("data: " + message + "\n\n");
            sse_context.Response.Flush();

        }

        internal async Task SendMessage(string message)
        {

            //if (!sse_context.Response.IsClientConnected) return;
            sse_context.Response.Write("data: " + message + "\n\n");
            sse_context.Response.Flush();

        }

        internal async Task SendMessageA(string message)
        {
            if (!sse_context.Response.IsClientConnected) return;
            //sseResponse.Write("data: " + message);
            //await sseResponse.Output.WriteLineAsync("id: " + sTicket);
            await sse_context.Response.Output.WriteLineAsync("data: " + message + "\n\n");
//            await sseResponse.Output.WriteLineAsync();
            await sse_context.Response.Output.FlushAsync();
            //            sseResponse.Flush();
           
            //if (!sseResponse.IsClientConnected) return;
            //sseResponse.Write("data: " + message + "\n\n");
            //sseResponse.Flush();

        }

        internal async void SendDisconnect()
        {
            if (!sse_context.Response.IsClientConnected) return;
            sse_context.Response.Write("event: close\n");
            sse_context.Response.Write("data: dataclose 123\n\n");
            sse_context.Response.Flush();
            sse_context.Response.End();
        }

        //public HttpResponseMessage GetEvents(CancellationToken clientDisconnectToken)
        //{
        //    var response = sseResponse;
        //    response.Content = new System.Net.Http.PushStreamContent(async (stream, httpContent, transportContext) =>
        //    {
        //        using (var writer = new StreamWriter(stream))
        //        {
        //            using (var consumer = new BlockingCollection<string>())
        //            {
        //                var eventGeneratorTask = EventGeneratorAsync(consumer, clientDisconnectToken);
        //                foreach (var @event in consumer.GetConsumingEnumerable(clientDisconnectToken))
        //                {
        //                    await writer.WriteLineAsync("data: " + @event);
        //                    await writer.WriteLineAsync();
        //                    await writer.FlushAsync();
        //                }
        //                await eventGeneratorTask;
        //            }
        //        }
        //    }, "text/event-stream");
        //    return response;
        //}

        //private async Task EventGeneratorAsync(BlockingCollection<string> producer, CancellationToken cancellationToken)
        //{
        //    try
        //    {
        //        while (!cancellationToken.IsCancellationRequested)
        //        {
        //            producer.Add(DateTime.Now.ToString(), cancellationToken);
        //            await Task.Delay(1000, cancellationToken).ConfigureAwait(false);
        //        }
        //    }
        //    finally
        //    {
        //        producer.CompleteAdding();
        //    }
        //}
    }
    public class Clients
    {
        //private uint BufferSize = 1024 * 256;
        private ConcurrentDictionary<string, string> tickets = null;
        private ConcurrentDictionary<string, Object> clients = null;
        string sVersion = "";
        string sWSTemplate = "WS";

        public Clients() : this(null) { }
        public Clients(AppCache m_AppCache)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["WS_Template"] != null) sWSTemplate = (string)System.Configuration.ConfigurationManager.AppSettings["WS_Template"];
            //HttpContext m_context = HttpContext.Current;
            //if ((m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            //if (m_AppCache == null) m_AppCache = new AppCache();
            //CRuntime xrun = new CRuntime(m_AppCache, null);
            //if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            //xrun = new CRuntime(m_AppCache, null);
            //xrun.myHttpContext = null;
            //xrun.sVersion = sVersion;
            tickets = new ConcurrentDictionary<string, string>();
            clients = new ConcurrentDictionary<string, Object>();
            addTicket("da", "admin");


        }

        public async Task ConnectSSE(HttpContext context)
        {
            string sResult = "";
            string sTicket = getRequestTicket(HttpContext.Current.Request);
            if (sTicket == "no") return;
            if (sTicket != "da")
                if (!tickets.ContainsKey(sTicket)) return;
            string sUser = tickets[sTicket];
            SSEClient client = new SSEClient(sTicket, this);
            client.sse_context = context;
            clients.TryAdd(sTicket, client);

            sResult = RunWSTemplate("OnConnect", sTicket, sUser, "");
            await client.Connect(context);

        }
        public async Task Connect(AspNetWebSocketContext context)
        {
            string sResult = "";
            string sTicket = getRequestTicket(HttpContext.Current.Request);
            if (sTicket == "no") return;
            if (sTicket != "da")
                if (!tickets.ContainsKey(sTicket)) return;
            string sUser = tickets[sTicket];
            WSClient client = new WSClient(sTicket, this);
            client.ws = context.WebSocket;
            clients.TryAdd(sTicket, client);
            sResult = RunWSTemplate("OnConnect", sTicket, sUser, "");
            await client.Connect(context);
        }
        //private async Task SendMessage<T>(string connectionId, T data)
        public string addTicket(string sTicket, string sUser)
        {
            tickets.TryAdd(sTicket, sUser);
            return "";
        }
        public string removeTicket(string sTicket)
        {
            return SendDisconnect(sTicket);
        }
        public string removeUser(string sUser)
        {
            string sTickets = "";
            foreach (var de in tickets)
                if (de.Value.ToString() == sUser)
                    sTickets = sTickets + de.Key + ";";
            string[] aTickets = sTickets.Split(';');
            return SendDisconnect(aTickets);
        }
        public void sendTicket(string sTicket, string sMessage)
        {
            string[] aTickets = { sTicket };
            SendMessages(aTickets, sMessage);
            //BroadCast(sMessage);
        }
        public string sendUser(string sUser, string sMessage)
        {
            string sTickets = "";
            foreach (var de in tickets)
                if ((sUser == "*") || (de.Value.ToString() == sUser))
                    sTickets = sTickets + de.Key + ";";
            string[] aTickets = sTickets.Split(';');
            SendMessages(aTickets, sMessage);
            //BroadCast(sMessage);
            return aTickets.Length.ToString();
        }
        public string getUsers(string sOpen)
        {
            string sUsers = ",";
            foreach (var de in tickets)
                if ((sOpen != "1") || clients.ContainsKey(de.Key))
                    if (!sUsers.Contains("," + de.Value.ToString() + ","))
                        sUsers = sUsers + "\"" + de.Value.ToString() + "\",";
            if (sUsers == ",") sUsers = "[]";
            else
                sUsers = "[" + sUsers.Substring(1, sUsers.Length - 2) + "]";
            return sUsers;
        }
        public string getTickets(string sUser, string sOpen)
        {
            string sTickets = "";
            foreach (var de in tickets)
                if ((sUser == "") || (de.Value.ToString() == sUser))
                    if ((sOpen != "1") || clients.ContainsKey(de.Key))
                        sTickets = sTickets + "\"" + de.Key + "\",";
            if (sTickets == "") sTickets = "[]";
            else
                sTickets = "[" + sTickets.Substring(0, sTickets.Length - 1) + "]";
            return sTickets;
        }
        public string getTicket(string sTicket, string sOpen)
        {
            if (sOpen == "1")
                if (clients.ContainsKey(sTicket))
                    return tickets[sTicket].ToString();
            if (tickets.ContainsKey(sTicket))
                return tickets[sTicket].ToString();
            return "";
        }

        private async Task SendMessage(string sTicket, string message)
        {
            if (clients.ContainsKey(sTicket))
            {
                if (clients[sTicket] is WSClient)
                    ((WSClient)clients[sTicket]).SendMessage(message);
                if (clients[sTicket] is SSEClient)
                    ((SSEClient)clients[sTicket]).SendMessage(message);

                //Client client = clients[sTicket];
                //await client.SendMessage( message);
            }
            //Client client = clients[sTicket];
            //await client.SendMessage(message);
        }
        private async Task SendMessages(string[] aTickets, string message)
        {
            var buffer = System.Text.Encoding.UTF8.GetBytes(message);
            foreach (string sTicket in aTickets)
            {
                if (clients.ContainsKey(sTicket))
                {
                    if (clients[sTicket] is WSClient)
                        ((WSClient)clients[sTicket]).SendMessage(message);
                    if (clients[sTicket] is SSEClient)
                        ((SSEClient)clients[sTicket]).SendMessage(message);

                    //Client client = clients[sTicket];
                    //await client.SendMessage( message);
                }
            }
        }
        private async Task BroadCast(string message)
        {
            var buffer = new ArraySegment<byte>(System.Text.Encoding.UTF8.GetBytes(message));
            var tasks = new List<Task>();
            var cts = new CancellationTokenSource();
            foreach (WSClient client in clients.Values)
            {
                tasks.Add(client.SendMessage(message));
//                    tasks.Add(ws.WebSocket.SendAsync(buffer, System.Net.WebSockets.WebSocketMessageType.Text, true, cts.Token));
            }
            await Task.WhenAll(tasks);
        }

        private string SendDisconnect(string[] aTickets)
        {
            string sResult = "";
            foreach (string sTicket in aTickets)
            {
                sResult += SendDisconnect(sTicket);
            }
            return sResult;
        }

        private string SendDisconnect(string sTicket)
        {
            string sResult = "";
            Object client;
            string sUser;
            clients.TryRemove(sTicket, out client);
            tickets.TryRemove(sTicket, out sUser);
            if (clients.ContainsKey(sTicket))
            {
                if (clients[sTicket] is WSClient)
                    ((WSClient)clients[sTicket]).SendDisconnect();
                if (clients[sTicket] is SSEClient)
                    ((SSEClient)clients[sTicket]).SendDisconnect();
            }
            bool bLast = (!tickets.Values.Contains(sUser));
            //if (!bSilent)
            //    sResult = RunWSTemplate("OnDisconnect", sTicket, sUser, bLast ? "1" : "0");
            if (sTicket == "da") addTicket("da", "admin");
            return sResult;
        }

        internal string Disconnected(string sTicket, bool bSilent)
        {
            string sResult = "";
            Object client;
            string sUser;
            clients.TryRemove(sTicket, out client);
            tickets.TryRemove(sTicket, out sUser);
            bool bLast = (!tickets.Values.Contains(sUser));
            if (!bSilent)
                sResult = RunWSTemplate("OnDisconnect", sTicket, sUser, bLast ? "1" : "0");
            if (sTicket == "da") addTicket("da", "admin");
            return sResult;
            //OnDisconnect(sTicket, _user, bLast);
            //var t = BroadCast("{}");
        }

        internal string Messaged(string sTicket, string sMessage)
        {
            string sUser;
            tickets.TryGetValue(sTicket, out sUser);
            string sResult = RunWSTemplate("OnMessage", sTicket, sUser, sMessage);
            //SendMessage(sTicket, "response:" + sResult);
            return sResult;
            //var t = BroadCast("{}");
        }
        //private  void OnConnect(string sTicket, string sUser)
        //{
        //    Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
        //    newHash.Add("Event", "OnConnect");
        //    newHash.Add("Ticket", sTicket);
        //    newHash.Add("User", sUser);
        //    RunXDocTemplate("WS", newHash);
        //}
        //private  void OnDisconnect(string sTicket, string sUser, bool bLast)
        //{
        //    Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
        //    newHash.Add("Event", "OnDisconnect");
        //    newHash.Add("Ticket", sTicket);
        //    newHash.Add("User", sUser);
        //    newHash.Add("Last", bLast?"1":"0");
        //    RunXDocTemplate("WS", newHash);
        //}
        //private  void OnMessage(string sTicket, string sUser, string sMessage)
        //{
        //    Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
        //    newHash.Add("Event", "OnMessage");
        //    newHash.Add("Ticket", sTicket);
        //    newHash.Add("User", sUser);
        //    newHash.Add("Message", sMessage);
        //    RunXDocTemplate("WS", newHash);

        //}

        private string RunWSTemplate(string sEvent, string sTicket, string sUser, string sMessage)
        {
            Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
            newHash.Add("Event", sEvent);
            newHash.Add("Ticket", sTicket);
            newHash.Add("User", sUser);
            newHash.Add("Message", sMessage);
            string sResult = RunXDocTemplate(sWSTemplate, newHash);
            return sResult;
        }

        private string RunXDocTemplate(string strTemplateName, Hashtable hParameters)
        {
            AppCache m_AppCache = null;
            StringBuilder sbResponse = new StringBuilder();
            string sExit = "";
            string sResponse = "";
            try
            {
                HttpContext m_context = HttpContext.Current;
                if ((m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
                if (m_AppCache == null) m_AppCache = new AppCache();
                CRuntime xrun = new CRuntime(m_AppCache, null);
                xrun = new CRuntime(m_AppCache, null);
                xrun.Clients = this;
                xrun.myHttpContext = null;
                xrun.sVersion = sVersion;
                string sResult = xrun.RunTemplate(strTemplateName, hParameters, sbResponse, ref sExit);
                sResponse = sbResponse.ToString();
                if (xrun.ExitValue != "") sExit = xrun.ExitValue;
                if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            }
            catch (Exception ex)
            {
                sExit = ex.Message;
            }
            if (sExit == "")
                return sResponse;
            else
                return sExit;
        }

        private string myReadContent(HttpRequest req)
        {
            string postedData = "";
            int iContentLength = req.ContentLength;
            //if (iContentLength < 5000)
            //{
            StreamReader reader = new StreamReader(req.InputStream, req.ContentEncoding);
            postedData = reader.ReadToEnd().Trim();
            return postedData;
            //}
            //else
            //    return "[[datasize exceeds limit]]";
        }
        private string getRequestTicket(HttpRequest req)
        {
            string sTicket = "";
            NameValueCollection col = req.Params;
            if (col["ticket"] != null)
                sTicket = col["ticket"].ToString();
            return sTicket;
        }
    }
*/

    public class WSClients
    {
        private uint BufferSize = 1024 * 256;
        private ConcurrentDictionary<string, string> tickets = null;
        private ConcurrentDictionary<string, AspNetWebSocketContext> clients = null;
        string sVersion = "";
        string sWSTemplate = "WS";

        public WSClients() : this(null) { }
        public WSClients(AppCache m_AppCache)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["WS_Template"] != null) sWSTemplate = (string)System.Configuration.ConfigurationManager.AppSettings["WS_Template"];
            //HttpContext m_context = HttpContext.Current;
            //if ((m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            //if (m_AppCache == null) m_AppCache = new AppCache();
            //CRuntime xrun = new CRuntime(m_AppCache, null);
            //if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            //xrun = new CRuntime(m_AppCache, null);
            //xrun.myHttpContext = null;
            //xrun.sVersion = sVersion;
            tickets = new ConcurrentDictionary<string, string>();
            clients = new ConcurrentDictionary<string, AspNetWebSocketContext>();


        }
        public async Task Connect(AspNetWebSocketContext context)
        {
            string sResult = "";
            string sTicket = getRequestTicket(HttpContext.Current.Request);
            if (sTicket == "no") return;
            if (sTicket != "da")
                if (!tickets.ContainsKey(sTicket)) return;
            string sUser = tickets[sTicket];
            clients.TryAdd(sTicket, context);
            sResult = RunWSTemplate("OnConnect", sTicket, sUser, "");
            //            OnConnect(sTicket, sUser);
            var buffer = new byte[BufferSize];
            while (context.WebSocket.State == System.Net.WebSockets.WebSocketState.Open)
            {
                var cts = new CancellationTokenSource();
                var segment = new ArraySegment<byte>(buffer);
                try
                {
                    var result = await context.WebSocket.ReceiveAsync(segment, cts.Token);
                    switch (result.MessageType)
                    {
                        case System.Net.WebSockets.WebSocketMessageType.Text:
                            var message = System.Text.Encoding.UTF8.GetString(buffer, 0, result.Count);
                            Message(sTicket, message);
                            break;
                        case System.Net.WebSockets.WebSocketMessageType.Close:
                            Disconnect(sTicket, false);
                            break;
                        default:
                            throw new NotSupportedException();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Disconnect(sTicket, true);
                }
            }
            //OnSocketClosed(guid);
        }
        //private async Task SendMessage<T>(string connectionId, T data)
        public string addTicket(string sTicket, string sUser)
        {
            if (tickets.TryAdd(sTicket, sUser))
                return "1";
            else
                return "0";
        }
        public string removeTicket(string sTicket)
        {
            return Disconnect(sTicket, true);
        }
        public string removeUser(string sUser)
        {
            string sTickets = "";
            foreach (var de in tickets)
                if (de.Value.ToString() == sUser)
                    sTickets = sTickets + de.Key + ";";
            string[] aTickets = sTickets.Split(';');
            return Disconnect(aTickets);
        }
        public string sendTicket(string sTicket, string sMessage)
        {
            string[] aTickets = { sTicket };
            SendMessages(aTickets, sMessage);
            return aTickets.Length.ToString();
        }
        public string sendUser(string sUser, string sMessage)
        {
            string sTickets = "";
            foreach (var de in tickets)
                if ((sUser == "*") || (de.Value.ToString() == sUser))
                    sTickets = sTickets + de.Key + ";";
            string[] aTickets = sTickets.Split(';');
            SendMessages(aTickets, sMessage);
            return aTickets.Length.ToString();
            //BroadCast(sMessage);
        }
        public string getUsers(string sOpen)
        {
            string sUsers = ",";
            foreach (var de in tickets)
                if ((sOpen != "1") || clients.ContainsKey(de.Key))
                    if (!sUsers.Contains("," + de.Value.ToString() + ","))
                        sUsers = sUsers + "\"" + de.Value.ToString() + "\",";
            if (sUsers == ",") sUsers = "[]";
            else
                sUsers = "[" + sUsers.Substring(1, sUsers.Length - 2) + "]";
            return sUsers;
        }
        public string getTickets(string sUser, string sOpen)
        {
            string sTickets = "";
            foreach (var de in tickets)
                if ((sUser == "") || (de.Value.ToString() == sUser))
                    if ((sOpen != "1") || clients.ContainsKey(de.Key))
                        sTickets = sTickets + "\"" + de.Key + "\",";
            if (sTickets == "") sTickets = "[]";
            else
                sTickets = "[" + sTickets.Substring(0, sTickets.Length - 1) + "]";
            return sTickets;
        }
        public string getTicket(string sTicket, string sOpen)
        {
            if (sOpen == "1")
                if (clients.ContainsKey(sTicket))
                    return tickets[sTicket].ToString();
            if (tickets.ContainsKey(sTicket))
                return tickets[sTicket].ToString();
            return "";
        }

        private async Task SendMessage(string sTicket, string message)
        {
            //var message = JsonConvert.SerializeObject(data);
            var ws = clients[sTicket].WebSocket;
            var buffer = System.Text.Encoding.UTF8.GetBytes(message);
            var cts = new CancellationTokenSource();
            if (ws.State == System.Net.WebSockets.WebSocketState.Open)
            {
                await ws.SendAsync(new ArraySegment<byte>(buffer), System.Net.WebSockets.WebSocketMessageType.Text, true, cts.Token);
            }
        }
        private async Task SendMessages(string[] aTickets, string message)
        {
            //var message = JsonConvert.SerializeObject(data);
            //            string[] aTickets = sTickets.Split(';');
            var buffer = System.Text.Encoding.UTF8.GetBytes(message);
            foreach (string sTicket in aTickets)
            {
                if (clients.ContainsKey(sTicket))
                {
                    var ws = clients[sTicket].WebSocket;
                    var cts = new CancellationTokenSource();
                    if (ws.State == System.Net.WebSockets.WebSocketState.Open)
                    {
                        await ws.SendAsync(new ArraySegment<byte>(buffer), System.Net.WebSockets.WebSocketMessageType.Text, true, cts.Token);
                    }
                }
            }
        }
        private async Task BroadCast(string message)
        {
            var buffer = new ArraySegment<byte>(System.Text.Encoding.UTF8.GetBytes(message));
            var tasks = new List<Task>();
            var cts = new CancellationTokenSource();
            foreach (var ws in clients.Values)
            {
                if (ws.WebSocket.State == System.Net.WebSockets.WebSocketState.Open)
                {
                    tasks.Add(ws.WebSocket.SendAsync(buffer, System.Net.WebSockets.WebSocketMessageType.Text, true, cts.Token));
                }
            }
            await Task.WhenAll(tasks);
        }

        private string Disconnect(string[] aTickets)
        {
            string sResult = "";
            foreach (string sTicket in aTickets)
            {
                sResult += Disconnect(sTicket, true);
            }
            return sResult;
        }

        private string Disconnect(string sTicket, bool bSilent)
        {
            string sResult = "";
            AspNetWebSocketContext _context;
            string sUser;
            clients.TryRemove(sTicket, out _context);
            tickets.TryRemove(sTicket, out sUser);
            var cts = new CancellationTokenSource();
            // .CloseAsync( System.Net.WebSockets.WebSocketCloseStatus.Empty,"",cts.Token);
            _context.WebSocket.CloseOutputAsync(System.Net.WebSockets.WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
            bool bLast = (!tickets.Values.Contains(sUser));
            if (!bSilent)
                sResult = RunWSTemplate("OnDisconnect", sTicket, sUser, bLast ? "1" : "0");
            if (sTicket == "da") addTicket("da", "admin");
            return sResult;
            //OnDisconnect(sTicket, _user, bLast);
            //var t = BroadCast("{}");
        }
        private string Message(string sTicket, string sMessage)
        {
            string sUser;
            tickets.TryGetValue(sTicket, out sUser);
            string sResult = RunWSTemplate("OnMessage", sTicket, sUser, sMessage);
            //SendMessage(sTicket, "response:" + sResult);
            return sResult;
            //var t = BroadCast("{}");
        }
        //private  void OnConnect(string sTicket, string sUser)
        //{
        //    Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
        //    newHash.Add("Event", "OnConnect");
        //    newHash.Add("Ticket", sTicket);
        //    newHash.Add("User", sUser);
        //    RunXDocTemplate("WS", newHash);
        //}
        //private  void OnDisconnect(string sTicket, string sUser, bool bLast)
        //{
        //    Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
        //    newHash.Add("Event", "OnDisconnect");
        //    newHash.Add("Ticket", sTicket);
        //    newHash.Add("User", sUser);
        //    newHash.Add("Last", bLast?"1":"0");
        //    RunXDocTemplate("WS", newHash);
        //}
        //private  void OnMessage(string sTicket, string sUser, string sMessage)
        //{
        //    Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
        //    newHash.Add("Event", "OnMessage");
        //    newHash.Add("Ticket", sTicket);
        //    newHash.Add("User", sUser);
        //    newHash.Add("Message", sMessage);
        //    RunXDocTemplate("WS", newHash);

        //}

        private string RunWSTemplate(string sEvent, string sTicket, string sUser, string sMessage)
        {
            Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
            newHash.Add("Event", sEvent);
            newHash.Add("Ticket", sTicket);
            newHash.Add("User", sUser);
            newHash.Add("Message", sMessage);
            string sResult = RunXDocTemplate(sWSTemplate, newHash);
            return sResult;
        }

        private string RunXDocTemplate(string strTemplateName, Hashtable hParameters)
        {
            AppCache m_AppCache = null;
            StringBuilder sbResponse = new StringBuilder();
            string sExit = "";
            string sResponse = "";
            try
            {
                HttpContext m_context = HttpContext.Current;
                if ((m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
                if (m_AppCache == null) m_AppCache = new AppCache();
                CRuntime xrun = new CRuntime(m_AppCache, null);
                xrun = new CRuntime(m_AppCache, null);
                xrun.Clients = this;
                /*
                                xrun.Clients = this;
                */
                xrun.myHttpContext = null;
                xrun.sVersion = sVersion;
                string sResult = xrun.RunTemplate(strTemplateName, hParameters, sbResponse, ref sExit);
                sResponse = sbResponse.ToString();
                if (xrun.ExitValue != "") sExit = xrun.ExitValue;
                if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            }
            catch (Exception ex)
            {
                sExit = ex.Message;
            }
            if (sExit == "")
                return sResponse;
            else
                return sExit;
        }

        private string myReadContent(HttpRequest req)
        {
            string postedData = "";
            int iContentLength = req.ContentLength;
            //if (iContentLength < 5000)
            //{
            StreamReader reader = new StreamReader(req.InputStream, req.ContentEncoding);
            postedData = reader.ReadToEnd().Trim();
            return postedData;
            //}
            //else
            //    return "[[datasize exceeds limit]]";
        }
        private string getRequestTicket(HttpRequest req)
        {
            string sTicket = "";
            NameValueCollection col = req.Params;
            if (col["ticket"] != null)
                sTicket = col["ticket"].ToString();
            return sTicket;
        }
    }

    public class SSEClients
    {
        private uint BufferSize = 1024 * 256;
        private ConcurrentDictionary<string, string> tickets = null;
        private ConcurrentDictionary<string, AspNetWebSocketContext> clients = null;
        string sVersion = "";
        string sSSETemplate = "SSE";

        public SSEClients() : this(null) { }
        public SSEClients(AppCache m_AppCache)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["SSE_Template"] != null) sSSETemplate = (string)System.Configuration.ConfigurationManager.AppSettings["SSE_Template"];
            //HttpContext m_context = HttpContext.Current;
            //if ((m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            //if (m_AppCache == null) m_AppCache = new AppCache();
            //CRuntime xrun = new CRuntime(m_AppCache, null);
            //if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            //xrun = new CRuntime(m_AppCache, null);
            //xrun.myHttpContext = null;
            //xrun.sVersion = sVersion;
            tickets = new ConcurrentDictionary<string, string>();
            clients = new ConcurrentDictionary<string, AspNetWebSocketContext>();
            addTicket("da", "admin");


        }
        public async Task Connect(AspNetWebSocketContext context)
        {
            string sResult = "";
            string sTicket = getRequestTicket(HttpContext.Current.Request);
            if (sTicket == "no") return;
            if (sTicket != "da")
                if (!tickets.ContainsKey(sTicket)) return;
            string sUser = tickets[sTicket];
            clients.TryAdd(sTicket, context);
            sResult = RunSSETemplate("OnConnect", sTicket, sUser, "");
            //            OnConnect(sTicket, sUser);
            var buffer = new byte[BufferSize];
            while (context.WebSocket.State == System.Net.WebSockets.WebSocketState.Open)
            {
                var cts = new CancellationTokenSource();
                var segment = new ArraySegment<byte>(buffer);
                try
                {
                    var result = await context.WebSocket.ReceiveAsync(segment, cts.Token);
                    switch (result.MessageType)
                    {
                        case System.Net.WebSockets.WebSocketMessageType.Text:
                            var message = System.Text.Encoding.UTF8.GetString(buffer, 0, result.Count);
                            Message(sTicket, message);
                            break;
                        case System.Net.WebSockets.WebSocketMessageType.Close:
                            Disconnect(sTicket, false);
                            break;
                        default:
                            throw new NotSupportedException();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Disconnect(sTicket, true);
                }
            }
            //OnSocketClosed(guid);
        }
        //private async Task SendMessage<T>(string connectionId, T data)
        public string addTicket(string sTicket, string sUser)
        {
            if (tickets.TryAdd(sTicket, sUser))
                return "1";
            else
                return "0";
        }
        public string removeTicket(string sTicket)
        {
            return Disconnect(sTicket, true);
        }
        public string removeUser(string sUser)
        {
            string sTickets = "";
            foreach (var de in tickets)
                if (de.Value.ToString() == sUser)
                    sTickets = sTickets + de.Key + ";";
            string[] aTickets = sTickets.Split(';');
            return Disconnect(aTickets);
        }
        public string sendTicket(string sTicket, string sMessage)
        {
            string[] aTickets = { sTicket };
            SendMessages(aTickets, sMessage);
            return aTickets.Length.ToString();
        }
        public string sendUser(string sUser, string sMessage)
        {
            string sTickets = "";
            foreach (var de in tickets)
                if ((sUser == "*") || (de.Value.ToString() == sUser))
                    sTickets = sTickets + de.Key + ";";
            string[] aTickets = sTickets.Split(';');
            SendMessages(aTickets, sMessage);
            return aTickets.Length.ToString();
            //BroadCast(sMessage);
        }
        public string getUsers(string sOpen)
        {
            string sUsers = ",";
            foreach (var de in tickets)
                if ((sOpen != "1") || clients.ContainsKey(de.Key))
                    if (!sUsers.Contains("," + de.Value.ToString() + ","))
                        sUsers = sUsers + "\"" + de.Value.ToString() + "\",";
            if (sUsers == ",") sUsers = "[]";
            else
                sUsers = "[" + sUsers.Substring(1, sUsers.Length - 2) + "]";
            return sUsers;
        }
        public string getTickets(string sUser, string sOpen)
        {
            string sTickets = "";
            foreach (var de in tickets)
                if ((sUser == "") || (de.Value.ToString() == sUser))
                    if ((sOpen != "1") || clients.ContainsKey(de.Key))
                        sTickets = sTickets + "\"" + de.Key + "\",";
            if (sTickets == "") sTickets = "[]";
            else
                sTickets = "[" + sTickets.Substring(0, sTickets.Length - 1) + "]";
            return sTickets;
        }
        public string getTicket(string sTicket, string sOpen)
        {
            if (sOpen == "1")
                if (clients.ContainsKey(sTicket))
                    return tickets[sTicket].ToString();
            if (tickets.ContainsKey(sTicket))
                return tickets[sTicket].ToString();
            return "";
        }

        private async Task SendMessage(string sTicket, string message)
        {
            //var message = JsonConvert.SerializeObject(data);
            var ws = clients[sTicket].WebSocket;
            var buffer = System.Text.Encoding.UTF8.GetBytes(message);
            var cts = new CancellationTokenSource();
            if (ws.State == System.Net.WebSockets.WebSocketState.Open)
            {
                await ws.SendAsync(new ArraySegment<byte>(buffer), System.Net.WebSockets.WebSocketMessageType.Text, true, cts.Token);
            }
        }
        private async Task SendMessages(string[] aTickets, string message)
        {
            //var message = JsonConvert.SerializeObject(data);
            //            string[] aTickets = sTickets.Split(';');
            var buffer = System.Text.Encoding.UTF8.GetBytes(message);
            foreach (string sTicket in aTickets)
            {
                if (clients.ContainsKey(sTicket))
                {
                    var ws = clients[sTicket].WebSocket;
                    var cts = new CancellationTokenSource();
                    if (ws.State == System.Net.WebSockets.WebSocketState.Open)
                    {
                        await ws.SendAsync(new ArraySegment<byte>(buffer), System.Net.WebSockets.WebSocketMessageType.Text, true, cts.Token);
                    }
                }
            }
        }
        private async Task BroadCast(string message)
        {
            var buffer = new ArraySegment<byte>(System.Text.Encoding.UTF8.GetBytes(message));
            var tasks = new List<Task>();
            var cts = new CancellationTokenSource();
            foreach (var ws in clients.Values)
            {
                if (ws.WebSocket.State == System.Net.WebSockets.WebSocketState.Open)
                {
                    tasks.Add(ws.WebSocket.SendAsync(buffer, System.Net.WebSockets.WebSocketMessageType.Text, true, cts.Token));
                }
            }
            await Task.WhenAll(tasks);
        }

        private string Disconnect(string[] aTickets)
        {
            string sResult = "";
            foreach (string sTicket in aTickets)
            {
                sResult += Disconnect(sTicket, true);
            }
            return sResult;
        }

        private string Disconnect(string sTicket, bool bSilent)
        {
            string sResult = "";
            AspNetWebSocketContext _context;
            string sUser;
            clients.TryRemove(sTicket, out _context);
            tickets.TryRemove(sTicket, out sUser);
            var cts = new CancellationTokenSource();
            // .CloseAsync( System.Net.WebSockets.WebSocketCloseStatus.Empty,"",cts.Token);
            _context.WebSocket.CloseOutputAsync(System.Net.WebSockets.WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
            bool bLast = (!tickets.Values.Contains(sUser));
            if (!bSilent)
                sResult = RunSSETemplate("OnDisconnect", sTicket, sUser, bLast ? "1" : "0");
            if (sTicket == "da") addTicket("da", "admin");
            return sResult;
            //OnDisconnect(sTicket, _user, bLast);
            //var t = BroadCast("{}");
        }
        private string Message(string sTicket, string sMessage)
        {
            string sUser;
            tickets.TryGetValue(sTicket, out sUser);
            string sResult = RunSSETemplate("OnMessage", sTicket, sUser, sMessage);
            //SendMessage(sTicket, "response:" + sResult);
            return sResult;
            //var t = BroadCast("{}");
        }

        private string RunSSETemplate(string sEvent, string sTicket, string sUser, string sMessage)
        {
            Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
            newHash.Add("Event", sEvent);
            newHash.Add("Ticket", sTicket);
            newHash.Add("User", sUser);
            newHash.Add("Message", sMessage);
            string sResult = RunXDocTemplate(sSSETemplate, newHash);
            return sResult;
        }

        private string RunXDocTemplate(string strTemplateName, Hashtable hParameters)
        {
            AppCache m_AppCache = null;
            StringBuilder sbResponse = new StringBuilder();
            string sExit = "";
            string sResponse = "";
            try
            {
                HttpContext m_context = HttpContext.Current;
                if ((m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
                if (m_AppCache == null) m_AppCache = new AppCache();
                CRuntime xrun = new CRuntime(m_AppCache, null);
                xrun = new CRuntime(m_AppCache, null);
                xrun.Clients = null;//this;
                /*
                                xrun.Clients = this;
                */
                xrun.myHttpContext = null;
                xrun.sVersion = sVersion;
                string sResult = xrun.RunTemplate(strTemplateName, hParameters, sbResponse, ref sExit);
                sResponse = sbResponse.ToString();
                if (xrun.ExitValue != "") sExit = xrun.ExitValue;
                if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            }
            catch (Exception ex)
            {
                sExit = ex.Message;
            }
            if (sExit == "")
                return sResponse;
            else
                return sExit;
        }

        private string myReadContent(HttpRequest req)
        {
            string postedData = "";
            int iContentLength = req.ContentLength;
            //if (iContentLength < 5000)
            //{
            StreamReader reader = new StreamReader(req.InputStream, req.ContentEncoding);
            postedData = reader.ReadToEnd().Trim();
            return postedData;
            //}
            //else
            //    return "[[datasize exceeds limit]]";
        }
        private string getRequestTicket(HttpRequest req)
        {
            string sTicket = "";
            NameValueCollection col = req.Params;
            if (col["ticket"] != null)
                sTicket = col["ticket"].ToString();
            return sTicket;
        }
    }

    public class CRuntime
    {

        private int m_Runs = 0;
        public int Runs
        {
            get { return m_Runs; }
            set { m_Runs = value; }
        }
        internal string m_trace = "";

        private static Logger m_logger = null;
        private SVCache m_svCache = null;
        public SVCache SVCache
        {
            get { return m_svCache; }
            set { m_svCache = value; }
        }
        private AppCache  m_appCache = null;
        public AppCache AppCache
        {
            get { return m_appCache; }
            //set { m_appCache = value; }
        }
        private XConnectors m_connectors = null;
        public XConnectors Connectors
        {
            get { return m_connectors; }
        }
        private ClientData m_clientData = null;

        private HttpContext m_HttpContext = null;
        //IXManagerImportExport m_manager = null;  
        public object myHttpContext
        {
            [SecurityCritical]
            get { return m_HttpContext; }
            [SecurityCritical]
            set { m_HttpContext = (HttpContext)value; }
        }
        private string m_userName = "anon";
        public string UserName
        {
            [SecurityCritical]
            get { return m_userName; }
        }

        //private WSClients m_wsClient = null;
        //public WSClients Clients
        //{
        //    get { return m_wsClient; }
        //    set { m_wsClient = value; }
        //}
        private WSClients wsClients = null;
        public WSClients Clients
        {
            get {
                if (wsClients != null) return wsClients;
                if ((string)System.Configuration.ConfigurationManager.AppSettings["WS"] == "1")
                {
                    if (wsClients == null)
                        wsClients = (WSClients)((HttpContext)myHttpContext).Application["wsClients"];
                    if (wsClients == null)
                    {
                        wsClients = new WSClients();
                        ((HttpContext)myHttpContext).Application["wsClients"] = wsClients;
                    }
                }
                return wsClients;
            }
            set { wsClients = (WSClients) value; }
        }

        public string sVersion = "";
        public bool ExitRequest = false;
        public string ExitValue = "";
        public CRuntime() : this("", null,null) { }
        public CRuntime(string connectionString): this(connectionString, null,null){}
        public CRuntime(AppCache appCache) : this("", appCache, null) { }
        public CRuntime(AppCache appCache,SVCache svCache) : this("", appCache, svCache) { }
        public CRuntime(string connectionString, AppCache appCache, SVCache svCache)
        {
            m_logger = LogManager.GetCurrentClassLogger();

            if (appCache == null)
                m_appCache = new AppCache();
            else
                m_appCache = appCache;
            if (svCache == null)
                m_svCache = new SVCache();
            else
                m_svCache = svCache;
//            m_connectors = new XConnectors();
            m_connectors = new XConnectors("", m_appCache.sConnections);
            if ((m_appCache.sConnections == "") && (m_connectors.jConnections != ""))
            {
                m_appCache.sConnections = m_connectors.jConnections;
                m_appCache.GetCfg("write", "json");
            }
            if (connectionString == "") connectionString = "#";
            m_clientData = m_connectors.GetConnector(connectionString);
            m_appCache.ClientData = m_clientData;
            FillUserData();
            if (System.Configuration.ConfigurationManager.AppSettings["trace"] != null) m_trace = (string)System.Configuration.ConfigurationManager.AppSettings["trace"];
        }
        public void Dispose()
        {
            if (m_clientData != null) m_clientData.Dispose();
            //if (m_manager != null) m_manager.Dispose();
            if (m_connectors != null) m_connectors.Dispose();
        }
        void FillUserData()
        {
            try
            {
                string sUserName = WindowsIdentity.GetCurrent().Name;
                //if (sUserName.IndexOf("\\") != -1)
                //    sUserName = sUserName.Substring(sUserName.IndexOf("\\") + 1);
                m_userName = sUserName;
            }
            catch (Exception)
            { }
        }


        public void Message(string sSessionID, string sLevel, string sText)
        {
            //System.Diagnostics.Debug.WriteLine("MSG:" + sLevel + " : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

            //log.Debug("Application loaded successfully.");
            switch(sLevel.ToLower())
            {
                case "info":
                    m_logger.Info(sText);
                    break;
                case "error":
                    m_logger.Error(sText);
                    break;
                case "fatal":
                    m_logger.Fatal(sText);
                    break;
                case "trace":
                    m_logger.Trace (sText);
                    break;
                case "warn":
                    m_logger.Warn(sText);
                    break;
                default:
                    m_logger.Debug( "["+sSessionID+"] " + sLevel+ ": "+ sText);
                    break;
            }
            if (m_clientData != null)
            {
                string sMessage = "";
                try
                {
                    sMessage += " insert into s_Message (sessionid,Level,content) values ('" + Utils.L_String(sSessionID) + "','" + Utils.L_String(sLevel) + "','" + Utils.L_String(sText) + "')";
                    m_clientData.ExecuteNonQuery(sMessage);
                }
                catch (Exception ex)
                {
                    //                System.Diagnostics.Debug.WriteLine(ex.Message);
                    if (ex.Message.StartsWith("Invalid object name"))
                    {
                        string sMessage1 = @"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[S_Message]') AND type in (N'U')) 
                                    CREATE TABLE [S_Message](
                                    	[MessageID] [int] IDENTITY(1,1) NOT NULL,
                                    	[SessionID] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[Level] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[Content] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[MessageDateTime] [datetime] NULL CONSTRAINT [DF_S_Message_MessageDateTime]  DEFAULT (getdate()),
                                     CONSTRAINT [PK_S_Message] PRIMARY KEY CLUSTERED ([MessageID] ASC) WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]) ON [PRIMARY] ";
                        try
                        {
                            m_clientData.ExecuteNonQuery(sMessage1 + " " + sMessage);
                        }
                        catch (Exception) { };
                    }
                }
            }
        }
        //#region S_Files
        //[SecurityCritical]
        //public void ReadSFile(string sKey, out string sFileName, out  string sFileExtension, out byte[] aFileContent, out string sFileContentType)
        //{
        //    sFileName = "";
        //    sFileExtension = "";
        //    aFileContent = null;
        //    sFileContentType = "";
        //    try
        //    {
        //        string sQueryLog = @"";

        //        sQueryLog += " select FileName,FileExtension,FileContent,FileContentType from S_Files where FileID = '" +  Utils.L_String (sKey) + "'";
        //        DataTable dt = m_clientData.GetDataTable(sQueryLog);
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            sFileName = dt.Rows[0][0].ToString();
        //            sFileExtension = dt.Rows[0][1].ToString();
        //            aFileContent = (byte[])dt.Rows[0][2];
        //            sFileContentType = dt.Rows[0][3].ToString();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //        throw;
        //    }
        //}
        //[SecurityCritical]
        //public string WriteSFile(string sKey, string sFileName, string sFileExtension, byte[] aFileContent, string sFileContentType)
        //{
        //    string sFileLog = "";
        //    string sFileID = sKey;
        //    try
        //    {
        //        if (sKey != "") sFileLog += " delete from S_Files where FileID = '" +  Utils.L_String (sKey) + "'";
        //        if (aFileContent != null)
        //        {
        //            if (sKey != "")
        //            {
        //                sFileLog += " IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[s_Files]') AND type in (N'U')) SET IDENTITY_INSERT s_Files on";
        //                sFileLog += " insert into S_Files (FileID,FileName,FileExtension,FileContent,FileContentType) values (" +  Utils.L_String (sKey) + ",'"
        //                    +  Utils.L_String (sFileName) + "','" +  Utils.L_String (sFileExtension) + "',@FileContent,'" +  Utils.L_String (sFileContentType) + "')";
        //                sFileLog += " IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[s_Files]') AND type in (N'U')) SET IDENTITY_INSERT s_Files on";
        //            }
        //            else
        //                sFileLog += " insert into S_Files (FileName,FileExtension,FileContent,FileContentType) values ('"
        //                +  Utils.L_String (sFileName) + "','" +  Utils.L_String (sFileExtension) + "',@FileContent,'" +  Utils.L_String (sFileContentType) + "')";

        //            sFileLog += " SELECT SCOPE_IDENTITY() fileid";
        //        }
        //        DataTable dt;
        //        if (aFileContent != null)
        //            dt = m_clientData.GetDataTable(sFileLog, new IDataParameter[] { new SqlParameter("@FileContent", aFileContent) });
        //        else
        //            dt = m_clientData.GetDataTable(sFileLog);
        //        if (dt != null && dt.Rows.Count > 0)
        //            sFileID = dt.Rows[0][0].ToString();
        //        return sFileID;
        //    }
        //    catch (Exception ex)
        //    {
        //        //                System.Diagnostics.Debug.WriteLine(ex.Message);
        //        if (ex.Message.StartsWith("Invalid object name"))
        //        {
        //            string sFileLog1 = @"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[S_Files]') )
        //                CREATE TABLE [S_Files]([FileID] [int] IDENTITY(1,1) NOT NULL,	[FileName] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, [FileExtension] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, [FileContent] [varbinary](max) NULL,	[FileContentType] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, CONSTRAINT [PK_S_Files] PRIMARY KEY CLUSTERED (	[FileID] ASC )WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]) ON [PRIMARY] ";
        //            m_clientData.ExecuteNonQuery(sFileLog1 + " " + sFileLog);
        //        }
        //        else
        //            throw;

        //    }
        //    return "";
        //}
        //#endregion S_Files

        Hashtable m_duration = CollectionsUtil.CreateCaseInsensitiveHashtable();
        Hashtable m_count = CollectionsUtil.CreateCaseInsensitiveHashtable();
        public void AddTimespan(string group, double d)
        {
            if (m_duration.ContainsKey(group))
            {
                m_duration[group] = d + (double)m_duration[group];
                m_count[group] = 1 + (int) m_count[group];
            }
            else
            {
                m_duration[group] = d;
                m_count[group] = 1;
            }
        }

        public string RunTemplate(string strTemplateName, Hashtable hParameters, StringBuilder sb, ref string sExit)
        {
            if (sVersion.StartsWith("5"))
                return RunTemplate5(strTemplateName, hParameters, sb, ref sExit);
            else if (strTemplateName.EndsWith("-v4"))
            {
                strTemplateName = strTemplateName.Substring(0, strTemplateName.Length - "-v4".Length);
                return RunTemplate5(strTemplateName, hParameters, sb, ref sExit);
            }
            else
                return RunTemplate34(strTemplateName, hParameters, sb, ref sExit);
        }
        public string RunTemplate34(string strTemplateName, Hashtable hParameters, StringBuilder sb, ref string sExit)
        {
            string sResult = "";
            string sWarning = "";
            JCmd o = m_appCache.RequestTemplateTree(strTemplateName, ref sWarning);
            CProcess p = new CProcess(this, strTemplateName);

            DateTime dt1 = DateTime.Now;
            string sUri = "";
            if (HttpContext.Current != null) if (HttpContext.Current.Request != null) sUri = HttpContext.Current.Request.Path + "?" + HttpContext.Current.Request.QueryString + ";";
            if (m_trace != "")
                Message(p.GetParameterValue("_SESSIONID_"), "Trace", "Start Request;               " + "; " + sUri);
            p.Run(o, hParameters, sb, ref sExit);
            if (m_trace != "")
                Message(p.GetParameterValue("_SESSIONID_"), "Trace", "End Request; " + DateTime.Now.Subtract(dt1).TotalMilliseconds.ToString("F0").PadLeft(6) + "; ".PadLeft(12) + sUri);

            //foreach (string key in m_duration.Keys)
            //    Message("1", "4.0 " + key, m_count[key].ToString() + " : " + m_duration[key].ToString());
            return sResult;
        }
        public string RunTemplate5(string strTemplateName, Hashtable hParameters, StringBuilder sb, ref string sExit)
        {
            string sResult = "";
            string sWarning = "";
            JCmd4  o =  m_appCache.RequestTemplateTree4(strTemplateName,ref sWarning);
            CProcess p = new CProcess(this, strTemplateName);

            DateTime dt1 = DateTime.Now;
            string sUri = "";
            if (HttpContext.Current != null) if (HttpContext.Current.Request != null) sUri = HttpContext.Current.Request.Path + "?" + HttpContext.Current.Request.QueryString + ";";
            if (m_trace != "")
                Message(p.GetParameterValue("_SESSIONID_"), "Trace", "Start Request;               " + "; " + sUri);
            p.Run4(o, hParameters, sb, ref sExit );
            if (m_trace != "")
                Message(p.GetParameterValue("_SESSIONID_"), "Trace", "End Request; " + DateTime.Now.Subtract(dt1).TotalMilliseconds.ToString("F0").PadLeft(6) + "; ".PadLeft(12) + sUri);

            //foreach (string key in m_duration.Keys)
            //    Message("1", "4.0 " + key, m_count[key].ToString() + " : " + m_duration[key].ToString());
            return sResult;
        }
        //public string RunTemplate1(string strTemplateName, Hashtable hParameters, ref string sExit)
        //{
        //    string sResult = "";
        //    //string sWarning = "";
        //    //JCmd o = m_appCache.RequestTemplateTree(strTemplateName, ref sWarning);
        //    //CProcess p = new CProcess(this, strTemplateName);
        //    //sResult = p.SRun(o, hParameters, ref sExit);
        //    ////if (hParameters["noDash"] == null)
        //    ////{
        //    ////    sResult = sResult.Replace("%d%", "#");
        //    ////    sResult = sResult.Replace("%dash%", "#");
        //    ////    sResult = sResult.Replace("%macro%", "@@");
        //    ////    sResult = sResult.Replace("%at%", "@");
        //    ////    sResult = sResult.Replace("%percent%", "%");
        //    ////    sResult = sResult.Replace("_percent_", "%");
        //    ////}

        //    ////foreach (string key in m_duration.Keys)
        //    ////    Message("1", "4.0_v1 " + key, m_count[key].ToString() + " : " + m_duration[key].ToString());
        //    return sResult;
        //}
        //public string RunTemplate2(string strTemplateName, Hashtable hParameters, StringBuilder sb, ref string sExit)
        //{
        //    string sResult = "";
        //    //string sWarning = "";
        //    //JCmd o = m_appCache.RequestTemplateTree(strTemplateName, ref sWarning);
        //    //CProcess2 p = new CProcess2(this, strTemplateName);
        //    //sResult = p.Run(o, hParameters, sb, ref sExit);
        //    //Message("", "4.0_v2", dd.ToString());
        //    return sResult;
        //}
        public string GetResponse(string strSQL, string connName, ref string strCookies)
        {
            ClientData providerData = m_connectors.GetConnector(connName);
            if (providerData == null) throw new Exception("Connection " + connName + " not found");
            string ret = providerData.GetResponse(strSQL,ref strCookies);
            return ret;
        }

        //#region Public Methods

        //[SecurityCritical]
        //string IXManagerImportExport.ImportTemplate(int iConnID, string sTemplateName, string sTemplateXML)
        //{
        //    XDocManager xm;
        //    if (iConnID < 1) xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(iConnID);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    try
        //    {
        //        XmlDocument xml_doc = new XmlDocument();
        //        xml_doc.LoadXml(sTemplateXML);
        //        if (sTemplateName == "ImportSet")
        //        {
        //            string sResult = "";
        //            sResult += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
        //            //System.Diagnostics.Debug.WriteLine("LD1:" + name + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));

        //            IXTemplate xt;
        //            XmlNodeList list = xml_doc.DocumentElement.ChildNodes;
        //            foreach (XmlNode nodet in list)
        //            {
        //                sTemplateName = GetXMLNodeValue(nodet, "Name");
        //                try
        //                {
        //                    xt = xm.LoadTemplate(sTemplateName);
        //                }
        //                catch (Exception ex)
        //                {
        //                    xt = xm.NewTemplate();
        //                }

        //                ImportFromXML(xt, xm, nodet, sTemplateName, "");
        //                string s = "ID:" + xt.ID.ToString() + " Name:" + xt.Name;
        //                sResult += s + "\n";
        //            }
        //            sResult += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
        //            return sResult;
        //        }
        //        else
        //        {
        //            if (sTemplateName == "fromXML") sTemplateName = GetXMLNodeValue(xml_doc.DocumentElement, "Name");
        //            IXTemplate xt;
        //            try
        //            {
        //                xt = xm.LoadTemplate(sTemplateName);
        //            }
        //            catch (Exception ex)
        //            {
        //                xt = xm.NewTemplate();
        //            }

        //            ImportFromXML(xt, xm, xml_doc, sTemplateName, "");
        //            string s = "ID:" + xt.ID.ToString() + " Name:" + xt.Name;
        //            return s;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Import failed. " + ex.Message;
        //    }
        //    /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
        //    finally
        //    {
        //        if (iConnID >= 1 && xm != null) xm.Dispose();
        //    }

        //}
        //[SecurityCritical]
        //string IXManagerImportExport.ImportTemplate(int iConnID, int iTemplateID, string sTemplateXML)
        //{
        //    XDocManager xm;
        //    if (iConnID < 1) xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(iConnID);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    IXTemplate xt;
        //    try
        //    {
        //        xt = xm.LoadTemplate(iTemplateID);
        //    }
        //    catch (Exception ex)
        //    {
        //        xt = xm.NewTemplate();
        //    }
        //    try
        //    {
        //        XmlDocument xml_doc = new XmlDocument();
        //        xml_doc.LoadXml(sTemplateXML);
        //        ImportFromXML(xt, xm, xml_doc, "", "");
        //        return xt.ID.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Import failed";
        //    }
        //    /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
        //    finally
        //    {
        //        if (iConnID >= 1 && xm != null) xm.Dispose();
        //    }
        //}
        //[SecurityCritical]
        //string IXManagerImportExport.ImportTemplate(string sConn, string sTemplateName, string sTemplateXML)
        //{
        //    //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "import manager started");
        //    XDocManager xm;
        //    if (sConn == "") xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(sConn);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    try
        //    {
        //        XmlDocument xml_doc = new XmlDocument();
        //        xml_doc.LoadXml(sTemplateXML);
        //        //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "xml loaded");
        //        if (sTemplateName == "ImportSet")
        //        {
        //            string sResult = "";
        //            sResult += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
        //            IXTemplate xt;
        //            XmlNodeList list = xml_doc.DocumentElement.ChildNodes;
        //            //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "loop start");
        //            foreach (XmlNode nodet in list)
        //            {
        //                try
        //                {
        //                    //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "item start");
        //                    sTemplateName = GetXMLNodeValue(nodet, "Name");
        //                    string sContent = GetXMLNodeValue(nodet, "Content");
        //                    sContent = sContent.Replace(Utils.CT_ENDCDATA, "]]>");
        //                    sContent = sContent.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
        //                    sContent = sContent.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
        //                    string sContentExpanded = new XTemplate(this).ExpandContent(sContent);
        //                    if (sContentExpanded == sContent)
        //                        sContentExpanded = "{no_macros}";
        //                    sContent = sContent.Replace("'", "''");
        //                    sContentExpanded = sContentExpanded.Replace("'", "''");
        //                    string sSQL = "";
        //                    sSQL += "UPDATE S_TEMPLATES SET TEMPLATECONTENT='" + sContent + "' , TEMPLATECONTENTEXPANDED ='" + sContentExpanded + "', TEMPLATETOREDIRECT = '" + "dirty " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "'  WHERE TEMPLATENAME = '" + sTemplateName + "' ";
        //                    sSQL += "if @@ROWCOUNT=0 BEGIN ";
        //                    sSQL += "INSERT INTO S_TEMPLATES (TEMPLATENAME, TEMPLATEDESCRIPTION, TEMPLATECONTENT, TEMPLATECONTENTEXPANDED, TEMPLATETOSAVE, TEMPLATETOREDIRECT, UNATTENDED, DATATEMPLATE,RIGHTS, DOCTYPE)";
        //                    sSQL += "VALUES ( '" + sTemplateName + "', '', '" + sContent + "',  '" + sContentExpanded + "', 0, '" + "dirty " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "', 0, 1, '', ''); SELECT SCOPE_IDENTITY() AS TEMPLATEID ";
        //                    sSQL += " END else SELECT TemplateID FROM S_TEMPLATES WHERE TEMPLATENAME = '" + sTemplateName + "'; ";
        //                    string sTemplateID = "-1";
        //                    DataTable dt = xm.m_dataProvider.GetDataTable(sSQL);
        //                    if (dt != null && dt.Rows.Count > 0)
        //                        sTemplateID = dt.Rows[0][0].ToString();

        //                    string s = "ID:" + sTemplateID + " Name:" + sTemplateName;
        //                    sResult += s + "\n";
        //                    //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "item end");
        //                }
        //                catch (Exception ex)
        //                {
        //                    System.Diagnostics.Debug.WriteLine(ex.Message);
        //                }

        //            }
        //            sResult += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
        //            return sResult;
        //        }
        //        else
        //        {
        //            if (sTemplateName == "fromXML") sTemplateName = GetXMLNodeValue(xml_doc.DocumentElement, "Name");
        //            IXTemplate xt;
        //            try
        //            {
        //                xt = xm.LoadTemplate(sTemplateName);
        //            }
        //            catch (Exception ex)
        //            {
        //                xt = xm.NewTemplate();
        //            }

        //            ImportFromXML(xt, xm, xml_doc, sTemplateName, "");
        //            string s = "ID:" + xt.ID.ToString() + " Name:" + xt.Name;
        //            return s;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Import failed. " + ex.Message;
        //    }
        //    /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
        //    finally
        //    {
        //        if ((sConn != "") && (xm != null)) xm.Dispose();
        //    }

        //}

        //[SecurityCritical]
        //string IXManagerImportExport.ImportTemplate(string sConn, int iTemplateID, string sTemplateXML)
        //{
        //    XDocManager xm;
        //    if (sConn == "") xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(sConn);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    IXTemplate xt;
        //    try
        //    {
        //        xt = xm.LoadTemplate(iTemplateID);
        //    }
        //    catch (Exception ex)
        //    {
        //        xt = xm.NewTemplate();
        //    }
        //    try
        //    {
        //        XmlDocument xml_doc = new XmlDocument();
        //        xml_doc.LoadXml(sTemplateXML);
        //        ImportFromXML(xt, xm, xml_doc, "", "");
        //        return xt.ID.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Import failed";
        //    }
        //    /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
        //    finally
        //    {
        //        if ((sConn != "") && (xm != null)) xm.Dispose();
        //    }
        //}
        //[SecurityCritical]
        //string IXManagerImportExport.ExportTemplatesLike(int iConnID, string sTemplatePattern, string sPostfix)
        //{
        //    string sResult = "";
        //    XDocManager xm;
        //    if (iConnID < 1) xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(iConnID);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    sResult = "<TemplateSet>";

        //    string sSQL = "SELECT TemplateName FROM [S_Templates] WHERE TemplateName LIKE '" + sTemplatePattern + "' ESCAPE '\\' ORDER BY TemplateName ASC ";
        //    DataTable dt = xm.Connectors.SystemConnector.GetDataTable(sSQL);
        //    if (dt != null && dt.Rows.Count > 0)
        //    {
        //        foreach (DataRow row in dt.Rows)
        //        {
        //            string sTemplateName = ClientData.GetStringResult(row[0], "");
        //            IXTemplate xt;
        //            try
        //            {
        //                xt = xm.LoadTemplate(sTemplateName);
        //            }
        //            catch (Exception ex)
        //            {
        //                return "Error: Invalid template";
        //            }
        //            xt.Name = xt.Name + sPostfix;
        //            XmlDocument xml_doc = new XmlDocument();
        //            xml_doc = ExportToXML(xt, xm, true, true);
        //            sResult += xml_doc.OuterXml;

        //        }
        //    }

        //    sResult += "</TemplateSet>";

        //    if (iConnID >= 1 && xm != null) xm.Dispose();
        //    return sResult;
        //}
        //[SecurityCritical]
        //string IXManagerImportExport.ExportTemplate(int iConnID, string sTemplateName)
        //{
        //    XDocManager xm;
        //    if (iConnID < 1) xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(iConnID);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    IXTemplate xt;
        //    try
        //    {
        //        xt = xm.LoadTemplate(sTemplateName);
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Invalid template";
        //    }
        //    XmlDocument xml_doc = new XmlDocument();
        //    xml_doc = ExportToXML(xt, xm, true, true);

        //    /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
        //    if (iConnID >= 1 && xm != null) xm.Dispose();

        //    return xml_doc.OuterXml;
        //}
        //[SecurityCritical]
        //string IXManagerImportExport.ExportTemplate(int iConnID, int iTemplateID)
        //{
        //    XDocManager xm;
        //    if (iConnID < 1) xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(iConnID);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    IXTemplate xt;
        //    try
        //    {
        //        xt = xm.LoadTemplate(iTemplateID);
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Invalid template";
        //    }
        //    XmlDocument xml_doc = new XmlDocument();
        //    xml_doc = ExportToXML(xt, xm, true, true);

        //    /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
        //    if (iConnID >= 1 && xm != null) xm.Dispose();

        //    return xml_doc.OuterXml;
        //}
        //[SecurityCritical]
        //string IXManagerImportExport.ExportTemplatesLike(string sConn, string sTemplatePattern, string sPostfix)
        //{
        //    string sResult = "";
        //    XDocManager xm;
        //    if (sConn == "") xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(sConn);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    sResult = "<TemplateSet>";

        //    string sSQL = "SELECT TemplateName FROM [S_Templates] WHERE TemplateName LIKE '" + sTemplatePattern + "' ESCAPE '\\' ORDER BY TemplateName ASC ";
        //    DataTable dt = xm.Connectors.SystemConnector.GetDataTable(sSQL);
        //    if (dt != null && dt.Rows.Count > 0)
        //    {
        //        foreach (DataRow row in dt.Rows)
        //        {
        //            string sTemplateName = ClientData.GetStringResult(row[0], "");
        //            IXTemplate xt;
        //            try
        //            {
        //                xt = xm.LoadTemplate(sTemplateName);
        //            }
        //            catch (Exception ex)
        //            {
        //                return "Error: Invalid template";
        //            }
        //            xt.Name = xt.Name + sPostfix;
        //            XmlDocument xml_doc = new XmlDocument();
        //            xml_doc = ExportToXML(xt, xm, true, true);
        //            sResult += xml_doc.OuterXml;

        //        }
        //    }

        //    sResult += "</TemplateSet>";

        //    if ((sConn != "") && (xm != null)) xm.Dispose();
        //    return sResult;
        //}
        //[SecurityCritical]
        //string IXManagerImportExport.ExportTemplate(string sConn, string sTemplateName)
        //{
        //    XDocManager xm;
        //    if (sConn == "") xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(sConn);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    IXTemplate xt;
        //    try
        //    {
        //        xt = xm.LoadTemplate(sTemplateName);
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Invalid template";
        //    }
        //    XmlDocument xml_doc = new XmlDocument();
        //    xml_doc = ExportToXML(xt, xm, true, true);

        //    /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
        //    if ((sConn != "") && (xm != null)) xm.Dispose();

        //    return xml_doc.OuterXml;
        //}
        //[SecurityCritical]
        //string IXManagerImportExport.ExportTemplate(string sConn, int iTemplateID)
        //{
        //    XDocManager xm;
        //    if (sConn == "") xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(sConn);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    IXTemplate xt;
        //    try
        //    {
        //        xt = xm.LoadTemplate(iTemplateID);
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Invalid template";
        //    }
        //    XmlDocument xml_doc = new XmlDocument();
        //    xml_doc = ExportToXML(xt, xm, true, true);

        //    /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
        //    if ((sConn != "") && (xm != null)) xm.Dispose();

        //    return xml_doc.OuterXml;
        //}

        //[SecurityCritical]
        //string IXManagerImportExport.RemoveTemplate(int iConnID, string sTemplateName)
        //{
        //    XDocManager xm;
        //    if (iConnID < 1) xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(iConnID);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    try
        //    {
        //        xm.DeleteTemplate(sTemplateName);
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Invalid template";
        //    }

        //    if (iConnID >= 1 && xm != null) xm.Dispose();

        //    return sTemplateName;
        //}
        //[SecurityCritical]
        //string IXManagerImportExport.RemoveTemplate(int iConnID, int iTemplateID)
        //{
        //    XDocManager xm;
        //    if (iConnID < 1) xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(iConnID);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    try
        //    {
        //        xm.DeleteTemplate(iTemplateID);
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Invalid template";
        //    }

        //    if (iConnID >= 1 && xm != null) xm.Dispose();

        //    return iTemplateID.ToString();
        //}
        //[SecurityCritical]
        //string IXManagerImportExport.RemoveTemplate(string sConn, string sTemplateName)
        //{
        //    XDocManager xm;
        //    if (sConn == "") xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(sConn);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    try
        //    {
        //        xm.DeleteTemplate(sTemplateName);
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Invalid template";
        //    }

        //    if ((sConn != "") && (xm != null)) xm.Dispose();

        //    return sTemplateName;
        //}
        //[SecurityCritical]
        //string IXManagerImportExport.RemoveTemplate(string sConn, int iTemplateID)
        //{
        //    XDocManager xm;
        //    if (sConn == "") xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(sConn);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    try
        //    {
        //        xm.LoadTemplate(iTemplateID);
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Invalid template";
        //    }

        //    if ((sConn != "") && (xm != null)) xm.Dispose();

        //    return iTemplateID.ToString();
        //}


        //private int ImportFromXML(IXTemplate xt, XDocManager xm, XmlDocument xmlDoc, string name, string content)
        //{
        //    XmlNode root = xmlDoc.DocumentElement;
        //    if (name == "")
        //        xt.Name = GetXMLNodeValue(root, "Name");
        //    else
        //        xt.Name = name;
        //    if (content == "")
        //        xt.Content = GetXMLNodeValue(root, "Content");
        //    else
        //        xt.Content = content;
        //    xt.Content = xt.Content.Replace(Utils.CT_ENDCDATA, "]]>");
        //    xt.Content = xt.Content.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
        //    xt.Content = xt.Content.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
        //    xt.Description = GetXMLNodeValue(root, "Description");
        //    xt.ToSave = XmlConvert.ToBoolean(GetXMLNodeValue(root, "ToSave", "false"));
        //    xt.Redirect = GetXMLNodeValue(root, "Redirect");
        //    xt.Unattended = XmlConvert.ToBoolean(GetXMLNodeValue(root, "Unattended", "false"));
        //    xt.IsData = XmlConvert.ToBoolean(GetXMLNodeValue(root, "DataTemplate", "true"));
        //    string sRights = GetXMLNodeValue(root, "Rights");
        //    string[] aRights = sRights.Split(';');
        //    int iRight;
        //    foreach (string sRight in aRights)
        //    {
        //        if (int.TryParse(sRight, out iRight))
        //            if (iRight != 0) xt.Roles.Add(iRight);
        //    }
        //    xt.DocType = GetXMLNodeValue(root, "DocType");

        //    xt.Save();

        //    IDName[] queries = xt.GetQueriesList();
        //    foreach (IDName queryIDName in queries)
        //    {
        //        xm.DeleteQuery(queryIDName.ID);
        //    }

        //    XmlElement nodeQueries = (XmlElement)root.SelectSingleNode("Queries");
        //    foreach (XmlElement nQuery in nodeQueries.ChildNodes)
        //    {
        //        IXQuery xq = xm.NewQuery();
        //        xq.TemplateID = xt.ID;
        //        xq.Name = GetXMLNodeValue(nQuery, "Name");
        //        xq.QueryText = GetXMLNodeValue(nQuery, "Text");
        //        xq.QueryText = xq.QueryText.Replace(Utils.CT_ENDCDATA, "]]>");
        //        xq.QueryText = xq.QueryText.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
        //        xq.QueryText = xq.QueryText.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
        //        xq.ConnectionID = -1;
        //        XmlNodeList nConn = nQuery.GetElementsByTagName("Connection");
        //        if (nConn.Count > 0)
        //        {
        //            string sConnectionText = GetXMLNodeValue(nConn[0], "ConnectionString");
        //            string sConnectionName = GetXMLNodeValue(nConn[0], "Name");
        //            try
        //            {
        //                IXConnection xc = xm.LoadConnection(sConnectionName);
        //                xq.ConnectionID = xc.ID;
        //            }
        //            catch (Exception)
        //            {
        //                if (sConnectionName == "")
        //                    xq.ConnectionID = -1;
        //                else
        //                {
        //                    IXConnection xc = xm.NewConnection();
        //                    xc.Name = sConnectionName;
        //                    sConnectionText = sConnectionText.Replace(Utils.CT_ENDCDATA, "]]>");
        //                    sConnectionText = sConnectionText.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
        //                    sConnectionText = sConnectionText.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
        //                    xc.ConnectionString = sConnectionText;
        //                    xc.Save();
        //                    xq.ConnectionID = xc.ID;
        //                }
        //            }
        //        }
        //        xq.Save();
        //    }
        //    return xt.ID;
        //}
        //private int ImportFromXML(IXTemplate xt, XDocManager xm, XmlNode root, string name, string content)
        //{
        //    if (name == "")
        //        xt.Name = GetXMLNodeValue(root, "Name");
        //    else
        //        xt.Name = name;
        //    if (content == "")
        //        xt.Content = GetXMLNodeValue(root, "Content");
        //    else
        //        xt.Content = content;
        //    xt.Content = xt.Content.Replace(Utils.CT_ENDCDATA, "]]>");
        //    xt.Content = xt.Content.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
        //    xt.Content = xt.Content.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
        //    xt.Description = GetXMLNodeValue(root, "Description");
        //    xt.ToSave = XmlConvert.ToBoolean(GetXMLNodeValue(root, "ToSave", "false"));
        //    xt.Redirect = GetXMLNodeValue(root, "Redirect");
        //    xt.Unattended = XmlConvert.ToBoolean(GetXMLNodeValue(root, "Unattended", "false"));
        //    xt.IsData = XmlConvert.ToBoolean(GetXMLNodeValue(root, "DataTemplate", "true"));
        //    string sRights = GetXMLNodeValue(root, "Rights");
        //    string[] aRights = sRights.Split(';');
        //    int iRight;
        //    foreach (string sRight in aRights)
        //    {
        //        if (int.TryParse(sRight, out iRight))
        //            if (iRight != 0) xt.Roles.Add(iRight);
        //    }
        //    xt.DocType = GetXMLNodeValue(root, "DocType");

        //    xt.Save();

        //    IDName[] queries = xt.GetQueriesList();
        //    foreach (IDName queryIDName in queries)
        //    {
        //        xm.DeleteQuery(queryIDName.ID);
        //    }

        //    XmlElement nodeQueries = (XmlElement)root.SelectSingleNode("Queries");
        //    foreach (XmlElement nQuery in nodeQueries.ChildNodes)
        //    {
        //        IXQuery xq = xm.NewQuery();
        //        xq.TemplateID = xt.ID;
        //        xq.Name = GetXMLNodeValue(nQuery, "Name");
        //        xq.QueryText = GetXMLNodeValue(nQuery, "Text");
        //        xq.QueryText = xq.QueryText.Replace(Utils.CT_ENDCDATA, "]]>");
        //        xq.QueryText = xq.QueryText.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
        //        xq.QueryText = xq.QueryText.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
        //        xq.ConnectionID = -1;
        //        XmlNodeList nConn = nQuery.GetElementsByTagName("Connection");
        //        if (nConn.Count > 0)
        //        {
        //            string sConnectionText = GetXMLNodeValue(nConn[0], "ConnectionString");
        //            string sConnectionName = GetXMLNodeValue(nConn[0], "Name");
        //            try
        //            {
        //                IXConnection xc = xm.LoadConnection(sConnectionName);
        //                xq.ConnectionID = xc.ID;
        //            }
        //            catch (Exception)
        //            {
        //                IXConnection xc = xm.NewConnection();
        //                xc.Name = sConnectionName;
        //                sConnectionText = sConnectionText.Replace(Utils.CT_ENDCDATA, "]]>");
        //                sConnectionText = sConnectionText.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
        //                sConnectionText = sConnectionText.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
        //                xc.ConnectionString = sConnectionText;
        //                xc.Save();
        //                xq.ConnectionID = xc.ID;

        //            }
        //        }
        //        xq.Save();
        //    }
        //    return xt.ID;
        //}

        //private string GetXMLNodeValue(XmlNode queryNode, string childNode)
        //{
        //    return GetXMLNodeValue(queryNode, childNode, "");
        //}
        //private string GetXMLNodeValue(XmlNode queryNode, string childNode, string defaultValue)
        //{
        //    if (queryNode.SelectSingleNode(childNode) != null)
        //        return queryNode.SelectSingleNode(childNode).FirstChild is XmlCDataSection ?
        //            (queryNode.SelectSingleNode(childNode).FirstChild as XmlCDataSection).Value.Replace("<.!.[.CDATA.[", "<![CDATA[").Replace("].].>", "]]>") :
        //            queryNode.SelectSingleNode(childNode).InnerText;
        //    else
        //        if (queryNode.Attributes[childNode] != null)
        //            return queryNode.Attributes[childNode].Value;
        //        else return defaultValue;
        //}

        //private XmlDocument ExportToXML(IXTemplate xt, XDocManager xm, bool bComplete, bool bContent)
        //{
        //    string sCDATAContent;
        //    XmlDocument doc = new XmlDocument();
        //    XmlElement root = doc.CreateElement("Template");
        //    doc.AppendChild(root);

        //    XmlCDataSection elem_cData;

        //    XmlElement elem = doc.CreateElement("Name");
        //    elem.InnerText = xt.Name;
        //    root.AppendChild(elem);


        //    if (bContent)
        //    {
        //        elem = doc.CreateElement("Content");
        //        //elem_cData = doc.CreateCDataSection(xt.Content.Replace("<![CDATA[","<.!.[.CDATA.[").Replace("]]>","].].>" ));
        //        sCDATAContent = xt.Content;
        //        sCDATAContent = sCDATAContent.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA3);
        //        sCDATAContent = sCDATAContent.Replace(Utils.CT_ENDCDATA, Utils.CT_ENDCDATA2);
        //        sCDATAContent = sCDATAContent.Replace("]]>", Utils.CT_ENDCDATA);
        //        elem_cData = doc.CreateCDataSection(sCDATAContent);
        //        elem.AppendChild(elem_cData);
        //        root.AppendChild(elem);
        //    }

        //    elem = doc.CreateElement("Description");
        //    elem.InnerText = xt.Description;
        //    root.AppendChild(elem);

        //    elem = doc.CreateElement("ToSave");
        //    elem.InnerText = XmlConvert.ToString(xt.ToSave);
        //    root.AppendChild(elem);

        //    elem = doc.CreateElement("Redirect");
        //    elem.InnerText = xt.Redirect;
        //    root.AppendChild(elem);

        //    elem = doc.CreateElement("DataTemplate");
        //    elem.InnerText = XmlConvert.ToString(xt.IsData);
        //    root.AppendChild(elem);

        //    elem = doc.CreateElement("Unattended");
        //    elem.InnerText = XmlConvert.ToString(xt.Unattended);
        //    root.AppendChild(elem);

        //    if (bComplete)
        //    {
        //        string sRoles = "";
        //        foreach (int role in xt.Roles)
        //            sRoles += ";" + role.ToString();
        //        if (sRoles.Length > 0) sRoles = sRoles.Substring(1);
        //        elem = doc.CreateElement("Rights");
        //        elem.InnerText = sRoles;
        //        root.AppendChild(elem);

        //        elem = doc.CreateElement("DocType");
        //        elem.InnerText = xt.DocType;
        //        root.AppendChild(elem);
        //    }

        //    XmlElement elemQueries = doc.CreateElement("Queries");
        //    root.AppendChild(elemQueries);

        //    IDName[] queries = xt.GetQueriesList();
        //    foreach (IDName queryIDName in queries)
        //    {
        //        XDocuments.IXQuery xq = xm.LoadQuery(queryIDName.ID);

        //        XmlElement elemQuery = doc.CreateElement("Query");

        //        elem = doc.CreateElement("Name");
        //        elem.InnerText = xq.Name;
        //        elemQuery.AppendChild(elem);

        //        elem = doc.CreateElement("Text");
        //        //elem_cData = doc.CreateCDataSection(xq.QueryText.Replace("<![CDATA[", "<.!.[.CDATA.[").Replace("]]>", "].].>"));
        //        sCDATAContent = xq.QueryText;
        //        sCDATAContent = sCDATAContent.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA3);
        //        sCDATAContent = sCDATAContent.Replace(Utils.CT_ENDCDATA, Utils.CT_ENDCDATA2);
        //        sCDATAContent = sCDATAContent.Replace("]]>", Utils.CT_ENDCDATA);
        //        elem_cData = doc.CreateCDataSection(sCDATAContent);

        //        elem.AppendChild(elem_cData);
        //        elemQuery.AppendChild(elem);

        //        XmlElement elemConnection = doc.CreateElement("Connection");

        //        if (xq.ConnectionID != -1)
        //        {

        //            XDocuments.IXConnection xc = xm.LoadConnection(xq.ConnectionID);

        //            elem = doc.CreateElement("Name");
        //            elem.InnerText = xc.Name;
        //            elemConnection.AppendChild(elem);

        //            if (bComplete)
        //            {
        //                elem = doc.CreateElement("ConnectionString");
        //                //                        elem_cData = doc.CreateCDataSection(xc.ConnectionString.Replace("<![CDATA[", "<.!.[.CDATA.[").Replace("]]>", "].].>"));
        //                sCDATAContent = xc.ConnectionString;
        //                sCDATAContent = sCDATAContent.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA3);
        //                sCDATAContent = sCDATAContent.Replace(Utils.CT_ENDCDATA, Utils.CT_ENDCDATA2);
        //                sCDATAContent = sCDATAContent.Replace("]]>", Utils.CT_ENDCDATA);
        //                elem_cData = doc.CreateCDataSection(sCDATAContent);
        //                elem.AppendChild(elem_cData);
        //                elemConnection.AppendChild(elem);
        //            }
        //            elemQuery.AppendChild(elemConnection);
        //        }

        //        elemQueries.AppendChild(elemQuery);


        //    }

        //    return doc;
        //}


        //#endregion Public Methods

    }
}
