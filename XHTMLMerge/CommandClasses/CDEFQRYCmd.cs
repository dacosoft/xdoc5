//using KubionLogNamespace;
using System;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CDEFQRYCmd: CCmd
    {
        protected string
            m_strQueryName = "",
            m_strQueryPar = "";

        public string QueryName
        {
            get { return m_strQueryName; }
            set { m_strQueryName = value; }
        }
        public string QueryPar
        {
            get { return m_strQueryPar; }
            set { m_strQueryPar = value; }
        }

        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"DEFQRY\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"strQueryName\":\""); sJSON.Append(Utils._JsonEscape(m_strQueryName)); sJSON.Append("\"");
            sJSON.Append(",\"strQueryPar\":\""); sJSON.Append(Utils._JsonEscape(m_strQueryPar)); sJSON.Append("\"");
            sJSON.Append("}");
            sbWarning.Append("Command DEFQRY at line "); sbWarning.Append(Line.ToString()); sbWarning.Append(" is obsolete.\r\n");
        }
        
        public CDEFQRYCmd()
            : base()
		{
			m_enType = CommandType.DEFQRYCommand;
			this.m_bIsBlockCommand = false;
		}

  //      public override string Execute(CParser m_parser)
  //      {
  //          string l_strQueryName, l_strQueryPar;
  //          l_strQueryName = m_parser.ReplaceParameters(m_strQueryName);
  //          l_strQueryPar = m_parser.ReplaceParameters(m_strQueryPar);

  //          m_parser.Queries[l_strQueryName] = l_strQueryPar;

		//	return "";
		//}


    }
}


