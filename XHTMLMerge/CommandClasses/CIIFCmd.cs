﻿//////using KubionLogNamespace;
using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
//using XDataSourceModule;

namespace XHTMLMerge
{
    [Serializable]
    public class CIIFCmd : CCmd
    {
        protected string m_condition = "";
        protected string m_trueExpr = "";
        protected string m_falseExpr = "";
         protected string m_strParamName = "";
         protected string m_Opt1 = "";
         protected string m_Opt2 = "";


         delegate bool delegateEvalCondition(string op1, string op2);
         private delegateEvalCondition myEvalCondition = null;
         private void SetEvalCondition(string condition)
         {
             if (condition == "")
                 myEvalCondition = EvalConditionTrue;
             else if (condition.Contains("===="))
             {
                 int i = condition.IndexOf("====");
                 m_Opt1 = condition.Substring(0, i).Trim();
                 m_Opt2 = condition.Substring(i + 4).Trim();
                 myEvalCondition = EvalCondition4E;
             }
             else if (condition.Contains("!==="))
             {
                 int i = condition.IndexOf("!===");
                 m_Opt1 = condition.Substring(0, i).Trim();
                 m_Opt2 = condition.Substring(i + 4).Trim();
                 myEvalCondition = EvalConditionN3E;
             }
             else if (condition.Contains("==="))
             {
                 int i = condition.IndexOf("===");
                 m_Opt1 = condition.Substring(0, i).Trim();
                 m_Opt2 = condition.Substring(i + 3).Trim();
                 myEvalCondition = EvalCondition3E;
             }
             else if (condition.Contains("!=="))
             {
                 int i = condition.IndexOf("!==");
                 m_Opt1 = condition.Substring(0, i).Trim();
                 m_Opt2 = condition.Substring(i + 3).Trim();
                 myEvalCondition = EvalConditionN2E;
             }
             else if (condition.Contains("=="))
             {
                 int i = condition.IndexOf("==");
                 m_Opt1 = condition.Substring(0, i).Trim();
                 m_Opt2 = condition.Substring(i + 2).Trim();
                 myEvalCondition = EvalCondition2E;
             }
             else if (condition.Contains("!="))
             {
                 int i = condition.IndexOf("!=");
                 m_Opt1 = condition.Substring(0, i).Trim();
                 m_Opt2 = condition.Substring(i + 2).Trim();
                 myEvalCondition = EvalConditionN1E;
             }
             else if (condition.Contains(">>>"))
             {
                 int i = condition.IndexOf(">>>");
                 m_Opt1 = condition.Substring(0, i).Trim();
                 m_Opt2 = condition.Substring(i + 3).Trim();
                 myEvalCondition = EvalCondition3G;
             }
             else if (condition.Contains(">>"))
             {
                 int i = condition.IndexOf(">>");
                 m_Opt1 = condition.Substring(0, i).Trim();
                 m_Opt2 = condition.Substring(i + 2).Trim();
                 myEvalCondition = EvalCondition2G;
             }
             else if (condition.Contains("<<<"))
             {
                 int i = condition.IndexOf("<<<");
                 m_Opt1 = condition.Substring(0, i).Trim();
                 m_Opt2 = condition.Substring(i + 3).Trim();
                 myEvalCondition = EvalCondition3L;
             }
             else if (condition.Contains("<<"))
             {
                 int i = condition.IndexOf("<<");
                 m_Opt1 = condition.Substring(0, i).Trim();
                 m_Opt2 = condition.Substring(i + 2).Trim();
                 myEvalCondition = EvalCondition2L;
             }
             else
             {
                 m_condition = "SQL";
                 myEvalCondition = EvalConditionTrue;
             }

         }

        public string Condition
        {
            get { return m_condition; }
            set
            {
                m_condition = value; SetEvalCondition(m_condition);
            }
        }
        public string TrueExpr
        {
            get { return m_trueExpr; }
            set { m_trueExpr = value; }
        }
        public string FalseExpr
        {
            get { return m_falseExpr; }
            set { m_falseExpr = value; }
        }
        		public string ParameterName
		{
			get { return m_strParamName; }
			set { m_strParamName = value; }
		}


        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"IIF\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"ID\":\""); sJSON.Append(Utils._JsonEscape(m_condition)); sJSON.Append("\"");
            sJSON.Append(",\"Value2\":\""); sJSON.Append(Utils._JsonEscape(m_trueExpr)); sJSON.Append("\"");
            sJSON.Append(",\"Value3\":\""); sJSON.Append(Utils._JsonEscape(m_falseExpr)); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_strParamName)); sJSON.Append("\"");
            sJSON.Append("}");
        }

        public CIIFCmd()
            : base()
        {
            m_enType = CommandType.IIFCommand;
            this.m_bIsBlockCommand = false;
        }

        //public override string Execute(CParser m_parser)
        //{
        //    //DateTime dt1 = DateTime.Now; 
        //    string sResult = "";
        //    string l_condition, l_trueExpr, l_falseExpr, l_ParName;
        //    //DateTime dt2 = DateTime.Now;
        //    //process.m_runtime.AddTimespan("IIF_CondS", dt2.Subtract(dt1).TotalMilliseconds);
        //    string l_Opt1 = m_parser.ReplaceParameters(m_Opt1);
        //    string l_Opt2 = m_parser.ReplaceParameters(m_Opt2);
        //    bool condResult = true;
        //    if (m_condition == "SQL")
        //        condResult = EvalConditionSQL(l_Opt1, m_parser.DataProvider);
        //    else
        //        condResult = myEvalCondition(l_Opt1, l_Opt2);

        //    //l_condition = m_parser.ReplaceParameters(m_condition);
        //    //bool condResult = CParser.EvalCondition(l_condition, m_parser.DataProvider);
        //    if (condResult == true)
        //    {
        //        l_trueExpr = m_parser.ReplaceParameters(m_trueExpr);
        //        sResult = l_trueExpr;
        //    }
        //    else
        //    {
        //        l_falseExpr = m_parser.ReplaceParameters(m_falseExpr);
        //        sResult = l_falseExpr;
        //    }
        //    l_ParName = m_parser.ReplaceParameters(m_strParamName);
        //    //DateTime dt2 = DateTime.Now;
        //    //m_parser.AddTimespan("IIF", dt2.Subtract(dt1).TotalMilliseconds);
        //    if (l_ParName != "")
        //    {
        //        m_parser.TemplateParams[l_ParName] = sResult;
        //        return "";
        //    }
        //    else
        //        return sResult;
        //}
        //internal bool RegExMatch(string sRegEx, string sValue, bool bIgnoreCase)
        //{
        //    Regex objPattern;
        //    if (bIgnoreCase)
        //        objPattern = new Regex(sRegEx, RegexOptions.IgnoreCase);
        //    else
        //        objPattern = new Regex(sRegEx);

        //    return objPattern.IsMatch(sValue);
        //}


    }
}





