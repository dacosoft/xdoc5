﻿using System;
using System.Diagnostics;
//using XDataSourceModule;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
//using KubionLogNamespace;

namespace XHTMLMerge
{
    [Serializable]
    public class CJKEYSCmd : CCmd
    {
        # region Protected members

        protected string m_ParamName = "";
        protected string m_Source = "";
        protected string m_JPath = "";
        protected string m_ErrorParamName = "ERROR";

        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"JKEYS\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_ParamName)); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_Source)); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_JPath)); sJSON.Append("\"");
            //sJSON.Append(",\"isParSource\":\""); sJSON.Append(Utils._JsonEscape(m_isParSource)); sJSON.Append("\"");
            base.GetJsonChilds(sJSON,sbWarning);
            sJSON.Append("}");
        }
        #endregion Protected members

        #region Public properties

        public string ParameterName
        {
            get { return m_ParamName; }
            set { m_ParamName = value; }
        }
        public string Source
        {
            get { return m_Source; }
            set { m_Source = value; }
        }
        public string JPath
        {
            get { return m_JPath; }
            set { m_JPath = value; }
        }
        public string ErrorParamName
        {
            get { return m_ErrorParamName; }
            set { m_ErrorParamName = value; }
        }
        //protected string m_isParSource = "0";
        //public string isParSource
        //{
        //    get { return m_isParSource; }
        //    set { m_isParSource = value; }
        //}


        #endregion Public properties

        public CJKEYSCmd()
            : base()
        {
            this.m_enType = CommandType.JKEYSCommand ;
            m_bIsBlockCommand = true;
        }

        //public override string Execute(CParser m_parser)
        //{
        //    string l_ParamName, l_Source, l_JPath, l_ErrorParamName;
        //    l_ParamName = m_parser.ReplaceParameters(m_ParamName);
        //    l_Source = m_parser.ReplaceParameters(m_Source);
        //    l_JPath = m_parser.ReplaceParameters(m_JPath);
        //    l_ErrorParamName = m_parser.ReplaceParameters(m_ErrorParamName);

        //    //if (m_isParSource == "1") l_Source = (string)m_parser.TemplateParams[l_Source];

        //    string val = "";
        //    string sError = "";
        //    string sErrorVerbose = "";
        //    System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();

        //    try
        //    {
        //        JObject o = JObject.Parse(l_Source);
        //        if (l_ParamName == "")
        //            return o.SelectToken(l_JPath).ToString();
        //        else
        //        {
        //            JToken jt = o.SelectToken(l_JPath);
        //            val = jt.ToString();
        //            if (jt is JArray)
        //                m_parser.TemplateParams[l_ParamName + "__count"] = ((JArray)jt).Count.ToString();

        //            m_parser.TemplateParams[l_ParamName] = val;

        //            foreach (JProperty jp in jt.Children<JProperty>())
        //            {
        //                m_parser.TemplateParams[l_ParamName + "__key"] = jp.Name;
        //                if (jp.Value.Type == JTokenType.Date)
        //                {
        //                    DateTime d = (DateTime)jp.Value;
        //                    m_parser.TemplateParams[l_ParamName + "__value"] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
        //                }
        //                else
        //                    m_parser.TemplateParams[l_ParamName + "__value"] = jp.Value.ToString ();
        //                string output = base.Execute(m_parser);
        //                sbRetVal.Append(output);
        //                m_parser.TemplateParams.Remove(l_ParamName + "__key");
        //                m_parser.TemplateParams.Remove(l_ParamName + "__value");
        //            }
        //            return sbRetVal.ToString();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing JKEYS command";
        //        sErrorVerbose = ex.Message;
        //        sErrorVerbose = "Error executing JKEYS command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sErrorVerbose;
        //        sbRetVal.Clear();
        //    }

        //    if (l_ErrorParamName != "")
        //    {
        //        m_parser.TemplateParams[l_ErrorParamName] = sError;
        //        m_parser.TemplateParams[l_ErrorParamName + "Verbose"] = sErrorVerbose;
        //    }

        //    return sbRetVal.ToString();
        //}

    }
}