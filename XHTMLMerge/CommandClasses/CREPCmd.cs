////using KubionLogNamespace;
using System;
using System.Diagnostics;
using System.Text;
//using XDataSourceModule;

namespace XHTMLMerge
{
    [Serializable]
    public class CREPCmd : CCmd
	{
		# region Protected members

        protected int m_TemplateID = -1;
        protected string m_strQueryName = "";
		protected CCmd[] m_Parameters = null;
        //protected CContext m_context = null;
		//protected XDataSourceModule.IXDataSource m_evaluator = null;
        protected CCmd m_pageNumberCmd = null;
        protected CCmd m_pageSizeCmd = null;
        private int m_resultsCount = -1;
        [NonSerialized]
        //protected SourceResult m_Result = null;
        protected bool m_DataTable = false;

        private string GetCmdParameter(CCmd cmd, string sDefault)
        {
            string sResult = sDefault;
            if(cmd != null)
            if (cmd.Type == CommandType.PARCommand)
            {
                sResult = ((CPARCmd)cmd).ParameterName;
                sResult = "%" + sResult + "%";
            }
            else if (cmd.Type == CommandType.TEXTCommand)
                sResult = ((CTextCmd)cmd).Text;
            return sResult;
        }
        private string GetAParameter(string sParameter, string sDefault)
        {
            string sResult = sDefault;
            foreach (CCmd cmd in m_Parameters)
            {
                GetCmdParameter(cmd, "");
                string sValue = GetCmdParameter(cmd, ""); //((CTextCmd)cmd).Text;
                if (sValue.Contains("="))
                {
                    string sParName = sValue.Substring(0, sValue.IndexOf("="));
                    string sValue1 = sValue.Substring(sValue.IndexOf("=") + 1);
                    if (sParName.ToUpper() == sParameter.ToUpper()) sResult = sValue1;
                }
            }
            if (sResult.StartsWith("#PAR."))
                sResult = "%" + sResult.Substring("#PAR.".Length, sResult.Length - 1 - "#PAR.".Length) + "%";
            return sResult;
        }
        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
//#REP.getWFFirstStep(Query=#PAR.WFFirstStepQuery#,Conn=REST_DMS)#
//#JDATA.jd.%WFFirstStepQuery%.REST_DMS#
//#JLOOP.getWFFirstStep.%jd%.result#
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"JDATA\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_TemplateID.ToString())); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape("J" + m_strQueryName)); sJSON.Append("\"");
            string m_strQueryNameU = m_strQueryName.ToUpper();
            if (m_strQueryNameU == "SPLITXX" || m_strQueryNameU == "FOR" || m_strQueryNameU == "SPLITXX2" || m_strQueryNameU == "FOR2" || m_strQueryNameU == "SPLITXX3" || m_strQueryNameU == "FOR3")
            {
                if (m_strQueryNameU.StartsWith("SPLITXX"))
                {
                    sJSON.Append(",\"ID\":\""); sJSON.Append("SPLITXX"); sJSON.Append("\"");
                }
                else if (m_strQueryNameU.StartsWith("FOR"))
                {
                    sJSON.Append(",\"ID\":\""); sJSON.Append("FOR"); sJSON.Append("\"");
                }

                sJSON.Append(",\"Params\":[");
                bool isFirst = true;
                foreach (CCmd condCmd in this.m_Parameters)
                {
                    if (isFirst) isFirst = false; else sJSON.Append(",");
                    sJSON.Append("{");
                    sJSON.Append("\"Type\":\"Text\"");
                    sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
                    sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(GetCmdParameter(condCmd, ""))); sJSON.Append("\"");
                    sJSON.Append("}");
                }

                sJSON.Append("]");
            }
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(GetAParameter("Query", ""))); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(GetAParameter("Conn", ""))); sJSON.Append("\"");
            sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(GetAParameter("Error", "Error"))); sJSON.Append("\"");

            sJSON.Append("}");
            sJSON.Append(",{");
            sJSON.Append("\"Type\":\"JLOOP\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_TemplateID.ToString())); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_strQueryName)); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape("%J" + m_strQueryName + "%")); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape("result")); sJSON.Append("\"");
            string m_PageSize = GetCmdParameter(m_pageSizeCmd, "");
            if (m_PageSize != "") { sJSON.Append(",\"Value2\":\""); sJSON.Append(Utils._JsonEscape(m_PageSize)); sJSON.Append("\""); }
            string m_PageNr = GetCmdParameter(m_pageNumberCmd, "");
            if (m_PageNr != "") {sJSON.Append(",\"Value3\":\""); sJSON.Append(Utils._JsonEscape(m_PageNr)); sJSON.Append("\"");}
            sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(GetAParameter("Error", "Error"))); sJSON.Append("\"");
            base.GetJsonChilds(sJSON, sbWarning);
            sJSON.Append("}");



            //sJSON.Append("{");
            //sJSON.Append("\"Type\":\"REP\"");
            //sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            //sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_TemplateID.ToString())); sJSON.Append("\"");
            //sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_strQueryName)); sJSON.Append("\"");
            //sJSON.Append(",\"Params\":[");
            //bool isFirst = true;
            //foreach (CCmd cCmd in this.m_Parameters)
            //{
            //    if (isFirst) isFirst = false; else sJSON.Append(",");
            //    cCmd.GetJson(sJSON,sbWarning);
            //}
            //sJSON.Append("]");
            //if (m_pageNumberCmd != null) { sJSON.Append(",\"Value3\":"); m_pageNumberCmd.GetJson(sJSON,sbWarning); }
            //if (m_pageSizeCmd != null) { sJSON.Append(",\"Value2\":"); m_pageSizeCmd.GetJson(sJSON,sbWarning);}
            //sJSON.Append(",\"Opt1\":\""); sJSON.Append(Utils._JsonEscape(m_DataTable)); sJSON.Append("\"");
            //base.GetJsonChilds(sJSON, sbWarning);
            //sJSON.Append("}");
            //sbWarning.Append("Command REP at line "); sbWarning.Append(Line.ToString()); sbWarning.Append(" is obsolete.\r\n");
        }


		#endregion Protected members

		#region Public properties

        public int TemplateID
        {
            get { return m_TemplateID; }
            set { m_TemplateID = value; }
        }
         
        public int ResultsCount
        {
            get { return m_resultsCount; }
            set { m_resultsCount = value; }
        }

        public CCmd PageNumberCmd
        {
            get { return m_pageNumberCmd; }
            set { m_pageNumberCmd = value; }
        }

        public CCmd PageSizeCmd
        {
            get { return m_pageSizeCmd; }
            set { m_pageSizeCmd = value; }
        }


		public string QueryName
		{
			get { return m_strQueryName; }
			set { m_strQueryName = value; }
		}

		public CCmd[] Parameters
		{
			get { return m_Parameters; }
			set { m_Parameters = value; }
		}

        //public CContext Context
        //{
        //    get { return m_context; }
        //    set { m_context = value; }
        //}

        //public XDataSourceModule.IXDataSource Evaluator
        //{
        //    get { return m_evaluator; }
        //    set { m_evaluator = value; }
        //}
        public bool DataTable
        {
            get { return m_DataTable; }
            set { m_DataTable = value; if (m_DataTable) m_bIsBlockCommand = false; }
        }


		#endregion Public properties

		public CREPCmd():base()
		{
			this.m_enType = CommandType.REPCommand;		
			m_bIsBlockCommand = true;
		}

        //void m_evaluator_RequestHierarchy(XDataSourceModule.SourceResult sourceResult, string parameters)
        //{
        //    l_parser.RaiseRequestHierarchy(sourceResult, parameters);
        //}

        private string GetErrorParameter(Array m_parameters)
        {
            string sError = "Error";

            for (int index = 0; index < m_parameters.GetLength(0); index++)
            {
                if (m_parameters.GetValue(index) != null)
                {
                    string sValue = m_parameters.GetValue(index).ToString();
                    if (sValue.Contains("="))
                    {
                        string sParName = sValue.Substring(0, sValue.IndexOf("="));
                        string sValue1 = sValue.Substring(sValue.IndexOf("=") + 1);
                        if (sParName.ToUpper() == "ERROR") sError = sValue1;
                    }
                }
            }
            return sError;
        }

  //      public override string Execute(CParser m_parser)
  //      {
  //          string sError = "";
  //          string sErrorVerbose = "";
  //          string sParameters = "";
  //          double dDuration= 0;
  //          string key = "";
  //          //string retVal = "";
  //          System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();

  //          bool bDefQry = (m_parser.Queries[m_strQueryName] != null);
  //          Array queryParameters = GetParamArray(m_parser, bDefQry);
  //          try
  //          {

  //              //m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_evaluator_RequestHierarchy);
  //              m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_parser.parser_RequestHierarchy  );

  //              m_Result = m_parser.Evaluator.EvaluateSourceResult(m_TemplateID, m_strQueryName, queryParameters, out sError, out sErrorVerbose, out sParameters, out dDuration , out key);
  //              m_parser.Context.Keys[this] = key;
  //              m_parser.Context.Results[this] = m_Result;
  //          }
  //          catch (Exception ex)
  //          {
  //              //retVal = "";
  //              Trace.WriteLine(ex);
  //              sError = ex.Message;
  //              sErrorVerbose = sError;
  //          }
  //          string sErrorParameter = GetErrorParameter(queryParameters);
  //          string l_ErrorParName = m_parser.ReplaceParameters(sErrorParameter);
  //          if (sError != "")
  //          {
  //              //sErrorVerbose = "Error executing COUNT command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " with query " + m_strQueryName + sParameters + "; " + newLine + sErrorVerbose;
  //              sErrorVerbose = "Error executing REP command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sErrorVerbose;
  //              m_parser.Message("QueryError", sErrorVerbose);
  //          }
  //          else
  //          {
  //              sErrorVerbose = "";
  //          }
  //          // set par 
  //          if (l_ErrorParName != "")
  //          {
  //              m_parser.ParamDefaults[l_ErrorParName] = sError;
  //              m_parser.TemplateParams[l_ErrorParName] = sError;
  //              m_parser.ParamDefaults[l_ErrorParName + "Verbose"] = sErrorVerbose;
  //              m_parser.TemplateParams[l_ErrorParName + "Verbose"] = sErrorVerbose;
  //          }

  //          m_resultsCount = 0;
  //          if (m_Result != null) m_resultsCount = m_Result.GetCount();
  //          if (m_parser.ParserQueryLog == "1") m_parser.QueryLog(m_strQueryName, sParameters, m_resultsCount, sError, sErrorVerbose,dDuration );

  //          int pageNumber = -1;
  //          int pageSize = -1;
  //          int start = 0, end = m_resultsCount;

  //          if (m_pageNumberCmd != null && m_pageSizeCmd != null)
  //          {
  //              string strPgNum = m_pageNumberCmd.Execute(m_parser);
  //              string strPgSize = m_pageSizeCmd.Execute(m_parser);
  //              if (int.TryParse(strPgNum, out pageNumber))
  //                  if (int.TryParse(strPgSize, out pageSize))
  //                  {
  //                      start = (pageNumber - 1) * pageSize;
  //                      end = pageNumber * pageSize;
  //                  }
  //          }

  //          if (start < 0)
  //              start = 0;
  //          m_parser.Context.OSetQueryIndex (this,start);
  //          //m_context.SetQueryIndex(m_strQueryName, start);

  //          if (m_DataTable)
  //          {
  //              string retVal = "";
  //              if (m_Result != null) retVal = m_Result.GetJSON(start, end);
  //              m_parser.TemplateParams[m_strQueryName + "Data"] = retVal;
  //              return "";
  //          }

  //          for (int i = start; i < end; i++)
  //          {
  //              if (i >= m_resultsCount)
  //                  break;

  //              string output = base.Execute(m_parser);
  //              //retVal += output;
  //              sbRetVal.Append(output);
  //              m_parser.Context.OIncrementQIndex(this);
  //              //m_context.IncrementQIndex(m_strQueryName);
  //          }
  //          //return retVal;
  //          return sbRetVal.ToString();

		//}

        //public override string Execute(CParser m_parser)
        //{
        //    string error = "";
        //    //string retVal = "";

        //    System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();

        //    Array queryParameters = GetParamArray(m_parser);
        //    try
        //    {

        //        //m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_evaluator_RequestHierarchy);
        //        m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_parser.parser_RequestHierarchy);

        //        //int count = m_parser.Evaluator.EvaluateSource(m_TemplateID, m_strQueryName, queryParameters, out error);
        //        //m_resultsCount = count;
        //        m_Result = m_parser.Evaluator.EvaluateSourceResult(m_TemplateID, m_strQueryName, queryParameters, out error);
        //        m_resultsCount = m_Result.GetCount();

        //        int pageNumber = -1;
        //        int pageSize = -1;
        //        int start = 0, end = m_resultsCount;

        //        if (m_pageNumberCmd != null && m_pageSizeCmd != null)
        //        {
        //            string strPgNum = m_pageNumberCmd.Execute(m_parser);
        //            string strPgSize = m_pageSizeCmd.Execute(m_parser);
        //            if (int.TryParse(strPgNum, out pageNumber))
        //                if (int.TryParse(strPgSize, out pageSize))
        //                {
        //                    start = (pageNumber - 1) * pageSize;
        //                    end = pageNumber * pageSize;
        //                }
        //        }

        //        if (start < 0)
        //            start = 0;
        //        m_context.SetQueryIndex(m_strQueryName, start);

        //        for (int i = start; i < end; i++)
        //        {
        //            if (i >= m_resultsCount)
        //                break;

        //            string output = base.Execute(m_parser);
        //            //if (output.StartsWith(Environment.NewLine))
        //            //    output = output.Substring(Environment.NewLine.Length);
        //            //if (output.EndsWith(Environment.NewLine))
        //            //    output = output.Substring(0, output.Length - Environment.NewLine.Length);

        //            //retVal += output;
        //            sbRetVal.Append(output);
        //            m_context.IncrementQIndex(m_strQueryName);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //retVal = "";
        //        Trace.WriteLine(ex);
        //        string parameters = "";
        //        Array queryParams = GetParamArray(m_parser);
        //        if (queryParameters != null && queryParameters.GetLength(0) != 0)
        //        {
        //            parameters = " with parameters (";
        //            for (int i = 0; i < queryParameters.Length; i++)
        //            {
        //                parameters += (string)queryParameters.GetValue(i);
        //                if (i != queryParameters.Length - 1)
        //                    parameters += ",";
        //            }
        //            parameters += ")";
        //        }

        //        throw new Exception("Error executing REP command at line " + m_parser.GetLine(m_nStartIndex).ToString() + " with query " + m_strQueryName + parameters + "; " + newLine + ex.Message);
        //    }
        //    //return retVal;
        //    return sbRetVal.ToString();

        //}
//        public SourceResult GetResult(CParser m_parser)
//        {
//            return (SourceResult)m_parser.Context.Results[this];
////            return m_Result;
//        }
        public Array GetParamArray(CParser m_parser, bool bDefQry)
        {
            Array arrParams = Array.CreateInstance(typeof(string), m_Parameters.GetLength(0) + 1);
            for (int i = 0; i < m_Parameters.GetLength(0); i++)
            {
                CCmd cmd = m_Parameters[i];
                string strValue = cmd.Execute(m_parser);
                if (strValue.ToUpper().Contains("#PAR."))
                    strValue = m_parser.Parameters2Values(strValue);
                arrParams.SetValue(strValue, i);
            }
            if (bDefQry)
                arrParams.SetValue("DEFQRY=1", m_Parameters.GetLength(0));
            else
                arrParams.SetValue("DEFQRY=0", m_Parameters.GetLength(0));
            return arrParams;
        }

	}
}