//using KubionLogNamespace;
using System;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CExportCmd : CCmd
    {
        int m_ConnID = -99;
        string m_Conn = "";
        string m_TemplateName = "";
        EncodeOption m_encodeOption = EncodeOption.None;

        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"EXPORT\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_TemplateName)); sJSON.Append("\"");
            sJSON.Append(",\"ID\":\""); sJSON.Append(Utils._JsonEscape(m_ConnID)); sJSON.Append("\"");
            sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(m_Conn)); sJSON.Append("\"");
            sJSON.Append(",\"Value2\":\""); sJSON.Append(Utils._JsonEscape(m_encodeOption.ToString ())); sJSON.Append("\"");
            sJSON.Append("}");
        }

 
        public int ConnID
        {
            get { return m_ConnID; }
            set { m_ConnID = value; }
        }
        public string Conn
        {
            get { return m_Conn; }
            set { m_Conn = value; }
        }
        public string TemplateName
        {
            get { return m_TemplateName; }
            set { m_TemplateName = value; }
        }
        public EncodeOption EncodeOption
        {
            get { return m_encodeOption; }
            set { m_encodeOption = value; }
        }

        public CExportCmd()
            : base()
        {
            m_enType = CommandType.EXPORTCommand ;
            this.m_bIsBlockCommand = false;
        }


        //public override string Execute(CParser m_parser)
        //{
        //    string l_TemplateName, l_Conn;
        //    l_TemplateName = m_parser.ReplaceParameters(TemplateName);
        //    l_Conn = m_parser.ReplaceParameters(Conn);

        //    string sResult;
        //    if (ConnID == -99)
        //        sResult = m_parser.Manager.ExportTemplate(l_Conn, l_TemplateName);
        //    else
        //        sResult = m_parser.Manager.ExportTemplate(ConnID, l_TemplateName);
        //    sResult = Utils.Encode(sResult, m_encodeOption); ;
        //    sResult = sResult.Replace("%d", "%d_export");
        //    sResult = sResult.Replace("%dash", "%dash_export");
        //    sResult = sResult.Replace("%macro", "%macro_export");
        //    sResult = sResult.Replace("%percent", "%percent_export");
        //    sResult = sResult.Replace("_percent", "_percent_export");
        //    return sResult;

        //}

    }
}

