
using System;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CUseParameters : CCmd
    {

        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"USEPARAMETERS\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append("}");
        }
        
        public CUseParameters()
            : base()
        {
            m_enType = CommandType.USEPARAMETERSCommand ;
            this.m_bIsBlockCommand = false;
        }


        //public override string Execute(CParser m_parser)
        //{
        //    m_parser.m_UseParameters =true ;
        //    return "";
        //}

    }
}

