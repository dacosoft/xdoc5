//using KubionLogNamespace;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
//using XDataSourceModule;

namespace XHTMLMerge
{
    [Serializable]
    public class CIFCmd : CCmd
	{
		protected CIFCmd m_ElseIfCmd = null;
		protected CmdCollection m_conditionCmds = null;
        protected string m_Conditions = "CmdCollection";
        protected string m_Name = "IF";

        public string Conditions
        {
            get { return m_Conditions; }
            set { m_Conditions = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public CmdCollection ConditionCmds
		{
			get { return m_conditionCmds; }
			set { m_conditionCmds = value; }
		}

		public CIFCmd ElseIfCommand
		{
			get { return m_ElseIfCmd; }
			set { m_ElseIfCmd = value; }
		}

		public CIFCmd():base()
		{
			m_bIsBlockCommand = true;
			m_enType = CommandType.IFCommand;
			m_conditionCmds = new CmdCollection();
		}

//        public override string Execute(CParser m_parser)
//        {
//			string condition = "";
            
//            try 
//            {
//                //DateTime dt1 = DateTime.Now;
//                if (m_Conditions == "CmdCollection")
//                {
//                    //string sJSON = "";
//                    //foreach (CCmd condCmd in this.m_conditionCmds)
//                    //    sJSON += condCmd.FakeExecute(m_parser);
//                    //m_parser.AddString (sJSON );
//                    foreach (CCmd condCmd in this.m_conditionCmds)
//                    {
//                        condition += condCmd.Execute(m_parser);
//                    }
//                }
//                else
//                    condition = m_Conditions;
//                //DateTime dt2 = DateTime.Now;
//                //m_parser.AddTimespan("IF_cond", dt2.Subtract(dt1).TotalMilliseconds);
//                condition = m_parser.ReplaceParameters(condition);
//                bool condResult = CParser.EvalCondition(condition, m_parser.DataProvider);
//                if (condResult == true)
//                    return base.Execute(m_parser);
//                else if (condResult == false && m_ElseIfCmd != null)
//                    return m_ElseIfCmd.Execute(m_parser);

//                return "";
//            }
//            catch (Exception ex)
//            {
//                Trace.WriteLine(ex);
////                throw new Exception("IF command at line " + m_parser.GetLine(StartIndex).ToString() + " cannot evaluate the condition " + condition + "; " + newLine + ex.Message);
//                throw new Exception(ex.Message);
//            }
//            finally
//            {
//                //if (connector != null)
//                //    connector.Dispose();
//            }
//		}
        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            if (m_Conditions == "CmdCollection")
            {
                sJSON.Append("{");
                sJSON.Append("\"Type\":\"IF\"");
                sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
                sJSON.Append(",\"Params\":[");
                bool isFirst = true;
                foreach (CCmd condCmd in this.m_conditionCmds)
                {
                    if (isFirst) isFirst = false; else sJSON.Append(",");
                    condCmd.GetJson(sJSON, sbWarning);
                }
                sJSON.Append("]");
                base.GetJsonChilds(sJSON, sbWarning);
                if (m_ElseIfCmd != null)
                {
                    sJSON.Append(",\"ElseIf\":");
                    m_ElseIfCmd.GetJson(sJSON, sbWarning);
                }
                sJSON.Append("}");
            }
            else
            {
                sJSON.Append("{");
                sJSON.Append("\"Type\":\"IF\"");
                sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
                sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_Name.ToString())); sJSON.Append("\"");
                sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_Conditions.ToString())); sJSON.Append("\"");
                base.GetJsonChilds(sJSON, sbWarning);
                if (m_ElseIfCmd != null)
                {
                    sJSON.Append(",\"ElseIf\":");
                    m_ElseIfCmd.GetJson(sJSON, sbWarning);
                }
                sJSON.Append("}");
            }
        }

	}
}
