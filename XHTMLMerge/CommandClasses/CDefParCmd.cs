////using KubionLogNamespace;
using System;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CDefParCmd : CCmd
	{
        protected string m_strParamName = "";
        protected string  m_strValue = "";
        protected bool m_isSet = false;
        protected string m_isParSource = "0";
        protected string m_ReplaceAllParams = "0";

        public string ParameterName
        {
            get { return m_strParamName; }
            set { m_strParamName = value; }
        }
        public string ParameterValue
        {
            get { return m_strValue; }
            set { m_strValue = value; }
        }
        public bool isSet
        {
            get { return m_isSet; }
            set { m_isSet = value; }
        }
        public string isParSource
        {
            get { return m_isParSource; }
            set { m_isParSource = value; }
        }
        public string ReplaceAllParams
        {
            get { return m_ReplaceAllParams; }
            set { m_ReplaceAllParams = value; }
        }

        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            if (m_isSet)
            {
                sJSON.Append("{");
                sJSON.Append("\"Type\":\"SPAR\"");
                sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
                sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_strParamName)); sJSON.Append("\"");
                sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_strValue)); sJSON.Append("\"");
                sJSON.Append(",\"Opt1\":\""); sJSON.Append(Utils._JsonEscape(m_isParSource)); sJSON.Append("\"");
                sJSON.Append(",\"Opt2\":\""); sJSON.Append(Utils._JsonEscape(m_ReplaceAllParams)); sJSON.Append("\"");
                sJSON.Append("}");
            }
            else
            {
                sJSON.Append("{");
                sJSON.Append("\"Type\":\"DEFPAR\"");
                sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
                sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_strParamName)); sJSON.Append("\"");
                sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_strValue)); sJSON.Append("\"");
                sJSON.Append("}");
            }
        }

        public CDefParCmd()
            : base()
		{
			m_enType = CommandType.DefParCmd;
			this.m_bIsBlockCommand = false;
		}

  //      public override string Execute(CParser m_parser)
  //      {
  //          string l_strParamName, l_strValue;
  //          l_strParamName = m_parser.ReplaceParameters(m_strParamName);
  //          if(m_ReplaceAllParams == "1") 
  //              l_strValue = m_parser.ReplaceParameters(m_strValue,true );
  //          else
  //              l_strValue = m_parser.ReplaceParameters(m_strValue);

  //          if (m_isParSource == "1") 
  //              l_strValue = (string)m_parser.TemplateParams[l_strValue];
  //          if (l_strParamName != "") m_parser.ParamDefaults[l_strParamName] = l_strValue;
  //          if(isSet)
  //              if (l_strParamName == "")
  //                  return l_strValue;
  //              else
  //              {
  //                  m_parser.TemplateParams[l_strParamName] = l_strValue;
  //                  return "";
  //              }

		//	return "";
		//}
        //string json = new WebClient().DownloadString("url");
        //string json = @"{""this"":{""is"":{""an"":{""example"":""this is the first value I want to return""}}}}";
        //dynamic jObj = JObject.Parse(json);
        //string example = jObj.@this.@is.an.example;
	}
}
