//using KubionLogNamespace;
using System;
using System.Text;


namespace XHTMLMerge
{
    [Serializable]
    public class CGETCFGPARCmd : CCmd
    {
        string m_ParName = "";
        string m_CfgClass = "";
        string m_CfgName = "";
        string m_CfgDef = "";

        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"CFG\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_ParName)); sJSON.Append("\"");
            sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(m_CfgClass)); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_CfgName)); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_CfgDef)); sJSON.Append("\"");
            sJSON.Append("}");
            sbWarning.Append("Command GETCFGPAR at line "); sbWarning.Append(Line.ToString()); sbWarning.Append(" is obsolete.\r\n");
        }

        
        public string ParName
        {
            get { return m_ParName; }
            set { m_ParName = value; }
        }
        public string CfgClass
        {
            get { return m_CfgClass; }
            set { m_CfgClass = value; }
        }
        public string CfgName
        {
            get { return m_CfgName; }
            set { m_CfgName = value; }
        }
        public string CfgDef
        {
            get { return m_CfgDef; }
            set { m_CfgDef = value; }
        }

        public CGETCFGPARCmd()
            : base()
        {
            m_enType = CommandType.GETCFGPARCommand;
            this.m_bIsBlockCommand = false;
        }


        //public override string Execute(CParser m_parser)
        //{
        //    string l_ParName, l_CfgClass, l_CfgName, l_CfgDef;

        //    l_ParName = m_parser.ReplaceParameters(m_ParName);
        //    l_CfgClass = m_parser.ReplaceParameters(m_CfgClass);
        //    l_CfgName = m_parser.ReplaceParameters(m_CfgName);
        //    l_CfgDef = m_parser.ReplaceParameters(m_CfgDef);

        //    string sPar = m_parser.GetCfg(l_CfgClass + "__" + l_CfgName, l_CfgDef);
        //    if (sPar == "missing_setting") sPar = l_CfgDef;
        //    m_parser.ParamDefaults[l_ParName] = sPar;
        //    m_parser.TemplateParams [l_ParName] = sPar;

        //    return "";
        //}

    }
}

