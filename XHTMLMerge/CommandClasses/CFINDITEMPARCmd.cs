using System;
using System.Collections.Generic;
using System.Text;
//using KubionLogNamespace;

namespace XHTMLMerge
{
    [Serializable]
    public class CFINDITEMPARCmd : CCmd
    {
        string m_ParName = "";
        string m_Val = "";
        string m_VarName = "";
        string m_ContextName = "";
        string m_ID = "ID";

        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"FINDITEMPAR\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_ParName)); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_Val)); sJSON.Append("\"");
            sJSON.Append(",\"VarName\":\""); sJSON.Append(Utils._JsonEscape(m_VarName)); sJSON.Append("\"");
            sJSON.Append(",\"ContextName\":\""); sJSON.Append(Utils._JsonEscape(m_ContextName)); sJSON.Append("\"");
            sJSON.Append(",\"ID\":\""); sJSON.Append(Utils._JsonEscape(m_ID)); sJSON.Append("\"");
            sJSON.Append("}");
            sbWarning.Append("Command FINDITEMPAR at line "); sbWarning.Append(Line.ToString()); sbWarning.Append(" is obsolete.\r\n");
        }

        public string ParName
        {
            get { return m_ParName; }
            set { m_ParName = value; }
        }
        public string Val
        {
            get { return m_Val; }
            set { m_Val = value; }
        }
        public string VarName
        {
            get { return m_VarName; }
            set { m_VarName = value; }
        }
        public string ContextName
        {
            get { return m_ContextName; }
            set { m_ContextName = value; }
        }
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public CFINDITEMPARCmd()
            : base()
        {
            m_enType = CommandType.FINDITEMPARCommand;
            this.m_bIsBlockCommand = false;
        }


        //public override string Execute(CParser m_parser)
        //{
        //    string l_VarName, l_ContextName, l_ID, l_Val, l_ParName;

        //    l_VarName = m_parser.ReplaceParameters(m_VarName);
        //    l_ContextName = m_parser.ReplaceParameters(m_ContextName);
        //    l_ID = m_parser.ReplaceParameters(m_ID);
        //    l_Val = m_parser.ReplaceParameters(m_Val);
        //    l_ParName = m_parser.ReplaceParameters(m_ParName);

        //    string sPar = m_parser.GetSV(l_VarName, l_ContextName, l_ID, "");
        //    if (sPar == "missing_setting") sPar = "";

        //    //sPar = sPar.Replace("\\|", "<tilda>");
        //    //string[] aData = sPar.Split('|');
        //    string[] aData =Utils.MySplit( sPar,'|');
        //    sPar = "-1";
        //    for (int iData = 0; iData < aData.Length ;iData++ )
        //    {
        //        //aData[iData] = aData[iData].Replace("<tilda>", "\\|");
        //        if (aData[iData] == l_Val)
        //        {
        //            sPar = (iData+1).ToString();
        //            break;
        //        }
        //        //aData[iData] = aData[iData].Replace("\\,", "<tilda>");
        //        //string[] aData1 = aData[iData].Split(',');
        //        string[] aData1 = Utils.MySplit(aData[iData],',');
        //        for (int iData1 = 0; iData1 < aData1.Length ; iData1++)
        //        {
        //            //aData[iData] = aData[iData].Replace("<tilda>", "\\,");
        //            if (aData1[iData1] == l_Val)
        //            {
        //                sPar = (iData+1).ToString() + "," + (iData1+1).ToString();
        //                break;
        //            }
        //        }

        //    }

        //    m_parser.ParamDefaults[l_ParName] = sPar;
        //    m_parser.TemplateParams[l_ParName] = sPar;

        //    return "";
        //}

    }
}

