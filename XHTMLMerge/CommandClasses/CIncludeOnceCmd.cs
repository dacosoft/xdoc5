using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
//using KubionLogNamespace;

namespace XHTMLMerge
{

    [Serializable]
    public class CIncludeOnceCmd : CCmd
    {
        protected string m_TemplateName = "";
        protected bool m_Ignore = true;
        const int MAX_INCLUDES = 150;

        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"INCLUDEONCE\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_TemplateName)); sJSON.Append("\"");
            sJSON.Append(",\"Opt1\":\""); sJSON.Append(Utils._JsonEscape(m_Ignore)); sJSON.Append("\"");
            sJSON.Append("}");
        }


        public string TemplateName
        {
            get { return m_TemplateName; }
            set { m_TemplateName = value; }
        }
        public bool Ignore
        {
            get { return m_Ignore; }
            set { m_Ignore = value; }
        }

        public CIncludeOnceCmd(): base()
        {
            m_bIsBlockCommand = false;
            m_enType = CommandType.IncludeCommand;
        }

//        public override string Execute(CParser m_parser)
//        {
//            string l_TemplateName = m_parser.ReplaceParameters(m_TemplateName);
//            bool IgnoreMissing = m_Ignore ;

//            //m_parser.TemplateParams["ERROR"] = "";
//            //m_parser.TemplateParams["ERRORVERBOSE"] = "";
//            if (m_parser.RequestIncludeOnce(l_TemplateName))
//            {
//                try
//                {

//                    try
//                    {

//                        m_parser.RequestTemplateIDForInclude(l_TemplateName);

//                    }
//                    catch (Exception)
//                    {
//                        if (IgnoreMissing)
//                        {
//                            string sError = "Template " + l_TemplateName + " not found";
//                            m_parser.TemplateParams["ERROR"] = sError;
//                            string sErrorVerbose = "Error executing INCLUDE command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sError;
//                            m_parser.TemplateParams["ERRORVERBOSE"] = sErrorVerbose;
//                            return "";
//                        }
//                        else
//                            throw new Exception("Could not retrieve template content for TemplateName=" + l_TemplateName);
//                    }

//                    if (m_parser.IDForInclude != -1)
//                    {
//                        m_parser.PreviousTemplateIDs.Add(m_parser.TemplateID);
//                        if (m_parser.PreviousTemplateIDs.Count > MAX_INCLUDES)
//                        {
//                            string message = "Template inclusions reached the maximum number of " + MAX_INCLUDES.ToString();
//                            throw new Exception(message);
//                        }
//                    }
//                    m_parser.RequestTemplate(m_parser.IDForInclude);
//                    if (m_parser.TextForInclude == null)
//                        if (IgnoreMissing)
//                        {
//                            string sError = "Template " + l_TemplateName + " not found";
//                            m_parser.TemplateParams["ERROR"] = sError;
//                            string sErrorVerbose = "Error executing INCLUDEONCE command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sError;
//                            m_parser.TemplateParams["ERRORVERBOSE"] = sErrorVerbose;
//                            return "";
//                        }
//                        else
//                            throw new Exception("Could not retrieve template content for TemplateID=" + m_parser.IDForInclude.ToString());

//                    Hashtable hashParams = CollectionsUtil.CreateCaseInsensitiveHashtable();
//                    if (m_parser.AdditionalIncludeParams != null)
//                        foreach (DictionaryEntry de in m_parser.AdditionalIncludeParams)
//                            hashParams[de.Key] = de.Value;
//                    hashParams["INCLUDE"] = 1;
//                    string sSessionID = "";
//                    object retVal = null;
//                    m_parser.Evaluator.GetValue("SessionID", out retVal);
//                    if (retVal != null) sSessionID = retVal.ToString();
//                    hashParams["SESSIONID"] = sSessionID;

//                    //CParser parser = new CParser(m_parser.Manager, m_parser.IDForInclude, m_parser.DataProvider, m_parser.TextForInclude, hashParams, m_parser.VirtualPath, m_parser.OutputPath);
//                    CParser parser = new CParser(m_parser.Manager, m_parser.IDForInclude, m_parser.Evaluator, m_parser.TextForInclude, hashParams, m_parser.VirtualPath, m_parser.OutputPath);
//                    parser.GlobalParams = m_parser.GlobalParams;
//                    parser.PreviousTemplateIDs = m_parser.PreviousTemplateIDs;
//                    parser.RequestTemplateText += new CParser.RequestTemplateText_Handler(m_parser.parser_RequestTemplateText);
//                    parser.RequestTemplateID += new CParser.RequestTemplateID_Handler(m_parser.parser_RequestTemplateID);
//                    parser.LogInclude += new CParser.LogInclude_Handler(m_parser.parser_LogInclude);
//                    parser.IncludeLogIndentation = m_parser.IncludeLogIndentation + 1;
//                    //parser.SessionVariables = m_parser.SessionVariables; 

//                    string returnVal = parser.Parse();
//                    return returnVal;
//                }
//                catch (Exception ex)
//                {
//                    string commandName = "INCLUDEONCE";
//                    string message = "Error executing " + commandName + " comand at line " + m_parser.GetLine(this.StartIndex).ToString() + " for TemplateName = " + l_TemplateName + ";" + Environment.NewLine + ex.Message;
//                    throw new Exception(message);
//                }
//                finally
//                {
//                    if (m_parser.PreviousTemplateIDs.Count > 0 &&
//                        Convert.ToInt32(m_parser.PreviousTemplateIDs[m_parser.PreviousTemplateIDs.Count - 1]) == m_parser.TemplateID)
////                        m_parser.PreviousTemplateIDs.Remove(m_parser.TemplateID);
////                        m_parser.PreviousTemplateIDs.Remove(m_parser.PreviousTemplateIDs[m_parser.PreviousTemplateIDs.Count - 1]);
//                        m_parser.PreviousTemplateIDs.RemoveAt(m_parser.PreviousTemplateIDs.Count - 1);
//                }
//            }
//            //if (m_parser.PreviousTemplateIDs.Count > 0 &&
//            //    Convert.ToInt32(m_parser.PreviousTemplateIDs[m_parser.PreviousTemplateIDs.Count - 1]) == m_parser.TemplateID)
//            //    m_parser.PreviousTemplateIDs.Remove(m_parser.TemplateID);
//            return "";
//        }

        //void parser_LogInclude(CParser sender, LogIncludeEventArgs e)
        //{
        //    l_parser.RequestLogInclude(e.IncludeParams, e.Indentation);
        //}

        //void parser_RequestTemplateID(CParser sender, string templateName)
        //{
        //    l_parser.RequestTemplateIDForInclude(templateName);
        //    sender.IDForInclude = l_parser.IDForInclude;
        //}

        //void parser_RequestTemplateText(CParser sender, int templateID)
        //{
        //    l_parser.RequestTemplate(templateID);
        //    sender.TextForInclude = l_parser.TextForInclude;
        //    if (l_parser.AdditionalIncludeParams != null)
        //        foreach (DictionaryEntry de in l_parser.AdditionalIncludeParams)
        //            sender.AdditionalIncludeParams[de.Key] = de.Value;
        //}
    }
}

