//using KubionLogNamespace;
using System;
using System.Collections.Generic;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CADDPARCmd : CCmd
    {
        string m_ParName = "";
        string m_Val = "";
        string m_VarName = "";

        public string ParName
        {
            get { return m_ParName; }
            set { m_ParName = value; }
        }
        public string Val
        {
            get { return m_Val; }
            set { m_Val = value; }
        }
        public string VarName
        {
            get { return m_VarName; }
            set { m_VarName = value; }
        }

        public CADDPARCmd()
            : base()
        {
            m_enType = CommandType.ADDPARCommand;
            this.m_bIsBlockCommand = false;
        }


        //public override string Execute(CParser m_parser)
        //{
        //    string l_VarName,   l_Val, l_ParName;

        //    l_VarName = m_parser.ReplaceParameters(m_VarName);
        //    l_Val = m_parser.ReplaceParameters(m_Val);
        //    l_ParName = m_parser.ReplaceParameters(m_ParName);

        //    string sPar = m_parser.GetParameterValue (l_VarName);
        //    try
        //    {
        //        int iPar = Convert.ToInt32(sPar);
        //        int iVal = Convert.ToInt32(l_Val);
        //        sPar = (iPar + iVal).ToString();                
        //    }
        //    catch(Exception ){}
        //    m_parser.ParamDefaults[l_ParName] = sPar;
        //    m_parser.TemplateParams [l_ParName] = sPar;

        //    return "";
        //}
        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"ADDPAR\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_ParName)); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_Val)); sJSON.Append("\"");
            sJSON.Append(",\"VarName\":\""); sJSON.Append(Utils._JsonEscape(m_VarName)); sJSON.Append("\"");
            sJSON.Append("}");
            sbWarning.Append("Command ADDPAR at line "); sbWarning.Append(Line.ToString()); sbWarning.Append(" is obsolete.\r\n");
        }

    }
}

