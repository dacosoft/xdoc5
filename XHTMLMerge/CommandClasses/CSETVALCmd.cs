////using KubionLogNamespace;
using System;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CSETVALCmd : CCmd
    {
        protected  string m_Val = "";
        protected string m_VarName = "";
        protected string m_ContextName = "";
        protected string m_ID = "ID";
        protected bool m_OnlyNull = false;
        protected bool m_Append = false;
        protected string m_AppendChar = "";
        
        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"SVSET\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_Val)); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_VarName)); sJSON.Append("\"");
            sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(m_ContextName)); sJSON.Append("\"");
            sJSON.Append(",\"ID\":\""); sJSON.Append(Utils._JsonEscape(m_ID)); sJSON.Append("\"");
            sJSON.Append(",\"Opt1\":\""); sJSON.Append(Utils._JsonEscape(m_OnlyNull)); sJSON.Append("\"");
            sJSON.Append(",\"Opt2\":\""); sJSON.Append(Utils._JsonEscape(m_Append)); sJSON.Append("\"");
            sJSON.Append(",\"Value2\":\""); sJSON.Append(Utils._JsonEscape(m_AppendChar)); sJSON.Append("\"");
            sJSON.Append("}");
        }

        public string Val
        {
            get { return m_Val; }
            set { m_Val = value; }
        }
        public bool OnlyNull
        {
            get { return m_OnlyNull; }
            set { m_OnlyNull = value; }
        }
        public bool Append
        {
            get { return m_Append; }
            set { m_Append = value; }
        }
        public string AppendChar
        {
            get { return m_AppendChar; }
            set { m_AppendChar = value; }
        }
        public string VarName
        {
            get { return m_VarName; }
            set { m_VarName = value; }
        }
        public string ContextName
        {
            get { return m_ContextName; }
            set { m_ContextName = value; }
        }
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public CSETVALCmd()
            : base()
        {
            m_enType = CommandType.SETVALCommand;
            this.m_bIsBlockCommand = false;
        }


        //public override string Execute(CParser m_parser)
        //{
        //    string l_VarName, l_ContextName, l_ID, l_Val;

        //    l_VarName = m_parser.ReplaceParameters(m_VarName);
        //    l_ContextName = m_parser.ReplaceParameters(m_ContextName);
        //    l_ID = m_parser.ReplaceParameters(m_ID);
        //    l_Val = m_parser.ReplaceParameters(m_Val);
        //    l_Val = Utils.Encode(l_Val, EncodeOption.XDOCDecode);

        //    if ((Append) || (OnlyNull))
        //    {
        //        string sPar = m_parser.GetSV(l_VarName, l_ContextName, l_ID, "");
        //        if ((OnlyNull) && (sPar != "")) return "";
        //        if (Append) l_Val = sPar + (sPar == "" ? "" : AppendChar) + l_Val;
        //    }
        //    m_parser.SetSV(l_VarName, l_ContextName, l_Val, l_ID);
        //    return "";
        //}

    }
}

