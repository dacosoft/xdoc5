﻿using System;
using System.Diagnostics;
using XDataSourceModule;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using KubionLogNamespace;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CJARRAYCmd : CCmd
    {
        # region Protected members
        // JDATA.[<par_name>].<_select_>.[<_conn_>].[<_error_>]
        protected string m_strParamName = "";
        protected string m_Source = "";
        protected string m_JPath = "";
        protected string m_ErrorParamName = "ERROR";
        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"JARRAY\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_strParamName)); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_Source)); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_JPath)); sJSON.Append("\"");
            sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(m_ErrorParamName)); sJSON.Append("\"");
            sJSON.Append("}");
        }
        #endregion Protected members

        #region Public properties

        public string ParameterName
        {
            get { return m_strParamName; }
            set { m_strParamName = value; }
        }
        public string Source
        {
            get { return m_Source; }
            set { m_Source = value; }
        }
        public string JPath
        {
            get { return m_JPath; }
            set { m_JPath = value; }
        }
        public string ErrorParamName
        {
            get { return m_ErrorParamName; }
            set { m_ErrorParamName = value; }
        }
        //protected string m_isParSource = "0";
        //public string isParSource
        //{
        //    get { return m_isParSource; }
        //    set { m_isParSource = value; }
        //}


        #endregion Public properties

        public CJARRAYCmd()
            : base()
        {
            this.m_enType = CommandType.JARRAYCommand ;
            m_bIsBlockCommand = false;
        }

        public override string Execute(CParser m_parser)
        {
            //JSON.Net 3.5
            //XmlNote myXmlNode = JsonConvert.DeserializeXmlNode(myJsonString);
            //// or .DeserilizeXmlNode(myJsonString, "root"); // if myJsonString does not have a root
            //string jsonString = JsonConvert.SerializeXmlNode(myXmlNode);

            //// To convert an XML node contained in string xml into a JSON string   
            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(xml);
            //string jsonText = JsonConvert.SerializeXmlNode(doc);

            //// To convert JSON text contained in string json into an XML node
            //XmlDocument doc = JsonConvert.DeserializeXmlNode(json);

            //#JDATA.QData.%SEL%.API_Data.Err#

            //if (sConnIDName == "0") sConnIDName = "";
            //if (sConnIDName == "-1") sConnIDName = "";


            //if (sConnIDName != "")
            //{
            //    string sQueryStatement = SQL.SQLConnectionString;
            //    sQueryStatement = sQueryStatement.Replace("#P1#", sConnIDName);
            //    DataTable dtQueryc = connAdm.GetDataTable(sQueryStatement);
            //    if (dtQueryc == null || dtQueryc.Rows.Count == 0)
            //    {
            //        Error = "Cannot find requested connection (" + sConnIDName + ") for template id = " + Convert.ToString(m_templateID);
            //        ErrorVerbose = Error;
            //        return false;
            //    }
            //    strConn = Connectors.GetStringResult(dtQueryc.Rows[0][S_CONNECTIONS.CONNSTRING]);
            //}
            //if ((strConn == "") || (strConn == ConfigurationManager.AppSettings["XDocConnectionString"]))
            //    providerData = connAdm;
            //else
            //    providerData = new Connectors().GetConnector(strConn);

            return "";
        }


    }
}