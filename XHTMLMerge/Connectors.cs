using KubionDataNamespace;
using Newtonsoft.Json.Linq;
////using KubionLogNamespace;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;


namespace XHTMLMerge
{
    public class XConnectors
    {
        Hashtable m_connectors = null;
        public Hashtable m_connStrings = null;
        public string jConnections = "";
        string m_connString = "";
        bool inTransaction = false;

        public XConnectors():this("","")
        {}
        public XConnectors(string connString, string sConnections)
        {
            m_connectors = CollectionsUtil.CreateCaseInsensitiveHashtable();// new Hashtable();
            m_connStrings = CollectionsUtil.CreateCaseInsensitiveHashtable(); //new Hashtable();
            m_connString = connString;
            if (m_connString=="") m_connString = ConfigurationManager.AppSettings["XDocConnectionString"];
            FillConnections(sConnections);
        }

        public void Dispose()
        {
            if (m_connectors != null)
            {
                foreach (DictionaryEntry myDE in m_connectors)
                    (myDE.Value as ClientData).Dispose();
                m_connectors.Clear();
                m_connectors = null;
                m_connStrings.Clear();
                m_connStrings = null;
            }
        }
        public static string L_String(string strString)
        {
            if (strString == null) return "";
            else return strString.Replace("'", "''");
        }
        public static string[] MySplit(string sVal, char cSeparator)
        {
            //sVal = sVal.Replace("\\\\", "<_backslash_>");
            sVal = sVal.Replace("\\" + cSeparator.ToString(), "<_tilda_>");
            string[] aData = sVal.Split(cSeparator);
            for (int i = 0; i < aData.Length; i++)
            {
                aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString());
                //aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString()).Replace("<_backslash_>", "\\\\");
                //if (aData[i].EndsWith("\\\\")) 
                //    aData[i] = aData[i].Substring(0, aData[i].Length - 1);
            }
            return aData;
        }

        private void FillConnections(string sConnections)
        {
            if (m_connString != null)
            {
                m_connStrings.Add("", m_connString);
                m_connStrings.Add("#", m_connString);
                m_connStrings.Add("#0", m_connString);
                m_connStrings.Add("#-1", m_connString);
            }

            if (sConnections != "")
            {
                JToken jt = (JObject.Parse(sConnections)).SelectToken("sConnections");
                if (jt is JArray)
                {
                    JArray ja = (JArray)jt;
                    for (int i = 0; i < ja.Count; i++)
                    {
                        JToken j = ja[i];
                        string sConnID = "";
                        string sConnName = "";
                        string sConnString = "";
                        string sConnData = "";
                        foreach (JProperty jp in j.Children<JProperty>())
                        {
                            if (jp.Name == "ConnID") sConnID = jp.Value.ToString();
                            if (jp.Name == "ConnName") sConnName = jp.Value.ToString();
                            if (jp.Name == "ConnString") sConnString = jp.Value.ToString();
                            if (jp.Name == "ConnData") sConnData = jp.Value.ToString();
                        }
                        if (sConnData != "")
                        {
                            string sKey = "Kubion" + sConnName + "Chessm@sterChessm@ster";
                            sKey = sKey.Substring(3, 24);
                            sConnString = Decrypt(sConnData, sKey);
                        }
                        if (sConnString != "")
                        {
                            try
                            {
                                if (m_connStrings.Contains(sConnName)) m_connStrings.Remove(sConnName);
                                m_connStrings.Add(sConnName, sConnString);
                                if (m_connStrings.Contains("#" + sConnID)) m_connStrings.Remove("#" + sConnID);
                                m_connStrings.Add("#" + sConnID, sConnString);
                            }
                            catch (Exception) { };
                        }
                    }
                }
            }
            else if (SystemConnector != null)
            {
                string sConfigVariablesFile = System.AppDomain.CurrentDomain.BaseDirectory + "S_ConfigVariables.json";
                string sConnectionsFile = System.AppDomain.CurrentDomain.BaseDirectory + "S_Connections.json";
                string sSQL1 = "SELECT ConfigClass,ConfigName,Value FROM [S_ConfigVariables] ";
                DataTable dt1 = SystemConnector.GetDataTable(sSQL1);

                string sConfigVariables = Newtonsoft.Json.JsonConvert.SerializeObject(dt1);
                if (!File.Exists(sConfigVariablesFile))
                    File.WriteAllText(sConfigVariablesFile, "{\"sConfigVariables\":" + sConfigVariables + "}");

                string sSQL = "SELECT * FROM S_CONNECTIONS ";
                DataTable dt = SystemConnector.GetDataTable(sSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        //     [ConnID]      ,[ConnName]      ,[ConnString]
                        string sConnID = ClientData.GetStringResult(row["ConnID"], "");
                        string sConnName = ClientData.GetStringResult(row["ConnName"], "");
                        string sConnString = ClientData.GetStringResult(row["ConnString"], "");
                        m_connStrings.Add(sConnName, sConnString);
                        m_connStrings.Add("#" + sConnID, sConnString);
                    }
                    try
                    {
                        jConnections = Newtonsoft.Json.JsonConvert.SerializeObject(dt);
                        if (!File.Exists(sConnectionsFile))
                            File.WriteAllText(sConnectionsFile, "{\"sConnections\":" + jConnections + "}");
                    }
                    catch (Exception) { };
                }
            }
            //            {"ConnID":7,"ConnName":"HTTP","ConnString":"Provider=HTTP;URI=http://;Session=0"},
            if (m_connStrings["HTTP"] == null)
                m_connStrings.Add("HTTP", "Provider=HTTP;URI=http://;Session=0");
            else
                m_connStrings["HTTP"] = "Provider=HTTP;URI=http://;Session=0";
            if (m_connStrings["HTTP1"] == null)
                m_connStrings.Add("HTTP1", "Provider=HTTP;URI=http://;Session=1");
            else
                m_connStrings["HTTP1"] = "Provider=HTTP;URI=http://;Session=1";
            if (m_connStrings["HTTPS"] == null)
                m_connStrings.Add("HTTPS", "Provider=HTTP;URI=https://;Session=0");
            else
                m_connStrings["HTTPS"] = "Provider=HTTP;URI=https://;Session=0";
            if (m_connStrings["HTTPS1"] == null)
                m_connStrings.Add("HTTPS1", "Provider=HTTP;URI=https://;Session=1");
            else
                m_connStrings["HTTPS1"] = "Provider=HTTP;URI=https://;Session=1";
            if (m_connStrings["SQLite"] == null)
                m_connStrings.Add("SQLite", "Provider=SQLite;Data Source=:memory:");
            else
                m_connStrings["SQLite"] = "Provider=SQLite;Data Source=:memory:";
            if (m_connStrings["SQLite3600"] == null)
                m_connStrings.Add("SQLite3600", "Provider=SQLite;Data Source=:memory:||3600");
            else
                m_connStrings["SQLite3600"] = "Provider=SQLite;Data Source=:memory:||3600";

        }

        //{
		//	"ConnID": 1,
		//	"ConnName": "SQLite",
		//	"ConnString": "Provider=SQLite;Data Source=:memory:"
		//},
        //        /* 
        //         * old FILL connections
        //        private string GetConfigPath(string sName, string sDefault)
        //        {//GetFullPath, GetPath, GetFullFilePath, GetConfigPath
        //            string sPath = ConfigurationManager.AppSettings[sName];
        //            if (sPath == null) sPath = sDefault;
        //            if (sPath == null) return "";
        //            if (HttpContext.Current != null)
        //            {
        //                if (sPath.StartsWith("~"))
        //                    sPath = HttpContext.Current.Server.MapPath("~") + sPath.Substring(1);
        //                else
        //                    if (!Path.IsPathRooted(sPath))
        //                        if ((HttpContext.Current != null) && (HttpContext.Current.Server != null)) sPath = HttpContext.Current.Server.MapPath(Path.Combine("~", sPath));
        //            }
        //            else
        //            {
        //                if (sPath.StartsWith("~"))
        //                    sPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + sPath.Substring(1);
        //                else
        //                    if (!Path.IsPathRooted(sPath))
        //                        sPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, sPath);
        //            }

        //            //if (!Directory.Exists(sPath))
        //            //    Directory.CreateDirectory(sPath);
        //            if (!sPath.EndsWith("\\")) sPath += "\\";
        //            return sPath;
        //        }
        //        private string GetConfigPath2(string sName, string sDefault)
        //        {//GetFullPath, GetPath, GetFullFilePath, GetConfigPath
        //            string sPath = ConfigurationManager.AppSettings[sName];
        //            if (sPath == null) sPath = sDefault;
        //            if (sPath == null) return "";
        //            if (HttpContext.Current != null)
        //            {
        //                if (sPath.StartsWith("~"))
        //                    sPath = HttpContext.Current.Server.MapPath("~") + sPath.Substring(1);
        //                else
        //                    if (!Path.IsPathRooted(sPath))
        //                        if ((HttpContext.Current != null) && (HttpContext.Current.Server != null)) sPath = HttpContext.Current.Server.MapPath(Path.Combine("~", sPath));
        //            }
        //            //if (!Directory.Exists(sPath))
        //            //    Directory.CreateDirectory(sPath);
        //            if (!sPath.EndsWith("\\")) sPath += "\\";
        //            return sPath;
        //        }
        //        private string GetConfigPath_old1(string sName, string sDefault)
        //        {//GetFullPath, GetPath, GetFullFilePath, GetConfigPath
        //            string sPath = ConfigurationManager.AppSettings[sName];
        //            if (sPath == null) sPath = sDefault;
        //            if (sPath != null)
        //            {
        //                if (sPath.StartsWith("~"))
        //                    sPath = HttpContext.Current.Server.MapPath("~") + sPath.Substring(1);
        //                else
        //                    if (!Path.IsPathRooted(sPath))
        //                        if ((HttpContext.Current != null) && (HttpContext.Current.Server != null)) sPath = HttpContext.Current.Server.MapPath(Path.Combine("~", sPath));
        //                if (!Directory.Exists(sPath))
        //                    Directory.CreateDirectory(sPath);
        //                if (!sPath.EndsWith("\\")) sPath += "\\";
        //                return sPath;
        //            }
        //            return "";
        //        }

        //        //private string GetXDocPath()
        //        //{
        //        //    string sPath = ConfigurationManager.AppSettings["XDoc"];
        //        //    if (sPath == null) sPath = "";
        //        //    else
        //        //    {
        //        //        if (sPath.StartsWith("~"))
        //        //            sPath = HttpContext.Current.Server.MapPath("~") + sPath.Substring(1);
        //        //        else
        //        //            if (!Path.IsPathRooted(sPath))
        //        //                if ((HttpContext.Current != null) && (HttpContext.Current.Server != null)) sPath = HttpContext.Current.Server.MapPath(Path.Combine("~", sPath));
        //        //        if (!Directory.Exists(sPath))
        //        //            Directory.CreateDirectory(sPath);
        //        //        if (!sPath.EndsWith("\\")) sPath += "\\";
        //        //    }
        //        //    return sPath;
        //        //}
        //        //private string GetJsonPath()
        //        //{
        //        //    string sPath = ConfigurationManager.AppSettings["Json"];
        //        //    if (sPath == null) 
        //        //        sPath = "";
        //        //    else
        //        //    {
        //        //        if (sPath.StartsWith("~"))
        //        //            sPath = HttpContext.Current.Server.MapPath("~") + sPath.Substring(1);
        //        //        else
        //        //            if (!Path.IsPathRooted(sPath))
        //        //                if ((HttpContext.Current != null) && (HttpContext.Current.Server != null)) sPath = HttpContext.Current.Server.MapPath(Path.Combine("~", sPath));
        //        //        if (!Directory.Exists(sPath))
        //        //            Directory.CreateDirectory(sPath);
        //        //        if (!sPath.EndsWith("\\")) sPath += "\\";
        //        //    }
        //        //    return sPath;
        //        //}
        //        private void FillConnections()
        //        {
        //            if (m_connString != null)
        //            {
        //                m_connStrings.Add("", m_connString);
        //                m_connStrings.Add("#", m_connString);
        //                m_connStrings.Add("#0", m_connString);
        //                m_connStrings.Add("#-1", m_connString);
        //            }
        //            //            templateWarning = File.ReadAllText(m_XDocPath + templateName + "_warning.txt");
        //            //LogEventLog("FillConnections");
        //            string m_XDocPath = GetConfigPath("XDoc", "XDoc");
        //            //LogEventLog("XDoc:" + m_XDocPath);
        //            string m_JsonPath = GetConfigPath("XFiles", "");
        //            //LogEventLog("XFiles:" + m_JsonPath);
        //            string sConnectionsFile = m_XDocPath + "S_Connections.json";
        //            string sConnectionsFile1 = m_JsonPath + "S_Connections.json";

        //            string sConnections = "";
        //            if (File.Exists(sConnectionsFile1))
        //                sConnections = File.ReadAllText(sConnectionsFile1);
        //            else if (File.Exists(sConnectionsFile))
        //                sConnections = File.ReadAllText(sConnectionsFile);
        //            //LogEventLog("sConnections:" + sConnections);

        //            if (sConnections != "")
        //            {
        //                JToken jt = (JObject.Parse(sConnections)).SelectToken("sConnections");
        //                if (jt is JArray)
        //                {
        //                    JArray ja = (JArray)jt;
        //                    for (int i = 0; i < ja.Count; i++)
        //                    {
        //                        JToken j = ja[i];
        //                        string sConnID = "";
        //                        string sConnName = "";
        //                        string sConnString = "";
        //                        string sConnData = "";
        //                        foreach (JProperty jp in j.Children<JProperty>())
        //                        {
        //                            if (jp.Name == "ConnID") sConnID = jp.Value.ToString();
        //                            if (jp.Name == "ConnName") sConnName = jp.Value.ToString();
        //                            if (jp.Name == "ConnString") sConnString = jp.Value.ToString();
        //                            if (jp.Name == "ConnData") sConnData = jp.Value.ToString();
        //                        }
        //                        if (sConnData != "")
        //                        {
        //                            string sKey = "Kubion" + sConnName + "Chessm@sterChessm@ster";
        //                            sKey = sKey.Substring(3, 24);
        //                            sConnString = Decrypt(sConnData, sKey);
        //                        }
        //                        if (sConnString != "")
        //                        {
        //                            try
        //                            {
        //                                if (m_connStrings.Contains(sConnName)) m_connStrings.Remove(sConnName);
        //                                m_connStrings.Add(sConnName, sConnString);
        //                                if (m_connStrings.Contains("#" + sConnID)) m_connStrings.Remove("#" + sConnID);
        //                                m_connStrings.Add("#" + sConnID, sConnString);
        //                            }
        //                            catch (Exception) { };
        //                        }
        //                    }
        //                }
        //            }
        //            else if (SystemConnector != null)
        //            {
        //                string sConfigVariablesFile = m_JsonPath + "S_ConfigVariables.json";
        //                string sSQL1 = "SELECT ConfigClass,ConfigName,Value FROM [S_ConfigVariables] ";
        //                DataTable dt1 = SystemConnector.GetDataTable(sSQL1);

        //                string sConfigVariables = Newtonsoft.Json.JsonConvert.SerializeObject(dt1);
        //                File.WriteAllText(sConfigVariablesFile, "{\"sConfigVariables\":" + sConfigVariables + "}");

        //                string sSQL = "SELECT * FROM S_CONNECTIONS ";
        //                DataTable dt = SystemConnector.GetDataTable(sSQL);
        //                if (dt != null && dt.Rows.Count > 0)
        //                {
        //                    foreach (DataRow row in dt.Rows)
        //                    {
        //                        //     [ConnID]      ,[ConnName]      ,[ConnString]
        //                        string sConnID = ClientData.GetStringResult(row["ConnID"], "");
        //                        string sConnName = ClientData.GetStringResult(row["ConnName"], "");
        //                        string sConnString = ClientData.GetStringResult(row["ConnString"], "");
        //                        m_connStrings.Add(sConnName, sConnString);
        //                        m_connStrings.Add("#" + sConnID, sConnString);
        //                    }
        //                    try
        //                    {
        //                        sConnections = Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        //                        File.WriteAllText(sConnectionsFile1, "{\"sConnections\":" + sConnections + "}");
        //                    }
        //                    catch (Exception) { };
        //                }
        //            }
        ////            {"ConnID":7,"ConnName":"HTTP","ConnString":"Provider=HTTP;URI=http://;Session=0"},
        //            if (m_connStrings["HTTP"] == null)
        //                m_connStrings.Add("HTTP", "Provider=HTTP;URI=http://;Session=0");
        //            else
        //                m_connStrings["HTTP"] = "Provider=HTTP;URI=http://;Session=0";
        //            if (m_connStrings["HTTP1"] == null)
        //                m_connStrings.Add("HTTP1", "Provider=HTTP;URI=http://;Session=1");
        //            else
        //                m_connStrings["HTTP1"] = "Provider=HTTP;URI=http://;Session=1";

        //        }

        //        //private void FillConnections_orig()
        //        //{
        //        //    if (m_connString != null)
        //        //    {
        //        //        m_connStrings.Add("", m_connString);
        //        //        m_connStrings.Add("#", m_connString);
        //        //        m_connStrings.Add("#0", m_connString);
        //        //        m_connStrings.Add("#-1", m_connString);
        //        //    }
        //        //    //            templateWarning = File.ReadAllText(m_XDocPath + templateName + "_warning.txt");
        //        //    string m_XDocPath = GetXDocPath();
        //        //    string sConnectionsFile = m_XDocPath + "S_Connections.json";

        //        //    if (m_XDocPath != "")
        //        //    {
        //        //        string sConnections = "";
        //        //        // Directory.GetCurrentDirectory();
        //        //        if (File.Exists(sConnectionsFile))
        //        //        {
        //        //            sConnections = File.ReadAllText(sConnectionsFile);
        //        //            JToken jt = (JObject.Parse(sConnections)).SelectToken("sConnections");
        //        //            if (jt is JArray)
        //        //            {
        //        //                JArray ja = (JArray)jt;
        //        //                for (int i = 0; i < ja.Count; i++)
        //        //                {
        //        //                    JToken j = ja[i];
        //        //                    string sConnID = "";
        //        //                    string sConnName = "";
        //        //                    string sConnString = "";
        //        //                    foreach (JProperty jp in j.Children<JProperty>())
        //        //                    {
        //        //                        if (jp.Name == "ConnID") sConnID = jp.Value.ToString();
        //        //                        if (jp.Name == "ConnName") sConnName = jp.Value.ToString();
        //        //                        if (jp.Name == "ConnString") sConnString = jp.Value.ToString();

        //        //                    }
        //        //                    if (sConnString != "")
        //        //                    {
        //        //                        try
        //        //                        {
        //        //                            m_connStrings.Add(sConnName, sConnString);
        //        //                            m_connStrings.Add("#" + sConnID, sConnString);
        //        //                        }
        //        //                        catch(Exception){};
        //        //                    }
        //        //                }
        //        //            }
        //        //        }
        //        //        else if (SystemConnector!=null)
        //        //        {
        //        //            string sConfigVariablesFile = m_XDocPath + "S_ConfigVariables.json";
        //        //            string sSQL1 = "SELECT ConfigClass,ConfigName,Value FROM [S_ConfigVariables] ";
        //        //            DataTable dt1 = SystemConnector.GetDataTable(sSQL1);

        //        //            string sConfigVariables = Newtonsoft.Json.JsonConvert.SerializeObject(dt1);
        //        //            File.WriteAllText(sConfigVariablesFile, "{\"sConfigVariables\":" + sConfigVariables + "}");

        //        //            string sSQL = "SELECT * FROM S_CONNECTIONS ";
        //        //            DataTable dt = SystemConnector.GetDataTable(sSQL);
        //        //            if (dt != null && dt.Rows.Count > 0)
        //        //            {
        //        //                foreach (DataRow row in dt.Rows)
        //        //                {
        //        //                    //     [ConnID]      ,[ConnName]      ,[ConnString]
        //        //                    string sConnID = ClientData.GetStringResult(row["ConnID"], "");
        //        //                    string sConnName = ClientData.GetStringResult(row["ConnName"], "");
        //        //                    string sConnString = ClientData.GetStringResult(row["ConnString"], "");
        //        //                    m_connStrings.Add(sConnName, sConnString);
        //        //                    m_connStrings.Add("#" + sConnID, sConnString);
        //        //                }
        //        //                sConnections = Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        //        //                File.WriteAllText(sConnectionsFile, "{\"sConnections\":" + sConnections + "}");
        //        //            }
        //        //        }
        //        //    }
        //        //    else
        //        //    {
        //        //        string sSQL = "SELECT * FROM S_CONNECTIONS ";
        //        //        DataTable dt = SystemConnector.GetDataTable(sSQL);
        //        //        if (dt != null && dt.Rows.Count > 0)
        //        //        {
        //        //            foreach (DataRow row in dt.Rows)
        //        //            {
        //        //                //     [ConnID]      ,[ConnName]      ,[ConnString]
        //        //                string sConnID = ClientData.GetStringResult(row["ConnID"], "");
        //        //                string sConnName = ClientData.GetStringResult(row["ConnName"], "");
        //        //                string sConnString = ClientData.GetStringResult(row["ConnString"], "");
        //        //                m_connStrings.Add(sConnName, sConnString);
        //        //                m_connStrings.Add("#" + sConnID, sConnString);
        //        //            }
        //        //        }
        //        //    }
        //        //}
        //        */

        public ClientData SystemConnector
        {
            get { return GetConnector(""); }
        }
        public static string Encrypt(string input, string key)
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        public static string Decrypt(string input, string key)
        {
            try
            {
                byte[] inputArray = Convert.FromBase64String(input);
                TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
                tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
                tripleDES.Mode = CipherMode.ECB;
                tripleDES.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = tripleDES.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
                tripleDES.Clear();
                return UTF8Encoding.UTF8.GetString(resultArray);
            }
            catch (Exception e) {
                return "";
            };
        }

        public ClientData GetConnector(string connName)
        {
            if (connName == "" && m_connString == "")
            {
                string m_strLicense, m_strConnStringEncrypted;
                m_strLicense = ConfigurationManager.AppSettings["License"];
                m_strConnStringEncrypted = ConfigurationManager.AppSettings["XDocuments"];
                if (m_strLicense != null && m_strLicense != "" && m_strConnStringEncrypted != null && m_strConnStringEncrypted != "")
                {
                    string sKey = "Kubion" + m_strLicense + "Chessm@sterChessm@ster";
                    sKey = sKey.Substring(3, 24);
                    m_connString = Decrypt(m_strConnStringEncrypted, sKey);
                }
                else
                {
                    m_connString = ConfigurationManager.AppSettings["XDocConnectionString"];
                    if (m_connString == null || m_connString == "")
                        throw new Exception("The XDocuments connection string is not present in the config file");
                }
            }
            if (m_connectors.Contains(connName))
                return (ClientData)m_connectors[connName];
            else
            {
                string m_strConnString = "";
                if (m_connStrings.Contains(connName))
                    m_strConnString = (string)m_connStrings[connName];
                if (m_connStrings.Contains("#"+connName))
                    m_strConnString = (string)m_connStrings["#"+connName];
                if (m_strConnString != "")
                {
                    ClientData pd = new ClientData(m_strConnString);
                    if (inTransaction)
                        pd.BeginTransaction();
                    m_connectors.Add(connName, pd);
                    return pd;
                }
            }
            return null;
        }

        public bool InTransaction
        {
            get { return inTransaction; }
        }
        public void BeginTransaction()
        {
            if (!inTransaction)
            {
                if (m_connectors != null)
                    foreach (DictionaryEntry myDE in m_connectors)
                        (myDE.Value as ClientData).BeginTransaction();

                inTransaction = true;
            }
        }
        public void Commit()
        {
            if (inTransaction)
            {
                if (m_connectors != null)
                    foreach (DictionaryEntry myDE in m_connectors)
                        (myDE.Value as ClientData).CommitTransaction();

                inTransaction = false;
            }
        }
        public void Rollback()
        {
            if (inTransaction)
            {
                if (m_connectors != null)
                    foreach (DictionaryEntry myDE in m_connectors)
                        (myDE.Value as ClientData).RollbackTransaction();
                inTransaction = false;
            }
        }

        //#region GetResult

        //public static bool GetBoolResult(object obj)
        //{
        //    return GetBoolResult(obj, false);
        //}
        //public static bool GetBoolResult(object obj, bool defaultValue)
        //{
        //    int i;
        //    //if (obj != null && obj is bool)
        //    //    return (bool)obj;
        //    if (obj != null && bool.TryParse(obj.ToString(), out defaultValue))
        //        return defaultValue;
        //    if (obj != null && Int32.TryParse(obj.ToString(), out i))
        //        return (i != 0);

        //    return defaultValue;
        //}

        //public static string GetStringResult(object obj, string defaultValue)
        //{
        //    if (obj != null)
        //        if (obj is string)
        //            return ((string)obj).TrimEnd();
        //        else
        //            return obj.ToString();
        //    return defaultValue;
        //}
        //public static string GetStringResult(object obj)
        //{
        //    return GetStringResult(obj, "");
        //}

        //public static int GetInt32Result(object obj, int defaultValue)
        //{
        //    //            if (obj != null && obj is int)
        //    //            return (int)obj;
        //    if (obj != null && Int32.TryParse(obj.ToString(), out defaultValue))
        //        return defaultValue;

        //    return defaultValue;
        //}

        //public static int GetInt32Result(object obj)
        //{
        //    return GetInt32Result(obj, -1);
        //}

        //public static double GetDoubleResult(object obj, double defaultValue)
        //{
        //    if (obj != null && obj is double)
        //        return (double)obj;
        //    return defaultValue;
        //}

        //public static double GetDoubleResult(object obj)
        //{
        //    return GetDoubleResult(obj, -1);
        //}

        //public static DateTime GetDateTimeResult(object obj, DateTime defaultValue)
        //{
        //    if (obj != null && obj is DateTime)
        //        return (DateTime)obj;
        //    return defaultValue;
        //}
        //public static DateTime GetDateTimeResult(object obj)
        //{
        //    return GetDateTimeResult(obj, (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
        //}

        //public static byte[] GetBinaryResult(object obj)
        //{
        //    if (obj != null && obj != DBNull.Value && obj is byte[])
        //        return (byte[])obj;
        //    return null;
        //}

        //#endregion GetResult

    }

}
