﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KubionDataNamespace
{
    class LDAPData : IData
    {
        private DirectorySearcher dirSearch = null;

        public LDAPData(string sConnectionString)
        {
            string domain = "";
            string username = "";
            string password = "";
            if (dirSearch == null)
            {
                try
                {
                    domain = GetConnStringValue(sConnectionString, "domain");
                    username = GetConnStringValue(sConnectionString, "username");
                    password = GetConnStringValue(sConnectionString, "password");
                    dirSearch = new DirectorySearcher(
                        new DirectoryEntry("LDAP://" + domain, username, password));
                }
                catch (DirectoryServicesCOMException e)
                {
                    throw new Exception(e.Message.ToString());
                }
            }
            //try
            //{
            //    strUrl = ParseConnString(sConnectionString);
            //    if (GetConnStringValue(sConnectionString, "Session") == "1")
            //        cookieContainer = new CookieContainer();

            //}
            //catch (Exception ex)
            //{
            //    //////KubionLog.WriteLine(ex);
            //    throw;
            //}
        }
        private string GetConnStringValue(string strURI, string strKey)
        {
            int intStart = (";" + strURI).IndexOf(";" + strKey + "=", StringComparison.CurrentCultureIgnoreCase);
            if (intStart == -1) return "";
            intStart += (strKey + "=").Length;
            int intEnd = (strURI + ";").IndexOf(";", intStart);
            return (strURI + ";").Substring(intStart, (intEnd - intStart));
        }

        public string GetResponse(string strCommandUrl)
        {
            string strCookies = "";
            return GetResponse(strCommandUrl, ref strCookies);
        }
        public static string _JsonEscape(string aText)
        {
            //“ Is iets anders dan “  « » “ ”
            char[] escapeChars = new char[] { '\\', '\"', '“', '”', '\n', '\r', '\t', '\b', '\f' };
            StringBuilder s = new StringBuilder();
            if (aText != null)
            {
                int j = 0;
                int i = aText.IndexOfAny(escapeChars);
                while (i != -1)
                {
                    if (i != j) s.Append(aText.Substring(j, i - j));
                    switch (aText[i])
                    {
                        case '\\': s.Append("\\\\"); break;
                        case '\"': s.Append("\\\""); break;
                        case '“': s.Append("\\\""); break;
                        case '”': s.Append("\\\""); break;
                        case '\n': s.Append("\\n"); break;
                        case '\r': s.Append("\\r"); break;
                        case '\t': s.Append("\\t"); break;
                        case '\b': s.Append("\\b"); break;
                        case '\f': s.Append("\\f"); break;
                    }
                    j = i + 1;
                    i = aText.IndexOfAny(escapeChars, j);
                }
                s.Append(aText.Substring(j));
            }
            return s.ToString();
        }

        public string GetResponse(string strCommandUrl, ref string strCookies)
        {
            StringBuilder sb = new StringBuilder();
            bool isFirstRes = true;
            bool isFirstProp = true;
            string filter = strCommandUrl;

            sb.Append("[");
            if (dirSearch != null)
            {
                //ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(samaccountname=" + username + "))";
                //ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(mail=" + email + "))";
                //dirSearch.Filter = "(&((&(objectCategory=Person)(objectClass=User))))";
                //            LDAP://OU=Stadgenoot,OU=Organisatie,DC=connect,DC=local
                dirSearch.Filter = filter;
                dirSearch.SearchScope = SearchScope.Subtree;
                dirSearch.ServerTimeLimit = TimeSpan.FromSeconds(90);

                SearchResultCollection resultCol = dirSearch.FindAll();
                isFirstRes = true;
                foreach (SearchResult rs in resultCol)
                {
                    if (isFirstRes) isFirstRes = false;
                    else sb.Append(",");
                    sb.Append("{");
                    ResultPropertyCollection rpvc = rs.Properties;
                    //if rpvc.Contains("distinguishedname")
                    isFirstProp = true;
                    foreach (string key in rpvc.PropertyNames)
                    {
                        if (rpvc[key][0].ToString() != "System.Byte[]")
                        {
                            if (isFirstProp) isFirstProp = false;
                            else sb.Append(",");
                            sb.Append("\""); sb.Append(_JsonEscape(key)); sb.Append("\":\""); sb.Append(_JsonEscape(rpvc[key][0].ToString())); sb.Append("\"");
                        }
                    }
                    sb.Append("}");
                }
            }
            sb.Append("]");
            return sb.ToString();
        }

        // public string GetResponse(string strCommandUrl, ref string strCookies)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    string domain = "";
        //    string username = "";
        //    string password = "";
        //    string filter = "(&((&(objectCategory=Person)(objectClass=User))))";
        //    sb.Append("[");
        //    bool isFirstRes = true;
        //    bool isFirstProp = true;
        //    JObject jo = null;
        //    jo = JObject.Parse(strCommandUrl);
        //    if (jo.GetValue("domain") != null) domain = jo.GetValue("domain").ToString();
        //    if (jo.GetValue("username") != null) username = jo.GetValue("username").ToString();
        //    if (jo.GetValue("password") != null) password = jo.GetValue("password").ToString();
        //    if (jo.GetValue("filter") != null) filter = jo.GetValue("filter").ToString();

        //    if (dirSearch == null)
        //    {
        //        try
        //        {
        //            dirSearch = new DirectorySearcher(
        //                new DirectoryEntry("LDAP://" + domain, username, password));
        //        }
        //        catch (DirectoryServicesCOMException e)
        //        {
        //            throw new Exception(e.Message.ToString());
        //        }
        //    }
        //    if (dirSearch != null)
        //    {
        //        //ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(samaccountname=" + username + "))";
        //        //ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(mail=" + email + "))";
        //        //dirSearch.Filter = "(&((&(objectCategory=Person)(objectClass=User))))";
        //        //            LDAP://OU=Stadgenoot,OU=Organisatie,DC=connect,DC=local
        //        dirSearch.Filter = filter;

        //        dirSearch.SearchScope = SearchScope.Subtree;
        //        dirSearch.ServerTimeLimit = TimeSpan.FromSeconds(90);

        //        SearchResultCollection resultCol = dirSearch.FindAll();
        //        isFirstRes = true;
        //        foreach (SearchResult rs in resultCol)
        //        {
        //            if (isFirstRes) isFirstRes = false;
        //            else sb.Append(",");
        //            sb.Append("{");
        //            ResultPropertyCollection rpvc = rs.Properties;
        //            //if rpvc.Contains("distinguishedname")
        //            isFirstProp = true;
        //            foreach (string key in rpvc.PropertyNames)
        //            {
        //                if (rpvc[key][0].ToString() != "System.Byte[]")
        //                {
        //                    if (isFirstProp) isFirstProp = false;
        //                    else sb.Append(",");
        //                    sb.Append("\""); sb.Append(_JsonEscape(key)); sb.Append("\":\""); sb.Append(_JsonEscape(rpvc[key][0].ToString())); sb.Append("\"");
        //                }
        //            }
        //            sb.Append("}");

        //        }


        //        sb.Append("]");

        //        //for (int counter = 0; counter < resultCol.Count; counter++)
        //        //{
        //        //    SearchResult result = resultCol[counter];
        //        //    if (result.Properties.Contains("displayname"))
        //        //    {
        //        //        System.Diagnostics.Debug.Write(counter.ToString() + ": ");
        //        //        System.Diagnostics.Debug.WriteLine((String)result.Properties["displayname"][0]);
        //        //    }
        //        //}


        //    }
        //    return sb.ToString(); 
        //}

        public void BeginTransaction()
        {
        }
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
        }
        public void CommitTransaction()
        {
        }
        public void RollbackTransaction()
        {
        }
        private int iCommandTimeout = 30;
        public int CommandTimeout { get { return iCommandTimeout; } set { iCommandTimeout = value; } }
        public void Dispose()
        {
        }
        public bool ConnOpen()
        {
            return true;
        }
        public DataTable GetSchema(string sqlString)
        {
            return null;
        }
        public IDataReader GetDataReader(string sqlString)
        {
            return null;
        }
        public DataTable GetDataTable(string sqlString)
        {
            string strResponse = GetResponse(sqlString);
            DataTable ret = new DataTable();
            ret.Columns.Add("Response", System.Type.GetType("System.String"));
            DataRow objDataRow = ret.NewRow();
            objDataRow["Response"] = strResponse;
            ret.Rows.Add(objDataRow);
            return ret;
        }
        public DataTable GetDataTable(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            string strResponse = GetResponse(sqlString);
            DataTable ret = new DataTable();
            ret.Columns.Add("Response", System.Type.GetType("System.String"));
            DataRow objDataRow = ret.NewRow();
            objDataRow["Response"] = strResponse;
            ret.Rows.Add(objDataRow);
            m_Response = strResponse;
            return ret;
        }
        public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
        {
            return GetDataTable(sqlString);
        }
        public string ExecuteNonQuery(string sqlString)
        {
            string strResponse = "";
            string strOuterXml = "";
            string strInnerXml = "";

            return ExecuteNonQuery(sqlString, ref strResponse, ref strOuterXml, ref strInnerXml);
        }
        public string ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            try
            {
                return GetResponse(sqlString);
            }
            catch (Exception ex)
            {
                throw;
            }
            return "";
        }
        public string ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        {
            return ExecuteNonQuery(sqlString);
        }


    }
}
