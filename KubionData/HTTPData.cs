﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
////using KubionLogNamespace;

namespace KubionDataNamespace
{
    class HTTPData:IData 
    {
        string strUrl;
        string strUserName;
        string strPassword;
        string strDomain ;
        string strProxyUrl;
        string strProxyUserName ;
        string strProxyPassword ;
        string strProxyDomain ;
        string strThumbprint = "";
        CookieContainer cookieContainer = null;
        X509Certificate2 myCertificate = null;

        public HTTPData(string sConnectionString)
        {
                strUrl = ParseConnString(sConnectionString);
                if (GetConnStringValue(sConnectionString, "Session") == "1")
                    cookieContainer = new CookieContainer();
                
        }
        private string ParseConnString(string sURI)
        {
            strUrl = GetConnStringValue(sURI, "URI");
            strUserName = GetConnStringValue(sURI, "UserName");
            strPassword = GetConnStringValue(sURI, "Password");
            strDomain = GetConnStringValue(sURI, "Domain");
            strProxyUrl = GetConnStringValue(sURI, "ProxyURI");
            strProxyUserName = GetConnStringValue(sURI, "ProxyUserName");
            strProxyPassword = GetConnStringValue(sURI, "ProxyPassword");
            strProxyDomain = GetConnStringValue(sURI, "ProxyDomain");
            strThumbprint = GetConnStringValue(sURI, "Thumbprint");
            if (strThumbprint != "")
                myCertificate = FindThumbprintInStore(strThumbprint, StoreName.My, StoreLocation.LocalMachine);
            return strUrl;
        }
        private string GetConnStringValue(string strURI, string strKey)
        {
            int intStart = (";" + strURI).IndexOf(";" + strKey + "=", StringComparison.CurrentCultureIgnoreCase);
            if (intStart == -1) return "";
            intStart += (strKey + "=").Length;
            int intEnd = (strURI + ";").IndexOf(";", intStart);
            return (strURI + ";").Substring(intStart, (intEnd - intStart));
        }

        /*
         * ConnectionString: URI=http://service.woco.local/web.svc/;UserName=;Password=;Domain=;LoginProvider=;ProxyUrl=;ProxyUserName=;ProxyPassword=;ProxyDomain=;ProxyLoginProvider=;
         * Query:
         * [RequestUri]				GetDocuments
         * [RequestMethod]			POST
         * [RequestHeaders]			Content-Type~text/xml;charset=UTF-8|SOAPAction~urn:www-infosupport-com:corporatieservicebus:bsovereenkomst:v1:ibsovereenkomst/IBSOvereenkomst/ZoekHuuropzeggingReden|
         * [RequestDataEncoding]	utf-8
         * [RequestDataString]		<xml />
         */
        /*
         * ConnectionString: URI=http://www.google.com;UserName=;Password=;Domain=;LoginProvider=;ProxyUrl=;ProxyUserName=;ProxyPassword=;ProxyDomain=;ProxyLoginProvider=;
         * Query:
         * [RequestUri]				GetDocuments
         * [RequestMethod]			POST
         * [RequestHeaders]			Content-Type~application/json;charset=UTF-8|authorization~Basic NmJkZThiYTE|
         * [Payload]	    		{"Subject":"Your email flight plan!","Text-part":"Flight canceled"}
         */
        private string[] parseHTTPQuery(string sqlString)
        {
            string[] aLines = new string[5];

            for (int i = 0; i < 4; i++)
            {
                if (sqlString.IndexOf("\r\n") != -1)
                {
                    aLines[i] = sqlString.Substring(0, sqlString.IndexOf("\r\n"));
                    sqlString = sqlString.Substring(sqlString.IndexOf("\r\n") + 2);
                }
                else
                {
                    aLines[i] = sqlString;
                    sqlString = "";
                }
            }
            aLines[4] = (sqlString == "\r\n") ? "" : sqlString;
            return aLines ;
        }
        private JArray SelectJSONArray(string l_Source, string l_JPath)
        {
            JToken jt;
            if (l_JPath == "")
                jt = JToken.Parse(l_Source);
            else
                jt = (JObject.Parse(l_Source)).SelectToken(l_JPath);
            if (jt is JArray)
                return (JArray)jt;
            else
                return new JArray(jt);
        }

        //void AddHeaderCookies(HttpWebRequest request, string strCookies)
        //{
        //    if (request.CookieContainer == null) request.CookieContainer = new CookieContainer();
        //    if (strCookies != "")
        //    {
        //        string[] aCookies = strCookies.Split(';');
        //        foreach (string sCookie in aCookies)
        //        {
        //            string[] aCookie = sCookie.Split('=');
        //            request.CookieContainer.Add(new Cookie(aCookie[0], aCookie[1], "", "localhost"));
        //        }
        //    }
        //}
        void AddJsonCookies(HttpWebRequest request, string strCookies)
        {
            if (request.CookieContainer == null) request.CookieContainer = new CookieContainer();
            if (strCookies != "")
            {
                JArray ja = SelectJSONArray(strCookies, "");
                foreach (JToken j in ja)
                    request.CookieContainer.Add(new Cookie(j.Value<string>("Name"), j.Value<string>("Value"), j.Value<string>("Path"), j.Value<string>("Domain") == null ? "localhost" : j.Value<string>("Domain")));
            }
        }
        //CookieContainer ContainerCookies(string strCookies)
        //{
        //    CookieContainer cc = new CookieContainer();
        //    if (strCookies != "")
        //    {
        //        JArray ja = SelectJSONArray(strCookies, "");
        //        foreach (JToken j in ja)
        //            cc.Add(new Cookie(j.Value<string>("Name"), j.Value<string>("Value"), j.Value<string>("Path"), j.Value<string>("Domain")==null?"localhost":j.Value<string>("Domain")));
        //    }
        //    return cc;
        //}
        private void AddJsonHeaders(HttpWebRequest request, string strJsonRequestHeaders)
        {
            JArray ja = SelectJSONArray(strJsonRequestHeaders, "");
            foreach (JToken jt in ja)
                foreach (JProperty jp in jt.Children<JProperty>())
                    AddHeader(request, jp.Name.ToLower(), jp.Value.ToString());
        }
        private void AddPipeHeaders(HttpWebRequest request, string strRequestHeaders)
        {
            foreach (string strRequestHeader in strRequestHeaders.Split('|'))
            {
                if (strRequestHeader.Split('~').Length == 2)
                    AddHeader(request, strRequestHeader.Split('~')[0].ToLower(), strRequestHeader.Split('~')[1]);
            }
        }
        private   void AddHeader(HttpWebRequest request ,string strTitle,string strValue)
        {
                    if (strTitle == "accept")
                    {
                        request.Accept = strValue;
                    }
                    else if (strTitle == "connection")
                    {
                        request.Connection = strValue;
                    }
                    else if (strTitle == "content-length")
                    {
                        request.ContentLength = long.Parse(strValue);
                    }
                    else if (strTitle == "content-type")
                    {
                        request.ContentType = strValue;
                    }
                    else if (strTitle == "date") { }
                    else if (strTitle == "expect")
                    {
                        request.Expect = strValue;
                    }
                    else if (strTitle == "host") { }
                    else if (strTitle == "if-modified-since")
                    {
                        request.IfModifiedSince = DateTime.Parse(strValue);
                    }
                    else if (strTitle == "range") { }
                    else if (strTitle == "referer")
                    {
                        request.Referer = strValue;
                    }
                    else if (strTitle == "transfer-encoding")
                    {
                        request.TransferEncoding = strValue;
                    }
                    else if (strTitle == "user-agent")
                    {
                        request.UserAgent = strValue;
                    }
                    else if (strTitle == "proxy-connection") { }
                    else
                    {
                        request.Headers.Add(strTitle, strValue);
                        //if (strRequestHeader.Split('~')[0] == "Cookie")
                        //    AddHeaderCookies(request, strRequestHeader.Split('~')[1]);
                    }

        }
        private static X509Certificate2 FindThumbprintInStore(string thumbprint,StoreName name, StoreLocation location)
        {
            if (thumbprint=="") return null;
            X509Store certStore = new X509Store(name, location);
            certStore.Open(OpenFlags.ReadOnly);
            var certCollection = certStore.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false);
            certStore.Close();
            if (certCollection.Count > 0)
            {
                return certCollection[0];
            }
            else
            {
                return null;
            }
        }
        private string GetHTTPResponse(string strRequestUri, string strRequestUserName, string strRequestPassword, string strRequestDomain
    , string strProxyUri, string strProxyUserName, string strProxyPassword, string strProxyDomain
    , string strRequestMethod, string strRequestHeaders, string strRequestDataEncoding, string strRequestDataString, ref string strCookies)
        {
            if (strRequestUri == null) throw new Exception("Missing RequestUri");
            if (strRequestMethod == null || strRequestMethod == "") strRequestMethod = "GET";

            // CookiesHeaders
            //JToken jCH = JSONParse(strCookies, "{}");
            //strHeaders = JSONSelect(jCH, "headers").ToString(Newtonsoft.Json.Formatting.None);
            //strCookies = JSONSelect(jCH, "cookies").ToString(Newtonsoft.Json.Formatting.None); 
            System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;


            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(strRequestUri);

            string strHeaders = "[]";
            if (strCookies == "")
                strCookies = "[]";
            else
            {
                JToken jCH = JToken.Parse(strCookies);
                strHeaders = jCH.SelectToken("headers").ToString(Newtonsoft.Json.Formatting.None);
                strCookies = jCH.SelectToken("cookies").ToString(Newtonsoft.Json.Formatting.None);
            }
            if (strHeaders != "[]") AddJsonHeaders(request, strHeaders);

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
                if (strCookies != "[]") AddJsonCookies(request, strCookies);
            }
            //else
            //    request.CookieContainer = ContainerCookies(strCookies);
            //////////AddJsonCookies(request, strCookies);

            request.Method = strRequestMethod;
            request.Timeout = CommandTimeout * 1000;
            AddPipeHeaders(request, strRequestHeaders);
            //foreach (string strRequestHeader in strRequestHeaders.Split('|'))
            //{
            //    if (strRequestHeader.Split('~').Length == 2)
            //    {
            //        if ((strRequestHeader.Split('~')[0].ToLower()) == "accept")
            //        {
            //            request.Accept = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "connection")
            //        {
            //            request.Connection = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "content-length")
            //        {
            //            request.ContentLength = long.Parse(strRequestHeader.Split('~')[1]);
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "content-type")
            //        {
            //            request.ContentType = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "date") { }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "expect")
            //        {
            //            request.Expect = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "host") { }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "if-modified-since")
            //        {
            //            request.IfModifiedSince = DateTime.Parse(strRequestHeader.Split('~')[1]);
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "range") { }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "referer")
            //        {
            //            request.Referer = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "transfer-encoding")
            //        {
            //            request.TransferEncoding = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "user-agent")
            //        {
            //            request.UserAgent = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "proxy-connection") { }
            //        else
            //        {
            //            request.Headers.Add(strRequestHeader.Split('~')[0], strRequestHeader.Split('~')[1]);
            //            //if (strRequestHeader.Split('~')[0] == "Cookie")
            //            //    AddHeaderCookies(request, strRequestHeader.Split('~')[1]);
            //        }
            //    }
            //}

            /* Provides credentials for password-based authentication schemes such as basic, digest, NTLM, and Kerberos authentication */
            if ((strRequestUserName == "" || strRequestUserName == null) && (strRequestPassword == "" || strRequestPassword == null) && (strRequestDomain == "" || strRequestDomain == null))
            {
                /* Set the credentials to be like impersonate */
                request.Credentials = System.Net.CredentialCache.DefaultCredentials;
            }
            else if (strRequestDomain == "" || strRequestDomain == null)
            {
                request.Credentials = new System.Net.NetworkCredential(strRequestUserName, strRequestPassword);
            }
            else
            {
                request.Credentials = new System.Net.NetworkCredential(strRequestUserName, strRequestPassword, strRequestDomain);
            }

            if (strProxyUri != "" && strProxyUri != null)
            {
                WebProxy proxy = new WebProxy();
                proxy.Address = new Uri(strProxyUri);
                if (strProxyUserName != "" && strProxyUserName != null)
                {
                    proxy.Credentials = new NetworkCredential(strProxyUserName, (strProxyPassword == null ? "" : strProxyPassword), (strProxyDomain == null ? "" : strProxyDomain));
                }
                request.Proxy = proxy;
            }

            if (myCertificate != null)
            {
                request.ClientCertificates.Add(myCertificate);
            }

            /* Send the content */
            if (strRequestDataString.Length > 0)
            {
                Encoding objEncoding = Encoding.Default;
                if (strRequestDataEncoding != null && strRequestDataEncoding != "")
                {
                    try { objEncoding = Encoding.GetEncoding(strRequestDataEncoding); }
                    catch (Exception) { objEncoding = Encoding.Default; }
                }
                byte[] postData = objEncoding.GetBytes(strRequestDataString);
                request.ContentLength = postData.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(postData, 0, postData.Length);
                requestStream.Close();
            }

            //if (cookieContainer != null)
            //{
            //    request.CookieContainer = cookieContainer;
            //    AddJsonCookies(request, strCookies);
            //}
            ////else
            ////    request.CookieContainer = ContainerCookies(strCookies);
            ////////////AddJsonCookies(request, strCookies);

            /* Read the response XML */
            //WebResponse response = null;
            HttpWebResponse response = null;
            string strResponse = "";
            try { response = (HttpWebResponse)request.GetResponse(); }
            catch (WebException webex)
            {
                if ((strHeaders.IndexOf("XDocIgnore") == -1) && (strRequestHeaders.IndexOf("XDocIgnore") == -1))
                {/* Need some special attention to get the error message from the response */
                    String data = String.Empty;
                    if (webex.Response != null)
                    {
                        StreamReader r = new StreamReader(webex.Response.GetResponseStream());
                        data = r.ReadToEnd();
                        r.Close();
                    }
                    throw new Exception(webex.Message + "\r\n" + data + "\r\nURL (decoded): " + strRequestUri + "\r\nPosted data (decoded): " + System.Web.HttpUtility.UrlDecode(strRequestDataString, Encoding.Default));
                }
                else
                {
                    response = (HttpWebResponse)webex.Response;
                    if (response == null)
                    {
                        strCookies = "{\"cookies\":" + "" + ",\"headers\":[" + "{\"Error\":1},{\"Message\":\"" + webex.Message + "\"}" + "]}";

                    }
                    else
                    {

                        response.Headers.Add("Error", "1");
                        response.Headers.Add("Message", webex.Message);
                        //response.Headers.Add("StatusCode", response.StatusCode.ToString());
                        //response.Headers.Add("StatusCodeInt", ((int)response.StatusCode).ToString());
                    }

                }
            }
            if (response != null)
            {
                response.Headers.Add("StatusCode", response.StatusCode.ToString());
                response.Headers.Add("StatusCodeInt", ((int)response.StatusCode).ToString());
                response.Headers.Add("StatusDescription", response.StatusDescription.ToString());
                Stream responseStream = response.GetResponseStream();
                StreamReader sr = new StreamReader(responseStream);
                strResponse = sr.ReadToEnd();
                string strReplace = (char)0x0 + "|" + (char)0x1 + "|" + (char)0x2 + "|" + (char)0x3 + "|" + (char)0x4 + "|" + (char)0x5 + "|" + (char)0x6 + "|" + (char)0x7 + "|" + (char)0x8 + "|" + (char)0xB + "|" + (char)0xC + "|" + (char)0xE + "|" + (char)0xF + "|" + (char)0x10 + "|" + (char)0x11 + "|" + (char)0x12 + "|" + (char)0x13 + "|" + (char)0x14 + "|" + (char)0x15 + "|" + (char)0x1A + "|" + (char)0x1B + "|" + (char)0x1C + "|" + (char)0x1D + "|" + (char)0x1E + "|" + (char)0x1F + (char)0x16 + "|" + (char)0x17 + "|" + (char)0x18 + "|" + (char)0x19 + "|" + (char)0x7F;
                Regex pattern = new Regex(strReplace);
                strResponse = pattern.Replace(strResponse, " ");
                strCookies = Newtonsoft.Json.JsonConvert.SerializeObject(((HttpWebResponse)response).Cookies);

                strHeaders = "";
                for (int i = 0; i < response.Headers.Count; i++)
                    strHeaders += ",{\"" + _JsonEscape(response.Headers.GetKey(i)) + "\":\"" + _JsonEscape(response.Headers.Get(i).ToString()) + "\"}";
                strHeaders = strHeaders == "" ? "" : strHeaders.Substring(1);
                strCookies = "{\"cookies\":" + strCookies + ",\"headers\":[" + strHeaders + "]}";

                //Cache-Control -- private
                //Content-Length -- 228
                //Content-Type -- text/html; charset=utf-8
                //Server -- Microsoft-IIS/8.5
                //X-AspNet-Version -- 4.0.30319
                //Authorization -- Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjbGFpbSI6Ikt1YmlvbjogXCJKV1RcIiwge0FQSX0iLCJhdWQiOiIqLmt1Ymlvbi5ubCIsImlzcyI6Imp3dC5rdWJpb24ubmwiLCJpYXQiOjE0NjMxNDMxMDYsImV4cCI6MTQ2MzE0NDAwNn0.rtfp0jV19fS9CV03Z_m_pgvQKqk5uwwv6N_bXS15b6E
                //Set-Cookie -- Authorization=Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjbGFpbSI6Ikt1YmlvbjogXCJKV1RcIiwge0FQSX0iLCJhdWQiOiIqLmt1Ymlvbi5ubCIsImlzcyI6Imp3dC5rdWJpb24ubmwiLCJpYXQiOjE0NjMxNDMxMDYsImV4cCI6MTQ2MzE0NDAwNn0.rtfp0jV19fS9CV03Z_m_pgvQKqk5uwwv6N_bXS15b6E; path=/
                //X-Powered-By -- ASP.NET
                //Date -- Fri, 13 May 2016 12:38:26 GMT
                response.Close();
            }
            return strResponse;
        }

        private string GetHTTPResponse_orig(string strRequestUri, string strRequestUserName, string strRequestPassword, string strRequestDomain
            , string strProxyUri, string strProxyUserName, string strProxyPassword, string strProxyDomain
            , string strRequestMethod, string strRequestHeaders, string strRequestDataEncoding, string strRequestDataString, ref string strCookies)
        {
            if (strRequestUri == null) throw new Exception("Missing RequestUri");
            if (strRequestMethod == null || strRequestMethod == "") strRequestMethod = "GET";
            
            // CookiesHeaders
            //JToken jCH = JSONParse(strCookies, "{}");
            //strHeaders = JSONSelect(jCH, "headers").ToString(Newtonsoft.Json.Formatting.None);
            //strCookies = JSONSelect(jCH, "cookies").ToString(Newtonsoft.Json.Formatting.None); 
            System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;


            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(strRequestUri);

            string strHeaders = "[]";
            if (strCookies == "")
                strCookies = "[]";
            else
            {
                JToken jCH = JToken.Parse(strCookies);
                strHeaders = jCH.SelectToken("headers").ToString(Newtonsoft.Json.Formatting.None);
                strCookies = jCH.SelectToken("cookies").ToString(Newtonsoft.Json.Formatting.None);
            }
            if (strHeaders != "[]") AddJsonHeaders(request, strHeaders);

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
                if (strCookies != "[]") AddJsonCookies(request, strCookies);
            }
            //else
            //    request.CookieContainer = ContainerCookies(strCookies);
            //////////AddJsonCookies(request, strCookies);

            request.Method = strRequestMethod;
            request.Timeout = CommandTimeout * 1000;
            AddPipeHeaders(request, strRequestHeaders);
            //foreach (string strRequestHeader in strRequestHeaders.Split('|'))
            //{
            //    if (strRequestHeader.Split('~').Length == 2)
            //    {
            //        if ((strRequestHeader.Split('~')[0].ToLower()) == "accept")
            //        {
            //            request.Accept = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "connection")
            //        {
            //            request.Connection = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "content-length")
            //        {
            //            request.ContentLength = long.Parse(strRequestHeader.Split('~')[1]);
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "content-type")
            //        {
            //            request.ContentType = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "date") { }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "expect")
            //        {
            //            request.Expect = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "host") { }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "if-modified-since")
            //        {
            //            request.IfModifiedSince = DateTime.Parse(strRequestHeader.Split('~')[1]);
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "range") { }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "referer")
            //        {
            //            request.Referer = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "transfer-encoding")
            //        {
            //            request.TransferEncoding = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "user-agent")
            //        {
            //            request.UserAgent = strRequestHeader.Split('~')[1];
            //        }
            //        else if ((strRequestHeader.Split('~')[0].ToLower()) == "proxy-connection") { }
            //        else
            //        {
            //            request.Headers.Add(strRequestHeader.Split('~')[0], strRequestHeader.Split('~')[1]);
            //            //if (strRequestHeader.Split('~')[0] == "Cookie")
            //            //    AddHeaderCookies(request, strRequestHeader.Split('~')[1]);
            //        }
            //    }
            //}

            /* Provides credentials for password-based authentication schemes such as basic, digest, NTLM, and Kerberos authentication */
            if ((strRequestUserName == "" || strRequestUserName == null) && (strRequestPassword == "" || strRequestPassword == null) && (strRequestDomain == "" || strRequestDomain == null))
            {
                /* Set the credentials to be like impersonate */
                request.Credentials = System.Net.CredentialCache.DefaultCredentials;
            }
            else if (strRequestDomain == "" || strRequestDomain == null)
            {
                request.Credentials = new System.Net.NetworkCredential(strRequestUserName, strRequestPassword);
            }
            else
            {
                request.Credentials = new System.Net.NetworkCredential(strRequestUserName, strRequestPassword, strRequestDomain);
            }

            if (strProxyUri != "" && strProxyUri != null)
            {
                WebProxy proxy = new WebProxy();
                proxy.Address = new Uri(strProxyUri);
                if (strProxyUserName != "" && strProxyUserName != null)
                {
                    proxy.Credentials = new NetworkCredential(strProxyUserName, (strProxyPassword == null ? "" : strProxyPassword), (strProxyDomain == null ? "" : strProxyDomain));
                }
                request.Proxy = proxy;
            }

            if(myCertificate!=null)
            {
                request.ClientCertificates.Add(myCertificate);
            }

            /* Send the content */
            if (strRequestDataString.Length > 0)
            {
                Encoding objEncoding = Encoding.Default;
                if (strRequestDataEncoding != null && strRequestDataEncoding != "")
                {
                    try { objEncoding = Encoding.GetEncoding(strRequestDataEncoding); }
                    catch (Exception ) { objEncoding = Encoding.Default; }
                }
                byte[] postData = objEncoding.GetBytes(strRequestDataString);
                request.ContentLength = postData.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(postData, 0, postData.Length);
                requestStream.Close();
            }

            //if (cookieContainer != null)
            //{
            //    request.CookieContainer = cookieContainer;
            //    AddJsonCookies(request, strCookies);
            //}
            ////else
            ////    request.CookieContainer = ContainerCookies(strCookies);
            ////////////AddJsonCookies(request, strCookies);

            /* Read the response XML */
            WebResponse response = null;
            try { response = request.GetResponse(); }
            catch (WebException webex)
            {
                /* Need some special attention to get the error message from the response */
                String data = String.Empty;
                if (webex.Response != null)
                {
                    StreamReader r = new StreamReader(webex.Response.GetResponseStream());
                    data = r.ReadToEnd();
                    r.Close();
                }
                throw new Exception(webex.Message + "\r\n" + data + "\r\nURL (decoded): " + strRequestUri + "\r\nPosted data (decoded): " + System.Web.HttpUtility.UrlDecode(strRequestDataString, Encoding.Default));
            }
            Stream responseStream = response.GetResponseStream();
            StreamReader sr = new StreamReader(responseStream);
            string strResponse = sr.ReadToEnd();

            string strReplace = (char)0x0 + "|" + (char)0x1 + "|" + (char)0x2 + "|" + (char)0x3 + "|" + (char)0x4 + "|" + (char)0x5 + "|" + (char)0x6 + "|" + (char)0x7 + "|" + (char)0x8 + "|" + (char)0xB + "|" + (char)0xC + "|" + (char)0xE + "|" + (char)0xF + "|" + (char)0x10 + "|" + (char)0x11 + "|" + (char)0x12 + "|" + (char)0x13 + "|" + (char)0x14 + "|" + (char)0x15 + "|" + (char)0x1A + "|" + (char)0x1B + "|" + (char)0x1C + "|" + (char)0x1D + "|" + (char)0x1E + "|" + (char)0x1F + (char)0x16 + "|" + (char)0x17 + "|" + (char)0x18 + "|" + (char)0x19 + "|" + (char)0x7F;
            Regex pattern = new Regex(strReplace);
            strResponse = pattern.Replace(strResponse, " ");
            strCookies = Newtonsoft.Json.JsonConvert.SerializeObject(((HttpWebResponse)response).Cookies);

            strHeaders = "";
            for (int i = 0; i < response.Headers.Count; i++)
                strHeaders += ",{\"" + _JsonEscape(response.Headers.GetKey(i)) + "\":\"" + _JsonEscape(response.Headers.Get(i).ToString()) + "\"}";
            strHeaders = strHeaders==""?"":strHeaders.Substring(1);
            strCookies = "{\"cookies\":" + strCookies + ",\"headers\":[" + strHeaders + "]}";

            //Cache-Control -- private
            //Content-Length -- 228
            //Content-Type -- text/html; charset=utf-8
            //Server -- Microsoft-IIS/8.5
            //X-AspNet-Version -- 4.0.30319
            //Authorization -- Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjbGFpbSI6Ikt1YmlvbjogXCJKV1RcIiwge0FQSX0iLCJhdWQiOiIqLmt1Ymlvbi5ubCIsImlzcyI6Imp3dC5rdWJpb24ubmwiLCJpYXQiOjE0NjMxNDMxMDYsImV4cCI6MTQ2MzE0NDAwNn0.rtfp0jV19fS9CV03Z_m_pgvQKqk5uwwv6N_bXS15b6E
            //Set-Cookie -- Authorization=Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjbGFpbSI6Ikt1YmlvbjogXCJKV1RcIiwge0FQSX0iLCJhdWQiOiIqLmt1Ymlvbi5ubCIsImlzcyI6Imp3dC5rdWJpb24ubmwiLCJpYXQiOjE0NjMxNDMxMDYsImV4cCI6MTQ2MzE0NDAwNn0.rtfp0jV19fS9CV03Z_m_pgvQKqk5uwwv6N_bXS15b6E; path=/
            //X-Powered-By -- ASP.NET
            //Date -- Fri, 13 May 2016 12:38:26 GMT

            response.Close();
            return strResponse;
        }
        public static string _JsonEscape(string aText)
        {
            //“ Is iets anders dan “  « » “ ”
            char[] escapeChars = new char[] { '\\', '\"', '“', '”', '\n', '\r', '\t', '\b', '\f' };
            StringBuilder s = new StringBuilder();
            if (aText != null)
            {
                int j = 0;
                int i = aText.IndexOfAny(escapeChars);
                while (i != -1)
                {
                    if (i != j) s.Append(aText.Substring(j, i - j));
                    switch (aText[i])
                    {
                        case '\\': s.Append("\\\\"); break;
                        case '\"': s.Append("\\\""); break;
                        case '“': s.Append("\\\""); break;
                        case '”': s.Append("\\\""); break;
                        case '\n': s.Append("\\n"); break;
                        case '\r': s.Append("\\r"); break;
                        case '\t': s.Append("\\t"); break;
                        case '\b': s.Append("\\b"); break;
                        case '\f': s.Append("\\f"); break;
                    }
                    j = i + 1;
                    i = aText.IndexOfAny(escapeChars, j);
                }
                s.Append(aText.Substring(j));
            }
            return s.ToString();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public string GetResponse(string strCommandUrl, ref string strCookies)
        {
            string[] aLines = parseHTTPQuery(strCommandUrl);
            string strCmd = aLines[0];
            string strMethod = aLines[1];
            string strHeaders = aLines[2];
            string strContentType = aLines[3];
            string strData = aLines[4];
            //strContentType = (strContentType == "") ? "application/x-www-form-urlencoded" : System.Web.HttpUtility.UrlDecode(strContentType, Encoding.Default);
            strContentType = (strContentType == "") ? "utf-8" : System.Web.HttpUtility.UrlDecode(strContentType, Encoding.Default);

            return GetHTTPResponse(strUrl + strCmd, strUserName, strPassword, strDomain, strProxyUrl, strProxyUserName, strProxyPassword, strProxyDomain, strMethod, strHeaders, strContentType, strData, ref strCookies);
 //           return GetHTTPResponse(strUrl + strCmd, strUserName, strPassword, strDomain, strProxyUrl, strProxyUserName, strProxyPassword, strProxyDomain, strMethod, strHeaders, strContentType, strData);
        }
        private string GetHTTPResponse(string strRequestUri, string strRequestUserName, string strRequestPassword, string strRequestDomain
            , string strProxyUri, string strProxyUserName, string strProxyPassword, string strProxyDomain
            , string strRequestMethod, string strRequestHeaders, string strRequestDataEncoding, string strRequestDataString)
        {
            if (strRequestUri == null) throw new Exception("Missing RequestUri");
            if (strRequestMethod == null || strRequestMethod == "") strRequestMethod = "GET";

            System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(strRequestUri);
            if (cookieContainer != null) request.CookieContainer = cookieContainer;
            request.Method = strRequestMethod;
            request.Timeout = CommandTimeout * 1000;
            foreach (string strRequestHeader in strRequestHeaders.Split('|'))
            {
                if (strRequestHeader.Split('~').Length == 2)
                {
                    if ((strRequestHeader.Split('~')[0].ToLower()) == "accept")
                    {
                        request.Accept = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "connection")
                    {
                        request.Connection = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "content-length")
                    {
                        request.ContentLength = long.Parse(strRequestHeader.Split('~')[1]);
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "content-type")
                    {
                        request.ContentType = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "date") { }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "expect")
                    {
                        request.Expect = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "host") { }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "if-modified-since")
                    {
                        request.IfModifiedSince = DateTime.Parse(strRequestHeader.Split('~')[1]);
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "range") { }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "referer")
                    {
                        request.Referer = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "transfer-encoding")
                    {
                        request.TransferEncoding = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "user-agent")
                    {
                        request.UserAgent = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "proxy-connection") { }
                    else
                    {
                        request.Headers.Add(strRequestHeader.Split('~')[0], strRequestHeader.Split('~')[1]);
                    }
                }
            }

            /* Provides credentials for password-based authentication schemes such as basic, digest, NTLM, and Kerberos authentication */
            if ((strRequestUserName == "" || strRequestUserName == null) && (strRequestPassword == "" || strRequestPassword == null) && (strRequestDomain == "" || strRequestDomain == null))
            {
                /* Set the credentials to be like impersonate */
                request.Credentials = System.Net.CredentialCache.DefaultCredentials;
            }
            else if (strRequestDomain == "" || strRequestDomain == null)
            {
                request.Credentials = new System.Net.NetworkCredential(strRequestUserName, strRequestPassword);
            }
            else
            {
                request.Credentials = new System.Net.NetworkCredential(strRequestUserName, strRequestPassword, strRequestDomain);
            }

            if (strProxyUri != "" && strProxyUri != null)
            {
                WebProxy proxy = new WebProxy();
                proxy.Address = new Uri(strProxyUri);
                if (strProxyUserName != "" && strProxyUserName != null)
                {
                    proxy.Credentials = new NetworkCredential(strProxyUserName, (strProxyPassword == null ? "" : strProxyPassword), (strProxyDomain == null ? "" : strProxyDomain));
                }
                request.Proxy = proxy;
            }

            if (myCertificate != null)
            {
                request.ClientCertificates.Add(myCertificate);
            }

            /* Send the content */
            if (strRequestDataString.Length > 0)
            {
                Encoding objEncoding = Encoding.Default;
                if (strRequestDataEncoding != null && strRequestDataEncoding != "")
                {
                    try { objEncoding = Encoding.GetEncoding(strRequestDataEncoding); }
                    catch (Exception ) { objEncoding = Encoding.Default; }
                }
                byte[] postData = objEncoding.GetBytes(strRequestDataString);
                request.ContentLength = postData.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(postData, 0, postData.Length);
                requestStream.Close();
            }

            /* Read the response XML */
            WebResponse response = null;
            try { response = request.GetResponse(); }
            catch (WebException webex)
            {
                /* Need some special attention to get the error message from the response */
                String data = String.Empty;
                if (webex.Response != null)
                {
                    StreamReader r = new StreamReader(webex.Response.GetResponseStream());
                    data = r.ReadToEnd();
                    r.Close();
                }
                throw new Exception(webex.Message + "\r\n" + data + "\r\nURL (decoded): " + strRequestUri + "\r\nPosted data (decoded): " + System.Web.HttpUtility.UrlDecode(strRequestDataString, Encoding.Default));
            }
            Stream responseStream = response.GetResponseStream();
            StreamReader sr = new StreamReader(responseStream);
            string strResponse = sr.ReadToEnd();

            string strReplace = (char)0x0 + "|" + (char)0x1 + "|" + (char)0x2 + "|" + (char)0x3 + "|" + (char)0x4 + "|" + (char)0x5 + "|" + (char)0x6 + "|" + (char)0x7 + "|" + (char)0x8 + "|" + (char)0xB + "|" + (char)0xC + "|" + (char)0xE + "|" + (char)0xF + "|" + (char)0x10 + "|" + (char)0x11 + "|" + (char)0x12 + "|" + (char)0x13 + "|" + (char)0x14 + "|" + (char)0x15 + "|" + (char)0x1A + "|" + (char)0x1B + "|" + (char)0x1C + "|" + (char)0x1D + "|" + (char)0x1E + "|" + (char)0x1F + (char)0x16 + "|" + (char)0x17 + "|" + (char)0x18 + "|" + (char)0x19 + "|" + (char)0x7F;
            Regex pattern = new Regex(strReplace);
            strResponse = pattern.Replace(strResponse, " ");
            response.Close();
            return strResponse;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public string GetResponse(string strCommandUrl)
        {
                string strCookies = "";
                return GetResponse(strCommandUrl,  ref strCookies);
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public DataTable GetDataTable(string sqlString)
        {
            string strResponse = GetResponse(sqlString);
            DataTable ret = new DataTable();
            ret.Columns.Add("Response", System.Type.GetType("System.String"));
            DataRow objDataRow = ret.NewRow();
            objDataRow["Response"] = strResponse;
            ret.Rows.Add(objDataRow);
            return ret;
        }
        public DataTable GetDataTable(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            string strResponse = GetResponse(sqlString);
            DataTable ret = new DataTable();
            ret.Columns.Add("Response", System.Type.GetType("System.String"));
            DataRow objDataRow = ret.NewRow();
            objDataRow["Response"] = strResponse;
            ret.Rows.Add(objDataRow);
            m_Response = strResponse;
            return ret;
        }
        public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
        {
            return GetDataTable(sqlString);
        }



        public string ExecuteNonQuery(string sqlString)
        {
            string strResponse = "";
            string strOuterXml = "";
            string strInnerXml = "";

            return ExecuteNonQuery(sqlString, ref strResponse, ref strOuterXml, ref strInnerXml);
        }
        public string ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
                return GetResponse(sqlString);
        }
        public string ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        {
            return ExecuteNonQuery(sqlString);
        }

        public void BeginTransaction()
        {
        }
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
        }
        public void CommitTransaction()
        {
        }
        public void RollbackTransaction()
        {
        }
        private int iCommandTimeout = 30;
        public int CommandTimeout { get { return iCommandTimeout; } set { iCommandTimeout = value; } }
        public void Dispose()
        {
        }
        private void OpenConnection()
        {
        }
        public bool ConnOpen()
        {
            return true;
        }
        public DataTable GetSchema(string sqlString)
        {
            return null;
        }
        public IDataReader GetDataReader(string sqlString)
        {
            return null;
        }

    }
}
